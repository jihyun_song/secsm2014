# -*- coding: utf-8 -*-
import config as CONFIG
import memcache
import random
import common_exceptions as ex
from time import time
from database import Database as db
import commons

class Tokenizer:
    @staticmethod
    def str_xor(s1, s2):
        return "".join([chr(ord(c1) ^ ord(c2)) for (c1,c2) in zip(s1,s2)])
        
    @staticmethod
    def xorCryptString(data, key='awesomepassword', encode=False, decode=False):
        from itertools import cycle
        import base64
        if decode:
            data = data.decode('ascii')
        xored = ''.join(chr(ord(x) ^ ord(y)) for (x,y) in zip(data, cycle(key)))
        if encode:
            return xored.encode('ascii').strip()
        return xored
        
    @staticmethod
    def makeToken(id):
        data = id;
        data += str(time())
        token = Tokenizer.xorCryptString(data, CONFIG.TOKENIZER_KEY, True)
        wordList = list(token)
        token = ""
        
        for t in wordList:
            if (t >= 'a' and t<='z') or (t >= 'A' and t<='Z') or (t >= '1' and t<='9'):
                token += t
            else:
                token += random.choice('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789')
                
        return token
        #return Tokenizer.str_xor(data, CONFIG.TOKENIZER_KEY)

    @staticmethod
    def decodeToken(token):
        return Tokenizer.xorCryptString(token, CONFIG.TOKENIZER_KEY, False, True)
        
    @staticmethod
    def saveInfo(id, grant):
        token = Tokenizer.makeToken(id)
        mc = memcache.Client(['127.0.0.1:11211'], debug=0)
        mc.set(token+"id", id, 600)
        mc.set(token+"grant", grant, 600)
        mc.set(token+"time", str(time()), 600)
        return token
            
    @staticmethod
    def getIDFromToken(token):
        mc = memcache.Client(['127.0.0.1:11211'], debug=0)
        return mc.get(token+"id")
            
    @staticmethod
    def getGrantFromToken(token):
        mc = memcache.Client(['127.0.0.1:11211'], debug=0)
        return mc.get(token+"grant")
            
    @staticmethod
    def getTimeFromToken(token):
        mc = memcache.Client(['127.0.0.1:11211'], debug=0)
        return mc.get(token+"time")
        
    @staticmethod
    def initializeTime(token):
        mc = memcache.Client(['127.0.0.1:11211'], debug=0)
        mc.set(token+"id", mc.get(token+"id"), 600)
        mc.set(token+"grant", mc.get(token+"grant"), 600)
        mc.set(token+"time", str(time()), 600)
        
    @staticmethod
    def destroyToken(token):
        mc = memcache.Client(['127.0.0.1:11211'], debug=0)
        mc.delete(token+"id")
        mc.set(token+"grant")
        mc.set(token+"time")
        
        
        
class Certifier:
    @staticmethod
    def getToken(id, pword):
        try:
            data = db.login(id,pword)
        except Exception as e:
            raise ex.CertificationFailException();

        if data is None:
            raise ex.CertificationFailException();
        else:
            token = Tokenizer.saveInfo(id, data['accountgrant']);
            data['token'] = token
            return data
            
    @staticmethod
    def checkTokenValidate(token):
        id = Tokenizer.getIDFromToken(token)
        if id is None:
            return False
        else:
            Tokenizer.initializeTime(token)
            return True
            
    @staticmethod
    def getIDFromToken(token):
        return Tokenizer.getIDFromToken(token)
            
    @staticmethod
    def getGrantFromToken(token):
        return Tokenizer.getGrantFromToken(token)
            
    @staticmethod
    def getTimeFromToken(token):
        return Tokenizer.getTimeFromToken(token)
        
    @staticmethod
    def destroyToken(token):
        Tokenizer.destroyToken(token)
