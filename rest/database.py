# -*- coding: utf-8 -*-
import config as CONFIG
import mysql.connector
import common_exceptions as ex
import commons
import json
from config import Grant as g
from DAO.accountDAO import AccountDAO as Account
from DAO.livingDAO import LivingDAO as Living
from DAO.educationDAO import EducationDAO as Education
from DAO.pxDAO import PxDAO as Px
from DAO.boardDAO import BoardDAO as Board


class Database:

############################################### account management ##################################

    @staticmethod
    def createUser(id, pword, username, entered, googleid, birthday, phone, university, accountgrant=1000, social_security_number=None):
        """
        create User account

        :param id : User account id
        :param pword : User account password
        :param name : User name
        :param Entered : Entered season, ex) 24-1
        :param googleid : User Google id
        :param birthday : Date ex) 2014-01-11
        :param phone : Phone number ex) 010-0000-0000
        :param accountgrant : User grant
                       000 : admin
                       001 : membership operator

                       100 : the chairman
                       101 : the manager of the general affairs department

                       200 : chief of living department [MALE]
                       201 : chief of living department [FEMALE]
                       202~: member of living department

                       300 : chief of education department [MALE]
                       301 : chief of education department [FEMALE]
                       302~: member of education department

                       400 : chief of network department [MALE]
                       401 : chief of network department [FEMALE]
                       402~: member of network department

                       500 : chief of asset management department [MALE]
                       501 : chief of asset management department [FEMALE]
                       502~: member of asset management department

                       600 : chief of planning department [MALE]
                       601 : chief of planning department [FEMALE]
                       602~: member of planning department

                       700 : chief of PX department [MALE]
                       701 : chief of PX department [FEMALE]
                       702~: member of PX department

                       800 : chief of hardware department [MALE]
                       801 : chief of hardware department [FEMALE]
                       802~: member of hardware department

                       1000 for general user
        """
        account = Account()
        account.createUserInternal(id, pword, username, entered, googleid, birthday, phone, university, accountgrant, social_security_number)
        del account

    @staticmethod
    def modifyUser(id, pword=None, username=None, entered=None, googleid=None, birthday=None, phone=None, university=None, accountgrant=None, social_security_number=None):
        account = Account()
        account.modifyUserInternal(id, pword, username, entered, googleid, birthday, phone, university, accountgrant, social_security_number)
        del account    
        
    @staticmethod
    def selectAllUsers():
        account = Account()
        data = account.selectAllUsersInternal()
        del account
        return data

    @staticmethod
    def selectUser(id):
        """
        get User info

        :param id : User account id
        """
        account = Account()
        data = account.selectUserInternal(id)
        del account
        return data

    @staticmethod
    def selectUserByGoogleID(googleid):
        """
        get User info

        :param googleid : User google id
        """
        account = Account()
        data = account.selectUserByGoogleIDInternal(googleid)
        del account
        return data

    @staticmethod
    def selectUserGrantAndGoogleID(accountID):
        """
        get User info

        :param accountID : User account id
        """
        account = Account()
        data = account.selectUserGrantAndGoogleIDInternal(accountID)
        del account
        return data
        

    @staticmethod
    def login(gmail, pword):
        """
        login

        :param id : User account id
        :param pword : User password
        """
        account = Account()
        data = account.loginInternal(gmail, pword)
        del account
        return data

#####################################################################################################



############################################### living ##############################################

    @staticmethod
    def setNoCountDay(attenddate, reason):
        '''
        Set No count day for attendance

        :param attenddate : 2014-11-11
        :param reason : summary of that date
        '''
        living = Living()
        living.setNoCountDayInternal(attenddate, reason)
        del living

    @staticmethod
    def getNoCountDays(Month):
        '''
        get no count days

        :param Month : Any day in the month ex) 2010-01-01
        '''
        living = Living()
        data = living.getNoCountDaysInternal(Month)
        del living
        return data

    @staticmethod
    def setAttendance(account_id, attenddate, attendance):
        '''
        check the attendance

        :param account_id : User account id
        :param attenddate : 2014-11-11
        :param attendance : [0, 1]
                       0 : absence
                       1 : attendance
        '''
        living = Living()
        living.setAttendanceInternal(account_id, attenddate, attendance)
        del living

    @staticmethod
    def getAttendance(account_id, Month):
        '''
        get attendance info by account id

        :param account_id : User account id
        :param Month : Any day in the month ex) 2010-01-01
        '''
        living = Living()
        data = living.getAttendanceInternal(account_id, Month)
        all_count = living.getAttendanceDaysInternal(Month)
        
        if all_count == 0:
            data['rate'] = 100
        else:
            data['rate'] = 100.0*float(data['rate'])/float(all_count)
        
        del living
        return data

    @staticmethod
    def getAttendanceNumber(Month):
        '''
        get attendance number

        :param Month : Any day in the month ex) 2010-01-01
        '''
        living = Living()
        data = living.getAttendanceNumberInternal(Month)
        del living
        return data

    @staticmethod
    def getAttendanceDays(Month):
        '''
        get attendance days

        :param Month : Any day in the month ex) 2010-01-01
        '''
        living = Living()
        data = living.getAttendanceDaysInternal(Month)
        del living
        return data

    @staticmethod
    def getAttendanceRate(Month):
        '''
        get attendance rate

        :param Month : Any day in the month ex) 2010-01-01
        '''
        living = Living()
        data = living.getAttendanceNumberInternal(Month)
        all_count = living.getAttendanceDaysInternal(Month)

        average_rate = 0.0
        count = 0
        for d in data['list']:
            count += 1
            if all_count != 0:
                d['cnt'] /= float(all_count)
                d['cnt'] *= 100.0
                average_rate += d['cnt']
            else:
                d['cnt'] = 100
                average_rate = 100
                
            if d['cnt'] > 100:
                d['cnt'] = 100

        if count > 1:
            average_rate /= count
            
        data['avg'] = average_rate

        del living
        return data

    @staticmethod
    def setDuty(dutydate, m1_account_id, m2_account_id=None, m3_account_id=None, m4_account_id=None, f1_account_id=None, f2_account_id=None, f3_account_id=None, f4_account_id=None):
        '''
        set duty by account id

        :param dutydate : 2014-11-11
        :param m1~4_account_id : Male1~4 account id
        :param f1~4_account_id : Female1~4 account id
        '''
        living = Living()
        living.setDutyInternal(dutydate, m1_account_id, m2_account_id, m3_account_id, m4_account_id, f1_account_id, f2_account_id, f3_account_id, f4_account_id)
        del living

    @staticmethod
    def getDuty(Month, account_id=None):
        '''
        get attendance info by account id

        :param Month : Any day in the month ex) 2010-01-01
        :param account_id : User account id
        '''
        living = Living()
        data = living.getDutyInternal(Month, account_id)
        del living
        return data

#####################################################################################################


############################################ education ##############################################

    @staticmethod
    def createProject(ProjectName, Summary, StartMonth, EndMonth, Creative_Rate, Technical_Rate, Useful_Rate, Successful_Rate, PL, Team1, Team2=None, Team3=None, Team4=None):
        """
        create project info

        :param ProjectName : String
        :param Summary : String
        :param StartMonth : 2014-11-01
                      Day don't care
        :param EndMonth   : 2014-12-01
                      Day don't care
        :param PL         : User account id
        :param Team1~4    : User account id

        minimum 2 teamates (PL, Team1)
        maximum 5 teamates (PL, Team1~4)
        """
        education = Education()
        education.createProjectInternal(ProjectName, Summary, StartMonth, EndMonth, Creative_Rate, Technical_Rate, Useful_Rate, Successful_Rate, PL, Team1, Team2, Team3, Team4)
        del education

    @staticmethod
    def modifyProject(id, ProjectName=None, Summary=None, StartMonth=None, EndMonth=None, Creative_Rate=None, Technical_Rate=None, Useful_Rate=None, Successful_Rate=None, PL=None, Team1=None, Team2=None, Team3=None, Team4=None):
        """
        update Project
        If you don't need to modify, insert None

        :param ProjectName : String
        :param Summary : String
        :param StartMonth : 2014-11-01
                      Day don't care
        :param EndMonth   : 2014-12-01
                      Day don't care
        :param PL         : User account id
        :param Team1~4    : User account id

        minimum 2 teamates (PL, Team1)
        maximum 5 teamates (PL, Team1~4)
        """
        education = Education()
        data = education.modifyProjectInternal(id, ProjectName, Summary, StartMonth, EndMonth, Creative_Rate, Technical_Rate, Useful_Rate, Successful_Rate, PL, Team1, Team2, Team3, Team4)
        del education

    @staticmethod
    def selectAllProjects():
        """
        get all prjects info
        """
        education = Education()
        data = education.selectAllProjectsInternal()
        del education
        return data

    @staticmethod
    def selectProject(id):
        """
        get specific project info
        :param id : project id
        """
        education = Education()
        data = education.selectProjectInternal(id)
        del education
        return data

    @staticmethod
    def deleteProject(id):
        """
        get specific project info
        :param id : project id
        """
        education = Education()
        data = education.deleteProjectInternal(id)
        del education
        return data

    @staticmethod
    def searchProject(Keyword):
        """
        search project by String for about
        Teamate name, Project Name, Summary, etc.

        :param Keyword : String to find keyword
        """
        education = Education()
        data = education.searchProjectInternal(Keyword)
        del education
        return data

    @staticmethod
    def registJuror(account_id, project_id, presentation_type):
        """
        regist juror of project presentation

        :param account_id: User account id
        :param project_id: project id
        :param presentation_type: [1, 2]
                            1 is Start Project presentation
                            2 is End Project presentation
        :param attendance: [0, 1]
        """
        education = Education()
        education.registJurorInternal(account_id, project_id, presentation_type)
        del education

    @staticmethod
    def getJuror(project_id):
        """
        get User's presentation attendance

        :param project_id: Project id
        """
        education = Education()
        data = education.getJurorInternal(project_id)
        del education
        return data

    @staticmethod
    def getAllJuror():
        education = Education()
        data = education.getAllJurorInternal()
        del education
        return data

    @staticmethod
    def deleteJuror(project_id, presentation_type, account_id):
        """
        get User's presentation attendance

        :param project_id: Project id
        :param presentation_type: [1, 2]
                            1 is Start Project presentation
                            2 is End Project presentation
        :param account_id: Account id
        """
        education = Education()
        education.deleteJurorInternal(project_id, presentation_type, account_id)
        del education

    @staticmethod
    def getJurorByAccount(account_id):
        """
        get User's juror attendance

        :param account_id: User account id
        """
        education = Education()
        data = education.getJurorByAccountInternal(account_id)
        del education
        return data

    @staticmethod
    def modifyAppraiseContents(project_id, presentation_type, contents):
        """
        get User's presentation attendance

        :param project_id: Project ID
        :param presentation_type: [1, 2]
                            1 is Start Project presentation
                            2 is End Project presentation
        :param contents: Appraise Contents
        """
        education = Education()
        data = education.modifyAppraiseContentsInternal(project_id, presentation_type, contents)
        del education

    @staticmethod
    def getAppraiseContents(project_id, presentation_type):
        """
        get User's presentation attendance

        :param account_id: User account id
        """
        education = Education()
        data = education.getAppraiseContentsInternal(project_id, presentation_type)
        del education
        return data

    @staticmethod
    def appraiseProject(account_id, appraise_id, jurorAppraiseResult):
        """
        reqeust to judge project presentation

        :param account_id: User account id
        :param appraise_id: id from getAppraiseContents
        :param jurorAppraiseResult:
        """
        education = Education()
        education.appraiseProjectInternal(account_id, appraise_id, jurorAppraiseResult)
        del education

    @staticmethod
    def getAppraiseResult(project_id, presentation_type):
        """
        get User's presentation attendance

        :param account_id: User account id
        """
        education = Education()
        data = education.getAppraiseResultInternal(project_id, presentation_type)
        del education
        return data

    @staticmethod
    def setPresentationAttendance(account_id, project_id, presentation_type, attendance):
        """
        check the user attendance at project

        :param account_id: User account id
        :param project_id: project id
        :param presentation_type: [1, 2]
                            1 is Start Project presentation
                            2 is End Project presentation
        :param attendance: [0, 1]
        """
        education = Education()
        education.setPresentationAttendanceInternal(account_id, project_id, presentation_type, attendance)
        del education

    @staticmethod
    def getPresentationAttendance(account_id):
        """
        get User's presentation attendance

        :param account_id: User account id
        """
        education = Education()
        data = education.getPresentationAttendanceInternal(account_id)
        del education
        return data

#####################################################################################################


################################################ PX #################################################

    @staticmethod
    def registItem(barcode, itemname, price, remained=0):
        """
        regist PX item

        :param barcode: barcode of item
        :param name: item name
        :param price: item price
        :param remained : item number
        """
        px = Px()
        px.registItemInternal(barcode, itemname, price, remained)
        del px

    @staticmethod
    def modifyItem(barcode, itemname=None, price=None, remained=None):
        """
        modify PX item info

        :param barcode: barcode of item
        :param itemname: item name
        :param price: item price
        :param remained : item number
        """
        px = Px()
        px.modifyItemInternal(barcode, itemname, price, remained)
        del px

    @staticmethod
    def getItems():
        """
        get PX items info

        """
        px = Px()
        data = px.getItemsInternal()
        del px
        return data

    @staticmethod
    def getItemByBarcode(barcode):
        """
        get PX items info
        
        ;param barcode : barcode of item
        """
        px = Px()
        data = px.getItemByBarcodeInternal(barcode)
        del px
        return data

    @staticmethod
    def deleteItem(barcode):
        """
        delet PX item

        :param barcode: barcode of item
        """
        px = Px()
        px.deleteItemInternal(barcode)
        del px

    @staticmethod
    def buyItem(account_id_list, item_list):
        """
        buy item from px

        :param account_id_list: User account id list
        :param item_list: list of dictionary (item_list = [item1, item2, item3,...])
                                              item['barcode'] : barcode of item
                                              item['number'] : number_of item
        """
        px = Px()
        px.buyItemInternal(account_id_list, item_list)
        del px

    @staticmethod
    def refundItem(account_id_list, item_list):
        """
        refund item from px

        :param account_id_list: User account id list
        :param item_list: list of dictionary (item_list = [item1, item2, item3,...])
                                              item['barcode'] : barcode of item
                                              item['number'] : number_of item
        """
        px = Px()
        px.refundItemInternal(account_id_list, item_list)
        del px

    @staticmethod
    def chargePX(account_id, amount):
        """
        buy item from px

        :param account_id: User account id
        :param amount: Amount of money
        """
        px = Px()
        px.chargePXInternal(account_id, amount)
        del px

    @staticmethod
    def getPXLog(start_time, end_time):
        """
        get User's presentation attendance

        :param start_time: start_time YYYY:MM:DD HH:mm:ss
        :param end_time: end_time YYYY:MM:DD HH:mm:ss
        """
        px = Px()
        data = px.getPXLogInternal(start_time, end_time)
        del px
        return data

    @staticmethod
    def getPXLogbyID(account_id, start_time, end_time):
        """
        get User's presentation attendance

        :param start_time: start_time YYYY:MM:DD HH:mm:ss
        :param end_time: end_time YYYY:MM:DD HH:mm:ss
        """
        px = Px()
        data = px.getPXLogbyIDInternal(account_id, start_time, end_time)
        del px
        return data



################################################ Board #####################################################

    @staticmethod
    def writeFreeBoard(account_id, title, content):
        """
        write freeboard

        :param account_id: id of writer
        :param title: title of board
        :param content: content of board
        """
        board = Board()
        board.writeAllBoardInternal(None, account_id, title, content, 1)
        del board

    @staticmethod
    def writeNotice(account_id, title, content):
        """
        write notice

        :param account_id: id of writer
        :param title: title of board
        :param content: content of board
        """
        board = Board()
        board.writeAllBoardInternal(None, account_id, title, content, 0)
        del board

    @staticmethod
    def getFreeBoardList(page_num=1, page_size=10):
        """
        get freeboard list
        """
        board = Board()
        data = board.getAllBoardListInternal(None, 1, page_num, page_size)
        del board
        return data

    @staticmethod
    def getNoticeList(page_num=1, page_size=10):
        """
        get notice list
        """
        board = Board()
        data = board.getAllBoardListInternal(None, 0, page_num, page_size)
        del board
        return data

    @staticmethod
    def getFreeBoardDetail(board_id):
        """
        get freeboard list
        """
        board = Board()
        data = board.getAllBoardDetailInternal(board_id)
        del board
        return data

    @staticmethod
    def writeComment(board_id, account_id, content):
        """
        write comment

        :param board_id: id of board
        :param account_id: id of comment writer
        :param content: content of comment
        """
        board = Board()
        board.writeCommentInternal(board_id, account_id, content)
        del board

    @staticmethod
    def deleteComment(comment_id):
        """
        write comment

        :param comment_id: id of comment
        """
        board = Board()
        board.deleteCommentInternal(comment_id)
        del board