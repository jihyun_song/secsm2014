# -*- coding: utf-8 -*-
import json
import logging
import datetime
import httplib2
import pickle
from googleapiclient.errors import HttpError
from logging.handlers import RotatingFileHandler
from flask import Flask, request, current_app, redirect
from decorators import crossdomain, set_grant, common_exception_handler
from apiclient.discovery import build
import commons
from config import Grant as g
from oauth2client.client import OAuth2WebServerFlow, Credentials
import common_exceptions as ex
import config as CONFIG
from database import Database as db
from calendarCertifier import CalendarCertifier as calCerti
from certifier import Certifier as cert

CLIENT_ID = "257566283142-c0algk432k6rj15jpkjol3euuvjv59b9.apps.googleusercontent.com"
CLIENT_SECRET = "-7sKsz1MuLDuCiSoFWUwTh62"


SCMEM_CAL_ID='5npnr98c46usg20j7eu67594rc@group.calendar.google.com'
SC_CAL_TEST_ID='pd549kv6168c10f87m07d62uo4@group.calendar.google.com'

# for test
SCMEM_CAL_ID=SC_CAL_TEST_ID
REDIRECT_URI='http://remake.secsm.org/signup.html'

app = Flask(__name__)

user_cre=None

class GoogleCalendar:
    def __init__(self):
        self.cal_id = SCMEM_CAL_ID
        self.redirect_uri = REDIRECT_URI
        self.client_id = CLIENT_ID
        self.client_secret = CLIENT_SECRET
        self.scope = 'https://www.googleapis.com/auth/calendar'
        self.admin_id = CONFIG.GOOGLE_CAL_ADMIN
        self.approval_prompt='force'
        self.access_type = 'offline'

    def _get_credential(self, account_id):
        '''
        get credentials from db
        if there is no credentials, need to authorize
        '''
        if account_id is not None:
            credentials = pickle.loads(calCerti.getCerti(account_id))
            #credentials = pickle.load(open('cres.pkl', 'rb'))
        return credentials

    def _get_service(self, account_id):
        credentials = self._get_credential(account_id)
        http = httplib2.Http()
        http = credentials.authorize(http)
        service = build("calendar", "v3", http=http)
        return service

    def register_credentials(self, account_id, code):
        '''
        get google_id from database by account_id
        get grant for google calendar from google account
        '''
        flow = OAuth2WebServerFlow(client_id=self.client_id,
                client_secret=self.client_secret,
                scope=self.scope)

        flow.redirect_uri =  self.redirect_uri
        try:
            credentials = flow.step2_exchange(code)
        except Exception as e:
            raise ex.CalendarCertificationAuthentificateException
        calCerti.saveCerti(account_id, pickle.dumps(credentials))
        self.invite_common_calendar(account_id)

    def invite_common_calendar(self, account_id):
        '''
        invite common calendar ID by self.cal_id
        '''
        try:
            role = None
            target_user = db.selectUserGrantAndGoogleID(account_id)
            if target_user['accountgrant'] in g.autonomy:
                role = "writer"
            elif target_user['accountgrant'] in g.master_chief:
                role = "owner"
            else:
                role = "reader"

            rule = {
                'scope': {
                    'type': 'user',
                    'value': target_user['googleid'],
                },
                'role': role
            }
            service = self._get_service(self.admin_id)
            created_rule = service.acl().insert(calendarId=self.cal_id, body=rule).execute()

        except Exception as e:
            print e
            raise ex.InviteCommonCalendarFailedException

    def update_common_calendar(self, account_id):
        '''
        change the role of calendar
        '''
        try:
            role = None
            target_user = db.selectUserGrantAndGoogleID(account_id)
            if target_user['accountgrant'] in g.autonomy:
                role = "writer"
            elif target_user['accountgrant'] in g.master_chief:
                role = "owner"
            else:
                role = "reader"

            rule = {
                'scope': {
                    'type': 'user',
                    'value': target_user['googleid'],
                },
                'role': role
            }
            service = self._get_service(self.admin_id)
            updated_rule = service.acl().update(calendarId=self.cal_id, ruleId='user:%s'%target_user['googleid'], body=rule).execute()
        except Exception as e:
            print e
            raise ex.InviteCommonCalendarFailedException

    def create_event(self, account_id, summary, description, location, start_datetime, end_datetime):
        event = {}
        event['summary'] = summary
        event['description'] = description
        event['location'] = location
        event['start'] = {'dateTime': commons.change_from_datetime_to_ISO_datetime(start_datetime), 'timeZone':'Asia/Seoul'}
        event['end'] = {'dateTime': commons.change_from_datetime_to_ISO_datetime(end_datetime), 'timeZone':'Asia/Seoul'}

        service = self._get_service(account_id)

        created_event = service.events().insert(calendarId=self.cal_id, body=event).execute()
        return created_event

    def get_event(self, account_id, event_id):
        service = self._get_service(account_id)
        event = service.events().get(calendarId=self.cal_id, eventId=event_id).execute()
        return event

    def modify_event(self, account_id, event_id, summary, description, location, start_datetime, end_datetime):
        service = self._get_service(account_id)

        event = self.get_event(account_id, event_id)
        event['summary'] = summary
        event['description'] = description
        event['location'] = location
        event['start'] = {'dateTime': commons.change_from_datetime_to_ISO_datetime(start_datetime), 'timeZone':'Asia/Seoul'}
        event['end'] = {'dateTime': commons.change_from_datetime_to_ISO_datetime(end_datetime), 'timeZone':'Asia/Seoul'}

        updated_event = service.events().update(
                calendarId=self.cal_id,
                eventId=event_id,
                body=event
                ).execute()
        return updated_event

    def delete_event(self, account_id, event_id):
        service = self._get_service(account_id)
        service.events().delete(
                calendarId=self.cal_id,
                eventId=event_id
                ).execute()

    def get_events_by_month(self, account_id, month):
        page_token = None
        result_events = []
        service = self._get_service(account_id)
        min_month = commons.get_first_date_as_ISO_datetime(month)
        max_month = commons.get_last_date_as_ISO_datetime(month)
        try:
            while True:
                events = service.events().list(
                        calendarId=self.cal_id,
                        pageToken=page_token,
                        singleEvents=True,
                        maxResults=1000,
                        orderBy='startTime',
                        timeMin=min_month,
                        timeMax=max_month
                        ).execute()
                for event in events['items']:
                    result_events.append(event)
                page_token = events.get('nextPageToken')
                if not page_token:
                    break

        except HttpError as e:
            print "e.__dict__ : %s" %e
        except Exception as e:
            print e
            #if str(e)[-21:-1] == "returned \"Not Found\"":
            if result_events != None:
                self.invite_common_calendar(account_id)
                raise ex.InviteCommonCalendarFailedException
            else:
                raise ex.ResponseDataNoneException

        return result_events

#################################################
# Living Calendars                              #
#################################################
@app.before_request
def grant_check_before_request():
    '''
    logging & authentification token
    '''
    if CONFIG.DEBUG_MODE:
        try:
            current_app.logger.debug('request headers : \n%s' % request.headers)
            current_app.logger.debug('request data : \n%s' % request.data)
            current_app.logger.debug('request files : \n%s' % str(request.files))
            print "reqeust : %s" %request
        except Exception as e:
            print e

    if request.endpoint in app.view_functions:
        view_func = app.view_functions[request.endpoint]
        try:
            print "You have KEYs in HEADER.AUTHRIZATION : %s" %str(request.headers['Authorization'])
            if 'Authorization' in request.headers and cert.checkTokenValidate(str(request.headers['Authorization'])):
                print "certification good"
                if hasattr(view_func, '_grant'):
                    if cert.getGrantFromToken(str(request.headers['Authorization'])) in getattr(view_func, '_grant'):
                        pass

                current_app.logger.debug('request authorization : \n %s' %request.headers['Authorization'])
            else:
                app.logger.warning("Unauthorized Access for %s" %request.endpoint)
                print "TODO : please set sertification. you didn't cert"
                return commons.body(message="Authorization failed", status=commons.API_UNAUTHORIZED)
        except Exception as e:
            print "auth : %s " %e
    else:
        app.logger.warning("Wrong endpoint, Please deny this below IP. request.endpoint is %s" %request.endpoint)
        return commons.body(message="Page Not Found", status=commons.PAGE_NOT_FOUND)


@app.route("/events/<account_id>/<month>", methods=["GET", "OPTIONS"], \
        endpoint="get_calendar_events_info_by_month")
@crossdomain(origin='*', headers='Content-Type')
@set_grant(g.master_chief + g.current_members)
@common_exception_handler
def get_calendar_events_info_by_month(account_id, month):
    '''
    get google calendar event's element information by month
    '''
    cal = GoogleCalendar()
    data = cal.get_events_by_month(account_id, month)
    del cal
    return commons.body(message="get calendar events by month done", data = data)

@app.route("/events", methods=["POST", "OPTIONS"], \
        endpoint="create_calendar_event_info")
@crossdomain(origin='*', headers='Content-Type')
@set_grant(g.master_chief + g.current_members)
@common_exception_handler
def create_calendar_event_info():
    '''
    create the google calendar event
    '''
    cal = GoogleCalendar()
    data = request.get_json(force=True)
    resp_data = cal.create_event( data['account_id'], \
            data['summary'], data['description'], data['location'], \
            data['start_datetime'], data['end_datetime'])
    del cal
    return commons.body(message="get calendar events by month done", data = resp_data)

@app.route("/events/<event_id>", methods=["PUT", "OPTIONS"], \
        endpoint="modify_calendar_event_info")
@crossdomain(origin='*', headers='Content-Type')
@set_grant(g.master_chief + g.current_members)
@common_exception_handler
def modify_calendar_event_info(event_id):
    '''
    modify the google calendar event
    '''
    cal = GoogleCalendar()
    data = request.get_json(force=True)
    resp_data = cal.modify_event(data['account_id'], \
            data['event_id'], data['summary'], data['description'], data['location'], \
            data['start_datetime'], data['end_datetime'])
    del cal
    return commons.body(message="modify calendar event done", data = resp_data)

@app.route("/events/<account_id>/<event_id>", methods=["DELETE", "OPTIONS"], \
        endpoint="delete_calendar_event_info")
@crossdomain(origin='*', headers='Content-Type')
@set_grant(g.master_chief)
@common_exception_handler
def delete_calendar_event_info(account_id, event_id):
    '''
    delete the google calendar event
    '''
    cal = GoogleCalendar()
    data = cal.delete_event(account_id, event_id)
    del cal
    return commons.body(message="delete calendar event done", data = data)


#################################################
# Run Google Calendar Application               #
#################################################

if __name__ == '__main__':
    # create log handler
    formatter = logging.Formatter("[%(asctime)s] %(levelname)s {%(pathname)s:%(lineno)d} - %(message)s")
    handler = RotatingFileHandler(CONFIG.GOOGLE_LOG_PATH, encoding='UTF-8', maxBytes=CONFIG.LOG_FILE_MAX_BYTES, \
            backupCount=CONFIG.BACKUP_COUNTS)
    handler.setLevel(logging.INFO)
    handler.setFormatter(formatter)

    # get log from werkzeug
    logger = logging.getLogger('werkzeug')
    logger.addHandler(handler)

    # get log from app.logger(by code)
    app.logger.addHandler(handler)
    #app.run(debug=DEBUG_MODE, host=CONFIG.RUN_HOST,port=CONFIG.GOOGLE_PORT)
    app.run(debug=True, host=CONFIG.RUN_HOST,port=CONFIG.GOOGLE_PORT)
