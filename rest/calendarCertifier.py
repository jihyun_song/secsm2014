# -*- coding: utf-8 -*-
import config as CONFIG
import mysql.connector
import common_exceptions as ex
import memcache
import commons

class CalendarCertifier:
    @staticmethod
    def saveCertiToCache(account_id, certi):
        mc = memcache.Client(['127.0.0.1:11211'], debug=0)
        mc.set((account_id+"_cal_certi").encode("utf-8"), certi, 600)

    @staticmethod
    def saveCertiToDB(account_id, certi):
        con = mysql.connector.connect(user=CONFIG.DATABASE_USER, password=CONFIG.DATABASE_PASSWD, use_unicode=True, charset='utf8')
        cursor = con.cursor()
        cursor.execute("use %(used_database)s;" % {'used_database' : CONFIG.DATABASE_DB})
        cursor.execute("set names utf8")

        query = "update account SET calendar_certi=%s where id=%s;"
        cursor.execute(query, (certi, account_id))
        con.commit()
        cursor.close()
        con.close()

    @staticmethod
    def saveCerti(account_id, certi):
        cs = CalendarCertifier()
        cs.saveCertiToCache(account_id, certi)
        cs.saveCertiToDB(account_id, certi)

    @staticmethod
    def getCertiFromCache(account_id):
        mc = memcache.Client(['127.0.0.1:11211'], debug=0)
        return mc.get((account_id+"_cal_certi").encode("utf-8"))

    @staticmethod
    def getCertiFromDB(account_id):
        con = mysql.connector.connect(user=CONFIG.DATABASE_USER, password=CONFIG.DATABASE_PASSWD, use_unicode=True, charset='utf8')
        cursor = con.cursor()
        cursor.execute("use %(used_database)s;" % {'used_database' : CONFIG.DATABASE_DB})
        cursor.execute("set names utf8")

        query = "select calendar_certi from account where id=%s;"
        cursor.execute(query, (account_id,))
        row = cursor.fetchone()
        
        if cursor.rowcount <= 0:
            cursor.close()
            con.close()
            raise ex.CalendarCertificationException()
        else:
            cursor.close()
            con.close()
            return row[0]

    @staticmethod
    def getCerti(account_id):
        cs = CalendarCertifier()
        certi = cs.getCertiFromCache(account_id)
        
        if certi is None:
            return cs.getCertiFromDB(account_id)
        else:
            return certi


