# -*- coding: utf-8 -*-

from flask import Response
import config as CONFIG
import json
import sys
import common_exceptions as ex
import datetime


#################################################
# Response Message                              #
#################################################
OK=200
FIELD_VALIDATION_FAIL=400
API_UNAUTHORIZED=401
DATA_ALREADY_EXIST_ERROR=402
DATA_NOT_EXIST_ERROR=403
PAGE_NOT_FOUND=404
SERVER_INTERNAL_ERROR=500
DATABASE_ERROR=501
MSG_OK="done"
CHARGE_MINUS=600
LOGIN_FAILED=601

def body(message, data=None, status=OK):
    if CONFIG.DEBUG_MODE and data is not None:
        try:
            print "commons body data : %s " % data
        except IOError as e:
            print "I/O error({0}): {1}".format(e.errno, e.strerror)
        except:
            print "Unexpected Error:" , sys.exc_info()[0]

    resp = json.dumps({u'status': status, u'result': { u'message': message, u'data': data }}, default=str)

    return Response(response=resp, mimetype='application/json', content_type='application/json;charset=utf-8')

#################################################
# date, ex) 2015-01-01                          #
# ISO datetime, ex) 2015-01-01T00:00:00+0900    #
#################################################
def validate_date(str_date):
    try:
        datetime.datetime.strptime(str_date, '%Y-%m-%d')
        return True
    except ValueError:
        raise ex.DateValidateException
    return False

def get_first_date_as_ISO_datetime(str_date):
    try:
        if validate_date(str_date):
            return datetime.datetime.strptime(str_date, '%Y-%m-%d')\
                    .strftime('%Y-%m-01T00:00:00+09:00')
    except Exception as e:
        print "get_first_date_as_ISO_datetime, input : %s" %str_date
        raise ex.DateValidateException

def get_last_date_as_ISO_datetime(str_date):
    try:
        if validate_date(str_date):
            last_datetime = get_last_day_of_month(str_date)
            return datetime.datetime.strptime(last_datetime, '%Y-%m-%d')\
                    .strftime('%Y-%m-%dT00:00:00+09:00')
    except Exception as e:
        print "get_last_date_as_ISO_datetime, input : %s" %str_date
        print "get_last_date_as_ISO_datetime, last_datetime : %s" %get_last_day_of_month(str_date)
        raise ex.DateValidateException


#################################################
# datetime,     ex) 2015-01-01 00:00:00            #
#################################################
def validate_datetime(str_datetime):
    try:
        datetime.datetime.strptime(str_datetime, '%Y-%m-%d %H:%M:%S')
        return True
    except ValueError:
        raise ex.DatetimeValidateException
    return False

def get_first_datetime_as_ISO_datetime(str_datetime):
    try:
        if validate_datetime(str_datetime):
            return datetime.datetime.strptime(str_datetime, '%Y-%m-%d %H:%M:%S')\
                    .strftime('%Y-%m-01T00:00:00+09:00')
    except Exception as e:
        raise ex.DateValidateException

def get_last_datetime_as_ISO_datetime(str_datetime):
    try:
        if validate_datetime(str_datetime):
            last_datetime = get_last_day_of_month(str_datetime)
            return datetime.datetime.strptime(last_datetime, '%Y-%m-%d %H:%M:%S')\
                    .strftime('%Y-%m-%dT00:00:00+09:00')
    except Exception as e:
        raise ex.DateValidateException


def change_from_datetime_to_ISO_datetime(str_datetime):
    try:
        if validate_datetime(str_datetime):
            return datetime.datetime.strptime(str_datetime, '%Y-%m-%d %H:%M:%S')\
                    .strftime('%Y-%m-%dT%H:%M:%S+09:00')
    except Exception as e:
        raise ex.DatetimeValidateException

def get_last_day_of_month(str_datetime):
    if str_datetime[5:7] == "02":
        if int(str_datetime[0:4]) % 4 != 0:
            str_datetime = str_datetime[:8] + "28" + str_datetime[10:]
        elif int(str_datetime[0:4]) % 100 != 0:
            str_datetime = str_datetime[:8] + "29" + str_datetime[10:]
        elif int(str_datetime[0:4]) % 400 != 0:
            str_datetime = str_datetime[:8] + "28" + str_datetime[10:]
        else:
            str_datetime = str_datetime[:8] + "29" + str_datetime[10:]
    elif str_datetime[5:7] == "01" or str_datetime[5:7] == "03" \
        or str_datetime[5:7] == "05" or str_datetime[5:7] == "07" \
        or str_datetime[5:7] == "08" or str_datetime[5:7] == "10" \
        or str_datetime[5:7] == "12":
        str_datetime = str_datetime[:8] + "31" + str_datetime[10:]
    else:
        str_datetime = str_datetime[:8] + "30" + str_datetime[10:]

    return str_datetime
