# -*- coding: utf-8 -*-
import commons
import mysql.connector

'''
Exception.msg returned to client as a message
Exception.status returned to client as a status code

Exception.__str__ returned to server API log
'''

class RequestMismatchException(Exception):
    def __init__(self, msg='request dismatched. check the GET parameter and POST body\'s id.'):
        self.msg = msg
        self.status = commons.FIELD_VALIDATION_FAIL

    def __str__(self):
        return "RequestMismatchException : the request GET parameter and POST body\'s id is not same, wrong access"

class RequestMissingException(Exception):
    def __init__(self, msg='request parameter missed. check API guideline.'):
        self.msg = msg
        self.status = commons.FIELD_VALIDATION_FAIL

    def __str__(self):
        return "RequestMissingException : the request required parameter missed"

class ResponseDataNoneException(Exception):
    def __init__(self, msg='response data is None.'):
        self.msg = msg
        self.status = commons.OK

    def __str__(self):
        return "ResponseDataNoneException : "

class DateValidateException(Exception):
    def __init__(self, msg='datetime validation fail. ex)2015-01-01'):
        self.msg = msg
        self.status = commons.FIELD_VALIDATION_FAIL

    def __str__(self):
        return "DateValidateException : datetime format is not matched"

class DatetimeValidateException(Exception):
    def __init__(self, msg='datetime validation fail. required ex) 2015-01-01 23:59:00'):
        self.msg = msg
        self.status = commons.FIELD_VALIDATION_FAIL

    def __str__(self):
        return "DatetimeValidateException : datetime format is not matched"

class AlreadyExistException(Exception):
    def __init__(self, msg='the item is already exist. check the value.'):
        self.msg = msg
        self.status = commons.DATA_ALREADY_EXIST_ERROR

    def __str__(self):
        return "AlreadyExistException : the item is already exist"

class NotExistException(Exception):
    def __init__(self, msg='item does not exist'):
        self.msg = msg
        self.status = commons.DATA_NOT_EXIST_ERROR

    def __str__(self):
        return "NotExistException : item does not exist"

class NotEnoughMoneyException(Exception):
    def __init__(self, msg='account has no enough money to buy.'):
        self.msg = msg
        self.status = commons.OK

    def __str__(self):
        return "NotEnoughException : not enough money"

class CertificationFailException(Exception):
    def __init__(self, msg='certification failed! check your ID & PASSWORD'):
        self.msg = msg
        self.status = commons.LOGIN_FAILED

    def __str__(self):
        return "CertificationFailException : certificate failed"

class AmountMinusException(Exception):
    def __init__(self, msg='amount can\'n be under zero.'):
        self.msg = msg
        self.status = commons.CHARGE_MINUS

    def __str__(self):
        return "AmountMinusException : amount can`t be under 0"

class CalendarCertificationException(Exception):
    def __init__(self, msg='calendar certification object is not exist'):
        self.msg = msg
        self.status = commons.API_UNAUTHORIZED

    def __str__(self):
        return "CalendarCertificationException : calendar certification object is not exist"

class CalendarCertificationAuthentificateException(Exception):
    def __init__(self, msg='calendar certificate failed, try again or request to admin.'):
        self.msg = msg
        self.status = commons.API_UNAUTHORIZED

    def __str__(self):
        return "CalendarCertificationAuthentificationException : calendar certificate failed"

class InviteCommonCalendarFailedException(Exception):
    def __init__(self, msg='invite calendar failed, please request to admin.'):
        self.msg = msg
        self.status = commons.API_UNAUTHORIZED

    def __str__(self):
        return "InviteCommonCalendarCertificationFailedException : please add user's google account in google calendar web page"


