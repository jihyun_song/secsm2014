DEBUG_MODE=True
RUN_PORT=80
RUN_HOST='0.0.0.0'

DATABASE_USER='database user account'
DATABASE_PASSWD='database user password'
DATABASE_DB='database name'

# using for generate token key to authorize
TOKENIZER_KEY='0000'

# define the ratio of eductaion depart's appraise.
# if EDUCATION_RATIO is 0.3, juror's appraise ratio will be 0.7
EDUCATION_RATIO=0.3

# log setting
LOG_PATH="logs/secsm.log"
GOOGLE_LOG_PATH="logs/secsm_calendar.log"
LOG_FILE_MAX_BYTES=100000
BACKUP_COUNTS=3

#################################################
# GRANT type                                    #
#################################################
class Grant:
    admin = [000]
    membership_operator = [001]
    chairman = [100]
    manager = [101]
    general_member = [1000]
    new_member = [2000]
    old_member = [3000]

    current_members = general_member + new_member
