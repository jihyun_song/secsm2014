import config as CONFIG
import mysql.connector
import common_exceptions as ex
import commons
import json
from config import Grant as g


class AccountDAO:
    def __init__(self):
        self.con = mysql.connector.connect(user=CONFIG.DATABASE_USER, password=CONFIG.DATABASE_PASSWD, use_unicode=True, charset='utf8')
        cursor = self.con.cursor()
        cursor.execute("use %(used_database)s;" % {'used_database' : CONFIG.DATABASE_DB})
        cursor.execute("set names utf8")
        cursor.close()

    def __del__(self):
        self.con.close()

    def fetchoneDict(self, cursor):
        row = cursor.fetchone()
        if row is None:
            return None
        else:
            cols = [ d[0] for d in cursor.description ]
            return dict(zip(cols, row))

    def fetchallDict(self, cursor):
        rows=[]
        row = self.fetchoneDict(cursor)
        while row is not None:
            rows.append(row)
            row = self.fetchoneDict(cursor)
        return rows

    def createUserInternal(self, id, pword, username, entered, googleid, birthday, phone, university, accountgrant, social_security_number):
        cursor = self.con.cursor()
        query = "select id from account where googleid=%s or id=%s;"
        cursor.execute(query, (googleid,id))
        data = self.fetchallDict(cursor)
        
        if cursor.rowcount > 0:
            cursor.close()
            raise ex.AlreadyExistException()
        
        query = "insert into account (id, pword, username, entered, googleid, birthday, phone, university, accountgrant, social_security_number)\
                VALUES (%s,password(%s),%s,%s,%s,%s,%s,%s,%s,%s);"
        cursor.execute(query, (id, pword, username, entered, googleid, birthday, phone, university, accountgrant, social_security_number))
        self.con.commit()
        cursor.close()

    def modifyUserInternal(self, id, pword, username, entered, googleid, birthday, phone, university, accountgrant, social_security_number):
        cursor = self.con.cursor()
        query = "select id from account where googleid=%s and id!=%s;"
        cursor.execute(query, (googleid, id))
        data = self.fetchallDict(cursor)
        
        if cursor.rowcount > 0:
            cursor.close()
            raise ex.AlreadyExistException()
            
        query = "update account set "
        count = 0
        params=[]

        if pword is not None:
            query += "pword=password(%s)"
            count += 1
            params.append(pword)

        if username is not None:
            query += ",username=%s"
            count += 1
            params.append(username)

        if entered is not None:
            query += ",entered=%s"
            count += 1
            params.append(entered)

        if googleid is not None:
            query += ",googleid=%s"
            count += 1
            params.append(googleid)

        if birthday is not None:
            query += ",birthday=%s"
            count += 1
            params.append(birthday)

        if phone is not None:
            query += ",phone=%s"
            count += 1
            params.append(phone)

        if university is not None:
            query += ",university=%s"
            count += 1
            params.append(university)

        if accountgrant is not None:
            query += ",accountgrant=%s"
            count += 1
            params.append(accountgrant)

        if social_security_number is not None:
            query += ",social_security_number=%s"
            count += 1
            params.append(social_security_number)

        if count == 0:
            cursor.close()
            return None
        else:
            query += " where id=%s;"
            params.append(id)

        cursor.execute(query, tuple(params))
        print(cursor.statement)
        self.con.commit()
        cursor.close()

    def selectAllUsersInternal(self):
        cursor = self.con.cursor()
        query = "select id,username,entered,googleid,birthday,phone,accountgrant,px_amount,university from account"
        cursor.execute(query)
        rows = self.fetchallDict(cursor)
        
        if cursor.rowcount <= 0:
            cursor.close()
            raise ex.NotExistException()
            
        cursor.close()
        return rows

    def selectUserInternal(self, id):
        cursor = self.con.cursor()
        query = "select id,username,entered,googleid,birthday,phone,accountgrant,px_amount,university from account where id=%s;"
        cursor.execute(query, (id,))
        
        row = self.fetchoneDict(cursor)
        
        if cursor.rowcount <= 0:
            cursor.close()
            raise ex.NotExistException()
            
        cursor.close()
        return row
        
        
    def selectUserByGoogleIDInternal(self, googleid):
        cursor = self.con.cursor()
        query = "select id,username,entered,googleid,birthday,phone,accountgrant,px_amount,university from account where googleid=%s;"
        cursor.execute(query, (googleid,))
        
        row = self.fetchoneDict(cursor)
        
        if cursor.rowcount <= 0:
            cursor.close()
            raise ex.NotExistException()
            
        cursor.close()
        return row
        
    def selectUserGrantAndGoogleIDInternal(self, accountID):
        cursor = self.con.cursor()
        query = "select googleid,accountgrant from account where id=%s;"
        cursor.execute(query, (accountID,))
        
        row = self.fetchoneDict(cursor)
        
        if cursor.rowcount <= 0:
            cursor.close()
            raise ex.NotExistException()
            
        cursor.close()
        return row
        
        
        
    def loginInternal(self, gmail, pword):
        cursor = self.con.cursor()
        query = "select id,username,entered,googleid,birthday,phone,accountgrant,px_amount,university from account where googleid=%s and pword=password(%s);"
        cursor.execute(query, (gmail, pword))
        
        data = self.fetchoneDict(cursor)
            
        if cursor.rowcount <= 0:
            cursor.close()
            raise ex.NotExistException()
            
        cursor.close()
        return data



