import config as CONFIG
import mysql.connector
import common_exceptions as ex
import commons
import json
from config import Grant as g


class PxDAO:
    def __init__(self):
        self.con = mysql.connector.connect(user=CONFIG.DATABASE_USER, password=CONFIG.DATABASE_PASSWD, use_unicode=True, charset='utf8')
        cursor = self.con.cursor()
        cursor.execute("use %(used_database)s;" % {'used_database' : CONFIG.DATABASE_DB})
        cursor.execute("set names utf8")
        cursor.close()

    def __del__(self):
        self.con.close()

    def fetchoneDict(self, cursor):
        row = cursor.fetchone()
        if row is None:
            return None
        else:
            cols = [ d[0] for d in cursor.description ]
            return dict(zip(cols, row))

    def fetchallDict(self, cursor):
        rows=[]
        row = self.fetchoneDict(cursor)
        while row is not None:
            rows.append(row)
            row = self.fetchoneDict(cursor)
        return rows

    def registItemInternal(self, barcode, itemname, price, remained):
        cursor = self.con.cursor()
        selectQuery = "select count(*) from px_item where barcode=%s;"
        cursor.execute(selectQuery, (barcode,))
        number = cursor.fetchone()[0]

        if number != 0:
            raise ex.AlreadyExistException()

        query = "insert into px_item (barcode, itemname, price, remained) values (%s, %s, %s, %s);"
        cursor.execute(query, (barcode, itemname, price, remained))
        self.con.commit()
        cursor.close()

    def modifyItemInternal(self, barcode, itemname, price, remained):
        cursor = self.con.cursor()
        selectQuery = "select count(*) from px_item where barcode=%s;"
        cursor.execute(selectQuery, (barcode,))
        number = cursor.fetchone()[0]

        if number == 0:
            raise ex.NotExistException()

        query = "update px_item SET"

        count = 0
        params=[]

        if itemname is not None:
            query += " itemname=%s"
            count += 1
            params.append(itemname)

        if price is not None:
            if count != 0:
                query += ","
            query += " price=%s"
            count += 1
            params.append(price)

        if remained is not None:
            if count != 0:
                query += ","
            query += " remained=%s"
            count += 1
            params.append(remained)

        if count == 0:
            cursor.close()
            return None
        else:
            query += " where barcode=%s;"
            params.append(barcode)

        cursor.execute(query, tuple(params))
        self.con.commit()
        cursor.close()

    def getItemsInternal(self):
        cursor = self.con.cursor()
        query = "select * from px_item;"
        cursor.execute(query)
        rows = self.fetchallDict(cursor)
        
        if cursor.rowcount <= 0:
            cursor.close()
            raise ex.NotExistException()
            
        cursor.close()
        return rows
        
    def getItemByBarcodeInternal(self, barcode):
        cursor = self.con.cursor()
        query = "select * from px_item where barcode=%s;"
        cursor.execute(query,(barcode,))
        rows = self.fetchoneDict(cursor)
        
        if cursor.rowcount <= 0:
            cursor.close()
            raise ex.NotExistException()
            
        cursor.close()
        return rows

    def deleteItemInternal(self, barcode):
        cursor = self.con.cursor()
        query = "delete from px_item where barcode=%s;"
        cursor.execute(query, (barcode,))
        self.con.commit()
        cursor.close()

    def buyItemInternal(self, account_id_list, item_list):
        account_number = len(account_id_list)
        account_params = []
        amount_params = []
        barcode_params = []

        cursor = self.con.cursor()
        
        priceSelectQuery = "select barcode, price from px_item where barcode in ("
        for item in item_list:
            priceSelectQuery += "%s,"
            barcode_params.append(item['barcode'])
            
        priceSelectQuery = priceSelectQuery[0:len(priceSelectQuery)-1]
        priceSelectQuery += ");"
        cursor.execute(priceSelectQuery, tuple(barcode_params))
        data = self.fetchallDict(cursor)

        if cursor.rowcount <= 0:
            raise ex.NotExistException()

        all_price = 0
        for d in data:
            for item in item_list:
                if item['barcode'] == d['barcode']:
                    all_price += int(d['price'])*int(item['number'])
                    break

        accountUpdateQuery = "update account SET px_amount=px_amount-%s where id in ("
        amountSelectQuery = "select px_amount from account where id in ("
        account_params.append(str(int(all_price)/account_number))
        
        for account in account_id_list:
            accountUpdateQuery += "%s,"
            amountSelectQuery += "%s,"
            account_params.append(account)
            amount_params.append(account)
        
        accountUpdateQuery = accountUpdateQuery[0:len(accountUpdateQuery)-1]
        accountUpdateQuery += ");"
        
        amountSelectQuery = amountSelectQuery[0:len(amountSelectQuery)-1]
        amountSelectQuery += ");"
        
        cursor.execute(amountSelectQuery, tuple(amount_params))
        data = self.fetchallDict(cursor)

        for d in data:
            if d['px_amount'] < int(all_price)/account_number:
                raise ex.NotEnoughMoneyException()

        for item in item_list:
            query = "update px_item SET remained=remained-%s where barcode=%s;"
            cursor.execute(query, (item['number'], item['barcode']))

            for account_id in account_id_list:
                query = "insert px_log (item_barcode, account_id, itemname, logtype, amount) values (%s,%s,(select * from (select itemname from px_item where barcode=%s) AS nameofitem),%s,%s);"
                cursor.execute(query, (item['barcode'], account_id, item['barcode'], 1, int(item['number'])/account_number))

        cursor.execute(accountUpdateQuery, tuple(account_params))
        
        self.con.commit()
        cursor.close()

    def refundItemInternal(self, account_id_list, item_list):
        account_number = len(account_id_list)
        account_params = []
        barcode_params = []

        cursor = self.con.cursor()
        
        priceSelectQuery = "select barcode, price from px_item where barcode in ("
        for item in item_list:
            priceSelectQuery += "%s,"
            barcode_params.append(item['barcode'])
            
        priceSelectQuery = priceSelectQuery[0:len(priceSelectQuery)-1]
        priceSelectQuery += ");"
        cursor.execute(priceSelectQuery, tuple(barcode_params))
        data = self.fetchallDict(cursor)

        if cursor.rowcount <= 0:
            raise ex.NotExistException()

        all_price = 0
        for d in data:
            for item in item_list:
                if item['barcode'] == d['barcode']:
                    if int(item['number']) < 0:
                        raise ex.AmountMinusException()
                    all_price += int(d['price'])*int(item['number'])
                    break


        for item in item_list:
            query = "update px_item SET remained=remained+%s where barcode=%s;"
            cursor.execute(query, (item['number'], item['barcode']))

            for account_id in account_id_list:
                query = "insert px_log (item_barcode, account_id, itemname, logtype, amount) values (%s,%s,(select * from (select itemname from px_item where barcode=%s) AS nameofitem),%s,%s);"
                cursor.execute(query, (item['barcode'], account_id, item['barcode'], 2, int(item['number'])/account_number))

                
        
        account_params.append(str(int(all_price)/account_number))
        accountUpdateQuery = "update account SET px_amount=px_amount+%s where id in ("
        for account in account_id_list:
            accountUpdateQuery += "%s,"
            account_params.append(account)
            
        accountUpdateQuery = accountUpdateQuery[0:len(accountUpdateQuery)-1]
        accountUpdateQuery += ");"
        
        cursor.execute(accountUpdateQuery, tuple(account_params))

        self.con.commit()
        cursor.close()

    def chargePXInternal(self, account_id, amount):
        if int(amount) < 0:
            raise ex.AmountMinusException()

        cursor = self.con.cursor()
        query = "update account SET px_amount=px_amount+%s where id=%s;"
        cursor.execute(query, (amount, account_id))

        query = "insert px_log (item_barcode, account_id, itemname, logtype, amount) values (null,%s,null,%s,%s);"
        cursor.execute(query, (account_id, 3, amount))

        self.con.commit()
        cursor.close()

    def getPXLogInternal(self, start_time, end_time):
        cursor = self.con.cursor()
        query = "select * from px_log where insert_time>=%s and insert_time<=%s;"
        cursor.execute(query, (start_time, end_time))
        rows = self.fetchallDict(cursor)
        
        if cursor.rowcount <= 0:
            cursor.close()
            raise ex.NotExistException()
            
        cursor.close()
        return rows

    def getPXLogbyIDInternal(self, account_id, start_time, end_time):
        cursor = self.con.cursor()
        query = "select * from px_log where insert_time>=%s and insert_time<=%s and account_id=%s;"
        cursor.execute(query, (start_time, end_time, account_id))
        rows = self.fetchallDict(cursor)
        
        if cursor.rowcount <= 0:
            cursor.close()
            raise ex.NotExistException()
            
        cursor.close()
        return rows
