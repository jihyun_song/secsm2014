import config as CONFIG
import mysql.connector
import common_exceptions as ex
import commons
import json
from config import Grant as g


class BoardDAO:
    def __init__(self):
        self.con = mysql.connector.connect(user=CONFIG.DATABASE_USER, password=CONFIG.DATABASE_PASSWD, use_unicode=True, charset='utf8')
        cursor = self.con.cursor()
        cursor.execute("use %(used_database)s;" % {'used_database' : CONFIG.DATABASE_DB})
        cursor.execute("set names utf8")
        cursor.close()

    def __del__(self):
        self.con.close()

    def fetchoneDict(self, cursor):
        row = cursor.fetchone()
        if row is None:
            return None
        else:
            cols = [ d[0] for d in cursor.description ]
            return dict(zip(cols, row))

    def fetchallDict(self, cursor):
        rows=[]
        row = self.fetchoneDict(cursor)
        while row is not None:
            rows.append(row)
            row = self.fetchoneDict(cursor)
        return rows


    def writeAllBoardInternal(self, sig_id, account_id, title, content, boardtype):
        cursor = self.con.cursor()
        query = "insert into board (sig_id, account_id, title, content, boardtype) values (%s, %s, %s, %s, %s);"
        cursor.execute(query, (sig_id, account_id, title, content, boardtype))
        self.con.commit()
        cursor.close()

    def getAllBoardListInternal(self, sig_id, boardtype, page_num, page_size):
        cursor = self.con.cursor()
        query = ""
        if sig_id is None:
            query = "select count(*) as cnt from board where board.sig_id is null and board.boardtype=%s;"
            cursor.execute(query, (boardtype,))
        else:
            query = "select count(*) as cnt from board where board.sig_id=%s and board.boardtype=%s;"
            cursor.execute(query, (sig_id, boardtype))
            
        data = self.fetchoneDict(cursor)
        
        if int(data['cnt']) <= 0:
            cursor.close()
            raise ex.NotExistException()
            
        mod = int(data['cnt'])%page_size
        
        data['cnt'] = int(data['cnt'])/page_size
        if mod != 0:
            data['cnt'] += 1
        
        if sig_id is None:
            query = "select board.id as board_id, board.title as title, board.insert_time as insert_time, account.entered as entered, account.username as username, board.account_id as account_id from board,account where board.account_id=account.id and board.sig_id is null and board.boardtype=%s order by insert_time desc limit %s,%s;"
            cursor.execute(query, (boardtype, (int(page_num)-1)*int(page_size), page_size))
        else:
            query = "select board.id as board_id, board.title as title, board.insert_time as insert_time, account.entered as entered, account.username as username, board.account_id as account_id from board,account where board.account_id=account.id and board.sig_id=%s and board.boardtype=%s order by insert_time desc limit %s,%s;"
            cursor.execute(query, (sig_id, boardtype, (int(page_num)-1)*int(page_size), page_size))
        
        data['list'] = self.fetchallDict(cursor)
        
        cursor.close()
        return data

    def getAllBoardDetailInternal(self, board_id):
        cursor = self.con.cursor()
        query = "select board.id as board_id, board.title as title, board.insert_time as insert_time, account.entered as entered, account.username as username, board.account_id as account_id, board.content as content from board,account where board.account_id=account.id and board.id=%s;"
        cursor.execute(query, (board_id,))
        board_data = self.fetchoneDict(cursor)
        
        if cursor.rowcount <= 0:
            cursor.close()
            raise ex.NotExistException()
            
        query = "select boardcomment.id as comment_id, boardcomment.insert_time as insert_time, account.entered as entered, account.username as username, boardcomment.account_id as account_id, boardcomment.content as content from boardcomment,account where boardcomment.account_id=account.id and boardcomment.board_id=%s order by insert_time;"
        cursor.execute(query, (board_id,))
        comment_data = self.fetchallDict(cursor)
        
        data = dict()
        data['board'] = board_data
        data['comment'] = comment_data
            
        cursor.close()
        return data
        
    def writeCommentInternal(self, board_id, account_id, content):
        cursor = self.con.cursor()
        query = "insert into boardcomment (account_id, board_id, content) values (%s, %s, %s);"
        cursor.execute(query, (account_id, board_id, content))
        self.con.commit()
        cursor.close()
        
    def deleteCommentInternal(self, comment_id):
        cursor = self.con.cursor()
        query = "delete from boardcomment where id=%s;"
        cursor.execute(query, (comment_id,))
        self.con.commit()
        cursor.close()
