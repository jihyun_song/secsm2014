import config as CONFIG
import mysql.connector
import common_exceptions as ex
import commons
import json
from config import Grant as g


class LivingDAO:
    def __init__(self):
        self.con = mysql.connector.connect(user=CONFIG.DATABASE_USER, password=CONFIG.DATABASE_PASSWD, use_unicode=True, charset='utf8')
        cursor = self.con.cursor()
        cursor.execute("use %(used_database)s;" % {'used_database' : CONFIG.DATABASE_DB})
        cursor.execute("set names utf8")
        cursor.close()

    def __del__(self):
        self.con.close()

    def fetchoneDict(self, cursor):
        row = cursor.fetchone()
        if row is None:
            return None
        else:
            cols = [ d[0] for d in cursor.description ]
            return dict(zip(cols, row))

    def fetchallDict(self, cursor):
        rows=[]
        row = self.fetchoneDict(cursor)
        while row is not None:
            rows.append(row)
            row = self.fetchoneDict(cursor)
        return rows
        
    def setNoCountDayInternal(self, attenddate, reason):
        cursor = self.con.cursor()
        checkQuery = "select * from attendance_no_count where attenddate=%s;"
        cursor.execute(checkQuery, (attenddate,))
        cursor.fetchall()

        if cursor.rowcount <= 0:
            query = "insert into attendance_no_count (attenddate, reason) values (%s, %s);"
            cursor.execute(query, (attenddate, reason))
        else:
            query = "update attendance set reason=%s where attenddate=%s;"
            cursor.execute(query, (reason, attenddate))

        self.con.commit()
        cursor.close()


    def getNoCountDaysInternal(self, Month):
        cursor = self.con.cursor()
        startmonth = Month[:8] + "01"
        endmonth = Month[:8]
        days = 0

        endmonth = commons.get_last_day_of_month(Month)

        query = "select * from attendance_no_count where attenddate>=%s and attenddate<=%s;"
        params = []
        params.append(startmonth)
        params.append(endmonth)

        cursor.execute(query, tuple(params))
            
        rows = self.fetchallDict(cursor)
        
        if cursor.rowcount <= 0:
            cursor.close()
            raise ex.NotExistException()
            
        cursor.close()
        return rows


    def setAttendanceInternal(self, account_id, attenddate, attendance):
        cursor = self.con.cursor()
        checkQuery = "select * from attendance where account_id=%s and attenddate=%s;"
        cursor.execute(checkQuery, (account_id, attenddate))
        cursor.fetchall()

        if cursor.rowcount <= 0:
            if attendance == 1:
                query = "insert into attendance (account_id, attenddate) values (%s, %s);"
                cursor.execute(query, (account_id, attenddate))
        else:
            if attendance == 0:
                query = "delete from attendance where account_id=%s and attenddate=%s;"
                cursor.execute(query, (account_id, attenddate))

        self.con.commit()
        cursor.close()

        
    def getAttendanceInternal(self, account_id, Month):
        cursor = self.con.cursor()
        startmonth = Month[:8] + "01"

        endmonth = commons.get_last_day_of_month(Month)
        query = "select account.username as name, account.entered as entered, attendance.attenddate as attenddate from attendance, account  where account_id=%s and account_id=account.id and attenddate>=%s and attenddate<=%s"
        cursor.execute(query, (account_id,startmonth,endmonth))
            
        rows = self.fetchallDict(cursor)
        
        if cursor.rowcount <= 0:
            cursor.close()
            raise ex.NotExistException()
            
        data = dict()
        data['cnt'] = cursor.rowcount
        data['rate'] = cursor.rowcount
        data['list'] = rows
            
        cursor.close()
        return data


    def getAttendanceNumberInternal(self, Month):
        cursor = self.con.cursor()
        startmonth = Month[:8] + "01"
        endmonth = Month[:8]

        if endmonth[5:7] == "02":
            endmonth += "28"
        elif endmonth[5:7] == "01" or endmonth[5:7] == "03" or endmonth[5:7] == "05" or endmonth[5:7] == "07" or endmonth[5:7] == "08" or endmonth[5:7] == "10" or endmonth[5:7] == "12":
            endmonth += "31"
        else:
            endmonth += "30"

        query = "select attendance.account_id as account_id, account.entered as entered, account.username as username, count(attendance.account_id) as cnt from attendance, account where account.id=attendance.account_id and attenddate>=%s and attenddate<=%s group by account_id order by account_id;"
        params = []
        params.append(startmonth)
        params.append(endmonth)

        cursor.execute(query, tuple(params))
        row = self.fetchoneDict(cursor)
        
        if cursor.rowcount <= 0:
            cursor.close()
            raise ex.NotExistException()

        count = 0
        sum = 0
        rows = []
        while row is not None:
            count += 1
            sum += row['cnt']
            rows.append(row)
            row = self.fetchoneDict(cursor)

        rowval = 0
        if count != 0:
            rowval = sum/count
            
        data = dict()
        data['list'] = rows
        data['avg'] = rowval
        
        cursor.close()
        return data


    def getAttendanceDaysInternal(self, Month):
        cursor = self.con.cursor()
        startmonth = Month[:8] + "01"
        endmonth = Month[:8]
        days = 0

        if endmonth[5:7] == "02":
            endmonth += "28"
            days = 28
        elif endmonth[5:7] == "01" or endmonth[5:7] == "03" or endmonth[5:7] == "05" or endmonth[5:7] == "07" or endmonth[5:7] == "08" or endmonth[5:7] == "10" or endmonth[5:7] == "12":
            endmonth += "31"
            days = 31
        else:
            endmonth += "30"
            days = 30

        query = "select count(attenddate) as cnt from attendance_no_count where attenddate>=%s and attenddate<=%s;"
        params = []
        params.append(startmonth)
        params.append(endmonth)

        cursor.execute(query, tuple(params))
        row = self.fetchoneDict(cursor)
        
        if cursor.rowcount <= 0:
            cursor.close()
            raise ex.NotExistException()
            
        cursor.close()
        return days - row['cnt']


    def setDutyInternal(self, dutydate, m1_account_id, m2_account_id, m3_account_id, m4_account_id, f1_account_id, f2_account_id, f3_account_id, f4_account_id):
        cursor = self.con.cursor()
        checkQuery = "select * from duty where dutydate=%s;"
        cursor.execute(checkQuery, (dutydate,))
        cursor.fetchall()

        if cursor.rowcount != 0:
            deletequery = "delete from duty where dutydate=%s;"
            cursor.execute(deletequery, (dutydate,))
            self.con.commit()

        query = "insert into duty SET dutydate=%s, m1_account_id=%s, m1_username=(select * from (select username from account where id=%s) as name1), m1_phone=(select * from (select phone from account where id=%s) as phone1)"

        params = []
        params.append(dutydate)
        params.append(m1_account_id)
        params.append(m1_account_id)
        params.append(m1_account_id)

        if m2_account_id is not None:
            query += ", m2_account_id=%s, m2_username=(select * from (select username from account where id=%s) as name2), m2_phone=(select * from (select phone from account where id=%s) as phone2)"
            params.append(m2_account_id)
            params.append(m2_account_id)
            params.append(m2_account_id)

        if m3_account_id is not None:
            query += ", m3_account_id=%s, m3_username=(select * from (select username from account where id=%s) as name3), m3_phone=(select * from (select phone from account where id=%s) as phone3)"
            params.append(m3_account_id)
            params.append(m3_account_id)
            params.append(m3_account_id)

        if m4_account_id is not None:
            query += ", m4_account_id=%s, m4_username=(select * from (select username from account where id=%s) as name4), m4_phone=(select * from (select phone from account where id=%s) as phone4)"
            params.append(m4_account_id)
            params.append(m4_account_id)
            params.append(m4_account_id)

        if f1_account_id is not None:
            query += ", f1_account_id=%s, f1_username=(select * from (select username from account where id=%s) as name5), f1_phone=(select * from (select phone from account where id=%s) as phone5)"
            params.append(f1_account_id)
            params.append(f1_account_id)
            params.append(f1_account_id)

        if f2_account_id is not None:
            query += ", f2_account_id=%s, f2_username=(select * from (select username from account where id=%s) as name6), f2_phone=(select * from (select phone from account where id=%s) as phone6)"
            params.append(f2_account_id)
            params.append(f2_account_id)
            params.append(f2_account_id)

        if f3_account_id is not None:
            query += ", f3_account_id=%s, f3_username=(select * from (select username from account where id=%s) as name7), f3_phone=(select * from (select phone from account where id=%s) as phone7)"
            params.append(f3_account_id)
            params.append(f3_account_id)
            params.append(f3_account_id)

        if f4_account_id is not None:
            query += ", f4_account_id=%s, f4_username=(select * from (select username from account where id=%s) as name8), f4_phone=(select * from (select phone from account where id=%s) as phone8)"
            params.append(f4_account_id)
            params.append(f4_account_id)
            params.append(f4_account_id)

        query += ";"
        cursor.execute(query, tuple(params))

        self.con.commit()
        cursor.close()


    def getDutyInternal(self, Month, account_id):
        cursor = self.con.cursor()
        startmonth = Month[:8] + "01"
        endmonth = Month[:8]

        if endmonth[5:7] == "02":
            endmonth += "28"
        elif endmonth[5:7] == "01" or endmonth[5:7] == "03" or endmonth[5:7] == "05" or endmonth[5:7] == "07" or endmonth[5:7] == "08" or endmonth[5:7] == "10" or endmonth[5:7] == "12":
            endmonth += "31"
        else:
            endmonth += "30"

        query = "select * from duty where dutydate>=%s and dutydate<=%s"
        params = []
        params.append(startmonth)
        params.append(endmonth)

        if account_id is not None:
            query += " and (m1_account_id=%s or m2_account_id=%s or m3_account_id=%s or m4_account_id=%s or f1_account_id=%s or f2_account_id=%s or f3_account_id=%s or f4_account_id=%s)"
            params.append(account_id)
            params.append(account_id)
            params.append(account_id)
            params.append(account_id)
            params.append(account_id)
            params.append(account_id)
            params.append(account_id)
            params.append(account_id)

        query += ";"
        cursor.execute(query, tuple(params))
        rows = self.fetchallDict(cursor)
        
        if cursor.rowcount <= 0:
            cursor.close()
            raise ex.NotExistException()
            
        cursor.close()
        return rows

