import config as CONFIG
import mysql.connector
import common_exceptions as ex
import commons
import json
from config import Grant as g


class EducationDAO:
    def __init__(self):
        self.con = mysql.connector.connect(user=CONFIG.DATABASE_USER, password=CONFIG.DATABASE_PASSWD, use_unicode=True, charset='utf8')
        cursor = self.con.cursor()
        cursor.execute("use %(used_database)s;" % {'used_database' : CONFIG.DATABASE_DB})
        cursor.execute("set names utf8")
        cursor.close()

    def __del__(self):
        self.con.close()

    def fetchoneDict(self, cursor):
        row = cursor.fetchone()
        if row is None:
            return None
        else:
            cols = [ d[0] for d in cursor.description ]
            return dict(zip(cols, row))

    def fetchallDict(self, cursor):
        rows=[]
        row = self.fetchoneDict(cursor)
        while row is not None:
            rows.append(row)
            row = self.fetchoneDict(cursor)
        return rows

    def createProjectInternal(self, ProjectName, Summary, StartMonth, EndMonth, Creative_Rate, Technical_Rate, Useful_Rate, Successful_Rate, PL, Team1, Team2, Team3, Team4):
        cursor = self.con.cursor()
        query = "insert into project (ProjectName, Summary, StartMonth, EndMonth, Creative_Rate, Technical_Rate, Useful_Rate, Successful_Rate, PL, PLName, Team1, Team1Name"

        if Team2 is None:
            query += ") VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,(select * from (select username from account where id=%s) AS PLName),%s,(select * from (select username from account where id=%s) AS Team1Name));"
            cursor.execute(query, (ProjectName, Summary, StartMonth, EndMonth, Creative_Rate, Technical_Rate, Useful_Rate, Successful_Rate, PL, PL, Team1, Team1))
        elif Team3 is None:
            query += ",Team2, Team2Name) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,(select * from (select username from account where id=%s) AS Team1Name),%s,(select * from (select username from account where id=%s) AS Team1Name),%s,(select * from (select username from account where id=%s) AS Team2Name));"
            cursor.execute(query, (ProjectName, Summary, StartMonth, EndMonth, Creative_Rate, Technical_Rate, Useful_Rate, Successful_Rate, PL, PL, Team1, Team1, Team2, Team2))
        elif Team4 is None:
            query += ",Team2, Team2Name, Team3, Team3Name) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,(select * from (select username from account where id=%s) AS Team1Name),%s,(select * from (select username from account where id=%s) AS Team1Name),%s,(select * from (select username from account where id=%s) AS Team2Name),%s,(select * from (select username from account where id=%s) AS Team3Name));"
            cursor.execute(query, (ProjectName, Summary, StartMonth, EndMonth, Creative_Rate, Technical_Rate, Useful_Rate, Successful_Rate, PL, PL, Team1, Team1, Team2, Team2, Team3, Team3))
        else:
            query += ",Team2, Team2Name, Team3, Team3Name, Team4, Team4Name) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,(select * from (select username from account where id=%s) AS Team1Name),%s,(select * from (select username from account where id=%s) AS Team1Name),%s,(select * from (select username from account where id=%s) AS Team2Name),%s,(select * from (select username from account where id=%s) AS Team3Name),%s,(select * from (select username from account where id=%s) AS Team4Name));"
            cursor.execute(query, (ProjectName, Summary, StartMonth, EndMonth, Creative_Rate, Technical_Rate, Useful_Rate, Successful_Rate, PL, PL, Team1, Team1, Team2, Team2, Team3, Team3, Team4, Team4))

        self.con.commit()

        project_ID = cursor.lastrowid

        query_presentation = "insert into presentation (project_id, presentation_type) values (%s, %s);"
        cursor.execute(query_presentation, (project_ID, "1"))
        cursor.execute(query_presentation, (project_ID, "2"))
        cursor.execute(query_presentation, (project_ID, "3"))

        query_pms = "insert into pms (project_id, ProjectName, PMSType, Contents) values (%s, %s, %s, (select * from (select Contents from pms where id=%s) AS Cont));"
        cursor.execute(query_pms, (project_ID, ProjectName, "1", "1"))
        cursor.execute(query_pms, (project_ID, ProjectName, "2", "2"))
        cursor.execute(query_pms, (project_ID, ProjectName, "3", "3"))

        self.con.commit()
        cursor.close()
        
    def modifyProjectInternal(self, id, ProjectName, Summary, StartMonth, EndMonth, Creative_Rate, Technical_Rate, Useful_Rate, Successful_Rate, PL, Team1, Team2, Team3, Team4):
        cursor = self.con.cursor()
        query = "update project set "

        count = 0
        params=[]

        if ProjectName is not None:
            query += "ProjectName=%s"
            count += 1
            params.append(ProjectName)

        if Summary is not None:
            query += ",Summary=%s"
            count += 1
            params.append(Summary)

        if StartMonth is not None:
            query += ",StartMonth=%s"
            count += 1
            params.append(StartMonth)

        if EndMonth is not None:
            query += ",EndMonth=%s"
            count += 1
            params.append(EndMonth)

        if Creative_Rate is not None:
            query += ",Creative_Rate=%s"
            count += 1
            params.append(Creative_Rate)

        if Technical_Rate is not None:
            query += ",Technical_Rate=%s"
            count += 1
            params.append(Technical_Rate)

        if Useful_Rate is not None:
            query += ",Useful_Rate=%s"
            count += 1
            params.append(Useful_Rate)

        if Successful_Rate is not None:
            query += ",Successful_Rate=%s"
            count += 1
            params.append(Successful_Rate)

        if PL is not None:
            query += ",pl=%s, plname=(select * from (select username from account where id=%s) AS plname)"
            count += 1
            params.append(PL)
            params.append(PL)

        if Team1 is not None:
            query += ",team1=%s, team1name=(select * from (select username from account where id=%s) AS team1name)"
            count += 1
            params.append(Team1)
            params.append(Team1)

        if Team2 is not None:
            query += ",team2=%s, team2name=(select * from (select username from account where id=%s) AS team2name)"
            count += 1
            params.append(Team2)
            params.append(Team2)

        if Team3 is not None:
            query += ",team3=%s, team3name=(select * from (select username from account where id=%s) AS team3name)"
            count += 1
            params.append(Team3)
            params.append(Team3)

        if Team4 is not None:
            query += ",team4=%s, team4name=(select * from (select username from account where id=%s) AS team4name)"
            count += 1
            params.append(Team4)
            params.append(Team4)

        if count == 0:
            cursor.close()
            return None
        else:
            query += " where id='%s';"
            params.append(id)

        cursor.execute(query, tuple(params))
        self.con.commit()
        cursor.close()

    def selectAllProjectsInternal(self):
        cursor = self.con.cursor()
        query = "select * from project;"
        cursor.execute(query)
        
        rows = self.fetchallDict(cursor)
        
        if cursor.rowcount <= 0:
            cursor.close()
            raise ex.NotExistException()
            
        cursor.close()
        return rows

    def selectProjectInternal(self, id):
        cursor = self.con.cursor()
        query = "select * from project where id=%s;"
        cursor.execute(query, (id,))
        row = self.fetchoneDict(cursor)
        
        if cursor.rowcount <= 0:
            cursor.close()
            raise ex.NotExistException()
        
        cursor.close()
        return row

    def deleteProjectInternal(self, id):
        cursor = self.con.cursor()
        query = "delete from project where id=%s;"
        cursor.execute(query, (id,))
        self.con.commit()
        cursor.close()
        return rows

    def searchProjectInternal(self, Keyword):
        cursor = self.con.cursor()
        query = "select * from project where ProjectName like %s or Summary like %s;"
        cursor.execute(query, ('%' + Keyword + '%', '%' + Keyword + '%'))
        rows = self.fetchallDict(cursor)
        
        if cursor.rowcount <= 0:
            cursor.close()
            raise ex.NotExistException()
            
        cursor.close()
        return rows

    def registJurorInternal(self, account_id, project_id, presentation_type):
        cursor = self.con.cursor()
        selectQuery = "select id from presentation where project_id=%s and presentation_type=%s;"
        cursor.execute(selectQuery, (project_id, presentation_type))
        Presentation_ID = cursor.fetchone()[0]
        
        selectQuery = "select account_id from presentationattendance where account_id=%s and presentation_id=%s;"
        cursor.execute(selectQuery, (account_id, Presentation_ID))
        cursor.fetchall()
        
        if cursor.rowcount > 0:
            raise ex.AlreadyExistException()
        
        query = "insert into presentationattendance (account_id, presentation_id) values (%s, %s);"
        cursor.execute(query, (account_id, Presentation_ID))
        self.con.commit()
        cursor.close()

    def getJurorInternal(self, project_id):
        cursor = self.con.cursor()
        query = "select presentationattendance.account_id as account_id, account.username as username, account.entered as entered, presentation.presentation_type as presentation_type from presentationattendance,presentation,account where presentationattendance.presentation_id=presentation.id and presentation.project_id=%s and presentationattendance.account_id=account.id;"
        cursor.execute(query, (project_id,))
        rows = self.fetchallDict(cursor)
        
        if cursor.rowcount <= 0:
            cursor.close()
            raise ex.NotExistException()
            
        cursor.close()
        return rows

    def getAllJurorInternal(self):
        cursor = self.con.cursor()
        user_query = "select id as account_id, username, entered from account;";
        cursor.execute(user_query)
        data = self.fetchallDict(cursor)
        
        for d in data:
            d['list'] = []
        
        print("asdf");
        query = "select presentationattendance.account_id as account_id, presentation.project_id as project_id, project.projectname as projectname, presentation.presentation_type as presentation_type from presentationattendance,presentation,project where presentationattendance.presentation_id=presentation.id and project.id=presentation.project_id;"
        cursor.execute(query)
        print("asdf");
        
        count = 0
        row = self.fetchoneDict(cursor)
        while row is not None:
            
            juror_data = None
            if row['project_id'] is not None:
                juror_data = dict()
                juror_data['project_id'] = row['project_id']
                juror_data['projectname'] = row['projectname']
                juror_data['presentation_type'] = row['presentation_type']
        
            exist = False
            for d in data:
                if d['account_id'] == row['account_id']:
                    d['list'].append(juror_data)
                    break
                
            count += 1
            row = self.fetchoneDict(cursor)
                
        if count <= 0:
            cursor.close()
            raise ex.NotExistException()
                    
        cursor.close()
        return data

        
    def deleteJurorInternal(self, project_id, presentation_type, account_id):
        cursor = self.con.cursor()
        query = "delete from presentationattendance where presentationattendance.presentation_id=(select * from (select id from presentation where project_id=%s AND presentation_type=%s) AS presentation_id) AND account_id=%s;"
        cursor.execute(query, (project_id, presentation_type, account_id))
        self.con.commit()
        cursor.close()

    def getJurorByAccountInternal(self, account_id):
        cursor = self.con.cursor()
        query = "select account.id as account_id, account.username as username, account.entered as entered, presentation.project_id as project_id, project.projectname as projectname, presentation.presentation_type as presentation_type from presentationattendance,project,presentation,account where presentationattendance.presentation_id=presentation.id and presentationattendance.account_id=%s and account.id=presentationattendance.account_id;"
        cursor.execute(query, (account_id,))
        rows = self.fetchallDict(cursor)
        
        if cursor.rowcount <= 0:
            cursor.close()
            raise ex.NotExistException()
            
        cursor.close()
        return rows
        
    def modifyAppraiseContentsInternal(self, project_id, presentation_type, contents):
        cursor = self.con.cursor()
        query = "update pms SET contents=%s where project_id=%s and pmstype=%s;"
        cursor.execute(query, (json.dumps(contents), project_id, presentation_type))
        self.con.commit()
        cursor.close()
        cursor = self.con.cursor()
        query = "delete from presentationattendance where presentationattendance.presentation_id=(select * from (select id from presentation where project_id=%s AND presentation_type=%s) AS presentation_id) AND account_id=%s;"
        cursor.execute(query, (project_id, presentation_type, account_id))
        self.con.commit()
        cursor.close()


    def getAppraiseContentsInternal(self, project_id, presentation_type):
        cursor = self.con.cursor()
        query = "select id,contents from pms where project_id=%s and pmstype=%s;"
        cursor.execute(query, (project_id,presentation_type))
        row = self.fetchoneDict(cursor)
        
        if cursor.rowcount <= 0:
            cursor.close()
            raise ex.NotExistException()
            
        print("here?")
        print(row['contents'])
        row['contents'] = json.loads(row['contents'])
        print("here!")
        
        cursor.close()
        return row


    def appraiseProjectInternal(self, account_id, appraise_id, jurorAppraiseResult):
        cursor = self.con.cursor()
        selectQuery = "select * from pmsresult where account_id=%s and pms_id=%s;"
        cursor.execute(selectQuery, (account_id, appraise_id))
        
        self.fetchallDict(cursor)
        
        if cursor.rowcount > 0:
            self.fetchallDict(cursor)
            cursor.close()
            raise ex.AlreadyExistException()
            
            

        query = "select * from presentationattendance as pre_att, presentation as pre, pms where pre.presentation_type=pms.pmstype and pre.project_id=pms.project_id and pre_att.account_id=%s and pms.id=%s and pre.id=pre_att.presentation_id;"
        cursor.execute(query, (account_id,appraise_id))
        
        self.fetchallDict(cursor)
        
        if cursor.rowcount <= 0:
            self.fetchallDict(cursor)
            cursor.close()
            raise ex.NotExistException()
            

        query = "insert into pmsresult (account_id, pms_id, contents) values (%s, %s, %s);"
        cursor.execute(query, (account_id, appraise_id, json.dumps(jurorAppraiseResult)))
        self.con.commit()
        cursor.close()


    def getAppraiseResultInternal(self, project_id, presentation_type):
        cursor = self.con.cursor()
        
        projectInfoQuery = "select creative_rate,useful_rate,technical_rate,successful_rate from project where id=%s;"
        cursor.execute(projectInfoQuery, (project_id,))
        rateInfo = self.fetchoneDict(cursor)
        
        if cursor.rowcount <= 0:
            cursor.close()
            raise ex.NotExistException()
        
        
        query = "select account.username as username,account.entered as entered,pmsresult.account_id as account_id, account.accountgrant as accountgrant, pmsresult.contents as contents from pmsresult, account, pms where pms.project_id=%s and pms.pmstype=%s and pmsresult.pms_id=pms.id and account.id=pmsresult.account_id;"
        cursor.execute(query, (project_id, presentation_type))

        jurorCount = 0
        jurorAppraiseResult = dict()
        jurorAppraiseResult['creative_score'] = 0.0
        jurorAppraiseResult['useful_score'] = 0.0
        jurorAppraiseResult['technical_score'] = 0.0
        jurorAppraiseResult['successful_score'] = 0.0
        jurorAppraiseResult['final_score'] = 0.0
        
        eduCount = 0
        eduAppraiseResult = dict()
        eduAppraiseResult['creative_score'] = 0.0
        eduAppraiseResult['useful_score'] = 0.0
        eduAppraiseResult['technical_score'] = 0.0
        eduAppraiseResult['successful_score'] = 0.0
        eduAppraiseResult['final_score'] = 0.0
        
        final_score = 0.0
        
        education_ratio = CONFIG.EDUCATION_RATIO
        juror_ratio = 1.0-CONFIG.EDUCATION_RATIO
        
        rows = []
        row = self.fetchoneDict(cursor)
        
        if cursor.rowcount <= 0:
            cursor.close()
            raise ex.NotExistException()
            
        while row is not None:
            row['final_score'] = 0.0
            row['contents'] = json.loads(row['contents'])
            
            if (row['accountgrant'] in g.education_chief) or (row['accountgrant'] in g.education_depart):
                eduCount += 1
                for resultInfo in row['contents']:
                    if resultInfo['num'] == 1:
                        eduAppraiseResult['creative_score'] += float(resultInfo['result'])
                        row['creative_score'] = float(resultInfo['result'])
                    elif resultInfo['num'] == 2:
                        eduAppraiseResult['useful_score'] += float(resultInfo['result'])
                        row['useful_score'] = float(resultInfo['result'])
                    elif resultInfo['num'] == 3:
                        eduAppraiseResult['technical_score'] += float(resultInfo['result'])
                        row['technical_score'] = float(resultInfo['result'])
                    elif resultInfo['num'] == 4:
                        eduAppraiseResult['successful_score'] += float(resultInfo['result'])
                        row['successful_score'] = float(resultInfo['result'])
                    elif resultInfo['num'] == 5:
                        row['feedback'] = resultInfo['result']
            else:
                jurorCount += 1
                for resultInfo in row['contents']:
                    if resultInfo['num'] == 1:
                        jurorAppraiseResult['creative_score'] += float(resultInfo['result'])
                        row['creative_score'] = resultInfo['result']
                    elif resultInfo['num'] == 2:
                        jurorAppraiseResult['useful_score'] += float(resultInfo['result'])
                        row['useful_score'] = resultInfo['result']
                    elif resultInfo['num'] == 3:
                        jurorAppraiseResult['technical_score'] += float(resultInfo['result'])
                        row['technical_score'] = resultInfo['result']
                    elif resultInfo['num'] == 4:
                        jurorAppraiseResult['successful_score'] += float(resultInfo['result'])
                        row['successful_score'] = resultInfo['result']
                    elif resultInfo['num'] == 5:
                        row['feedback'] = resultInfo['result']
                        
            row['final_score'] = float(row['creative_score'])*float(rateInfo['creative_rate'])/100.0 + float(row['useful_score'])*float(rateInfo['useful_rate'])/100.0 + float(row['technical_score'])*float(rateInfo['technical_rate'])/100.0 + float(row['successful_score'])*float(rateInfo['successful_rate'])/100.0
            rows.append(row)
            row = self.fetchoneDict(cursor)
            
        ### calc education ###
        if eduCount != 0:
            eduAppraiseResult['creative_score'] /= eduCount
            eduAppraiseResult['useful_score'] /= eduCount
            eduAppraiseResult['technical_score'] /= eduCount
            eduAppraiseResult['successful_score'] /= eduCount
            eduAppraiseResult['final_score'] = eduAppraiseResult['creative_score']*float(rateInfo['creative_rate'])/100.0 + eduAppraiseResult['useful_score']*float(rateInfo['useful_rate'])/100.0 + eduAppraiseResult['technical_score']*float(rateInfo['technical_rate'])/100.0 + eduAppraiseResult['successful_score']*float(rateInfo['successful_rate'])/100.0
        
        ### calc juror ###
        if jurorCount != 0:
            jurorAppraiseResult['creative_score'] /= jurorCount
            jurorAppraiseResult['useful_score'] /= jurorCount
            jurorAppraiseResult['technical_score'] /= jurorCount
            jurorAppraiseResult['successful_score'] /= jurorCount
            jurorAppraiseResult['final_score'] = jurorAppraiseResult['creative_score']*float(rateInfo['creative_rate'])/100.0 + jurorAppraiseResult['useful_score']*float(rateInfo['useful_rate'])/100.0 + jurorAppraiseResult['technical_score']*float(rateInfo['technical_rate'])/100.0 + jurorAppraiseResult['successful_score']*float(rateInfo['successful_rate'])/100.0
        
        ### calc all ###  
        allAppraiseResult = dict()
        
        if eduCount == 0:
            allAppraiseResult['creative_score'] = jurorAppraiseResult['creative_score']
            allAppraiseResult['useful_score'] = jurorAppraiseResult['useful_score']
            allAppraiseResult['technical_score'] = jurorAppraiseResult['technical_score']
            allAppraiseResult['successful_score'] = jurorAppraiseResult['successful_score']
            allAppraiseResult['final_score'] = jurorAppraiseResult['final_score']
        elif jurorCount == 0:
            allAppraiseResult['creative_score'] = eduAppraiseResult['creative_score']
            allAppraiseResult['useful_score'] = eduAppraiseResult['useful_score']
            allAppraiseResult['technical_score'] = eduAppraiseResult['technical_score']
            allAppraiseResult['successful_score'] = eduAppraiseResult['successful_score']
            allAppraiseResult['final_score'] = eduAppraiseResult['final_score']
        else:        
            allAppraiseResult['creative_score'] = eduAppraiseResult['creative_score']*education_ratio + jurorAppraiseResult['creative_score']*juror_ratio
            allAppraiseResult['useful_score'] = eduAppraiseResult['useful_score']*education_ratio + jurorAppraiseResult['useful_score']*juror_ratio
            allAppraiseResult['technical_score'] = eduAppraiseResult['technical_score']*education_ratio + jurorAppraiseResult['technical_score']*juror_ratio
            allAppraiseResult['successful_score'] = eduAppraiseResult['successful_score']*education_ratio + jurorAppraiseResult['successful_score']*juror_ratio
            allAppraiseResult['final_score'] = allAppraiseResult['creative_score']*float(rateInfo['creative_rate'])/100.0 + allAppraiseResult['useful_score']*float(rateInfo['useful_rate'])/100.0 + allAppraiseResult['technical_score']*float(rateInfo['technical_rate'])/100.0 + allAppraiseResult['successful_score']*float(rateInfo['successful_rate'])/100.0

        allResult = dict();
        
        allResult['list'] = rows
        allResult['edu'] = eduAppraiseResult
        allResult['juror'] = jurorAppraiseResult
        allResult['all'] = allAppraiseResult
        
        cursor.close()
        return allResult


    def setPresentationAttendanceInternal(self, account_id, project_id, presentation_type, attendance):
        cursor = self.con.cursor()

        selectQuery = "select id from presentation where project_id=%s and presentation_type=%s;"
        cursor.execute(selectQuery, (project_id, presentation_type))
        Presentation_ID = cursor.fetchone()[0]

        checkQuery = "select * from presentationattendance where Presentation_ID=%s;"
        cursor.execute(checkQuery, (Presentation_ID,))
        cursor.fetchall()

        if cursor.rowcount <= 0:
            query = "insert into presentationattendance (account_id, Presentation_ID, attendance) values (%s, %s, %s);"
            cursor.execute(query, (account_id, Presentation_ID, attendance))
        else:
            query = "update presentationattendance SET attendance=%s where account_id=%s and Presentation_ID=%s;"
            cursor.execute(query, (attendance, account_id, Presentation_ID))

        self.con.commit()
        cursor.close()


    def getPresentationAttendanceInternal(self, account_id):
        cursor = self.con.cursor()
        query = "select * from presentationattendance where account_id=%s;"
        cursor.execute(query, (account_id,))
        rows = self.fetchallDict(cursor)
        
        if cursor.rowcount <= 0:
            cursor.close()
            raise ex.NotExistException()
            
        cursor.close()
        return rows
