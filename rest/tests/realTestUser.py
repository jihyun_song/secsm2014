# -*- coding: utf-8 -*-
import requests
import json

def create_user(account_id, pwd, name, entered, googleID, birthday, university, phone, grant, ssn):
    # account create user (POST http://210.118.74.115:8080/accounts/)
    data = {}
    data['code'] = None
    data['id'] = account_id
    data['password'] = pwd
    data['name'] = name
    data['entered'] = entered
    data['googleID'] = googleID
    data['birthday'] = birthday
    data['university'] = university
    data['phone'] = phone
    data['grant'] = grant
    data['social_security_number'] = ssn

    send_url ="http://210.118.74.115:8080/accounts"

    send_json = json.dumps(data, default=str)

    try:
        r = requests.post(
            url = send_url,
            data = str(send_json)
        )
        print('Response HTTP Status Code : {status_code}'.format(status_code=r.status_code))
        print('Response HTTP Response Body : {content}'.format(content=r.content))
    except requests.exceptions.RequestException as e:
        print('HTTP Request failed')

        
create_user("qwefgh90", "secsm", "최창원", "24-2", "qwefgh90@naver.com", "1990-06-18", "인천대학교", "010-7314-4993", "1000", "9006181300111")
create_user("mysticPrg", "secsm", "황현구", "24-1", "mysticPrg@gmail.com", "1986-11-29", "인하대학교", "010-7241-2250", "1000", "8611291273126")
create_user("Kandelion", "secsm", "김현우", "24-1", "rlagusdn6575@gmail.com", "1993-07-28", "서강대학교", "010-9347-7574", "1000", "9307281853114")
create_user("hyesoo1104", "secsm", "오혜수", "24-1", "hyesoo1104@gmail.com", "1992-11-04", "인천대학교", "010-5118-1767", "1000", "9211042149838")
create_user("wonrst", "secsm", "류보원", "22-1", "wonrst@gmail.com", "1988-01-09", "홍익대학교", "010-8574-0106", "0", "8801091163723")
create_user("withsaemi", "secsm", "임새미", "24-2", "withsaemi@gmail.com", "1992-03-23", "숙명여자대학교", "010-9549-3132", "101", "9202202080522")
