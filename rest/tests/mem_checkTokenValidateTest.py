# -*- coding: utf-8 -*-
from certifier import Certifier

token = 'KwE7d7M8cGWPFXpncN'
print "Token : ", token
print "certi : ", Certifier.checkTokenValidate(token)   # token validate 확인(확인되면 갱신)
print "ID : " + Certifier.getIDFromToken(token)         # token 지정 ID 확인
print "Grant : ", Certifier.getGrantFromToken(token)    # token 권한 확인
print "time : ", Certifier.getTimeFromToken(token)      # token 생성 시간 확인



'''
$ python memtest.py
Token :  KwE7d7M8cGWPFXpncN
certi :  True
ID : test1
Grant :  1000
time :  1421423555.75
'''
