# -*- coding: utf-8 -*-
from certifier import Certifier

data = Certifier.getToken("test1", "ABCEDFG2")   # token을 포함한 유저 데이터
token = data['token']                       # token 가져오기

print "Token : ", token
print "certi : ", Certifier.checkTokenValidate(token)   # token validate 확인(확인되면 갱신)
print "ID : " + Certifier.getIDFromToken(token)         # token 지정 ID 확인
print "Grant : ", Certifier.getGrantFromToken(token)    # token 권한 확인
print "time : ", Certifier.getTimeFromToken(token)      # token 생성 시간 확인


'''
data = Certifier.getToken("testID", "0000")   # token을 포함한 유저 데이터
Token :  z9MiztoVNBTUJY4CTrD
certi :  True
ID : testID
Grant :  1000
time :  1421169478.18
'''
