# `pip install requests`

import requests
import time
import datetime
import sys, os, threading

RUN_COUNT=1000 # number of request
TEST_URL="http://210.118.74.115:8080/projects"
def send_request(test_url):
    # project list (GET http://210.118.74.115:8080/projects)

    try:
        r = requests.get(
            url=test_url,
        )
        print('Response HTTP Status Code : {status_code} at {datetime}'.format(status_code=r.status_code, datetime=datetime.datetime.now()))
        #print('Response HTTP Response Body : {content}'.format(content=r.content))
    except requests.exceptions.RequestException as e:
        print('HTTP Request failed')


def main(count):
    threads = []
    for c in range(0, count):
        t = threading.Thread(target=send_request, args=(TEST_URL,))
        t.start()
        threads.append(t)
    for t in threads:
        t.join()


if __name__=='__main__':

    start_time=time.time()
    main(RUN_COUNT)
    print ""
    print "main execution time: {exec_time}, request number is {iterate_count}".format(exec_time=time.time()-start_time, iterate_count=RUN_COUNT)
