# -*- coding: utf-8 -*-
import requests
import json

def create_user(account_id, pwd, name, entered, googleID, phone, grant):
    # account create user (POST http://210.118.74.115:8080/accounts/test6)
    data = {}
    data['id'] = account_id
    data['password'] = pwd
    data['name'] = name
    data['entered'] = entered
    data['googleID'] = googleID
    data['birthday'] = "1990-01-18"
    data['university'] = "신촌대"
    data['phone'] = phone
    data['grant'] = grant
    data['social_security_number'] = "123123-1231234"


    send_url ="http://210.118.74.115:8080/accounts/%s"%account_id

    send_json = json.dumps(data, default=str)

    try:
        r = requests.post(
            url = send_url,
            data = str(send_json)
        )
        print('Response HTTP Status Code : {status_code}'.format(status_code=r.status_code))
        print('Response HTTP Response Body : {content}'.format(content=r.content))
    except requests.exceptions.RequestException as e:
        print('HTTP Request failed')


if __name__ == "__main__":
    #create test user
    for i in range(6):
        create_user("test%s"%i, "QWER%s"%i, "테스트%s"%i, "2%s-1"%i, "test%s@gmail.com"%i, "010-1234-000%s"%i, 1000)

    #create juror
    for i in range(6):
        create_user("juror%s"%i, "QWER%s"%i, "배심원%s"%i, "2%s-1"%i, "juror%s@gmail.com"%i, "010-1234-000%s"%i, 1000)

    create_user("owner", "QWER", "운영자", "00-0", "owner@gamil.com", "010-1234-5678", 0)
