from datetime import timedelta
from flask import make_response, request, current_app
from functools import update_wrapper
import common_exceptions as ex
import mysql.connector
import commons

def set_grant(grant):
    def decorator(func):
        func._grant = grant
        return func
    return decorator

def crossdomain(origin=None, methods=None, headers=None,
                max_age=21600, attach_to_all=True,
                automatic_options=True):
    if methods is not None:
        methods = ', '.join(sorted(x.upper() for x in methods))
    if headers is not None and not isinstance(headers, basestring):
        headers = ', '.join(x.upper() for x in headers)
    if not isinstance(origin, basestring):
        origin = ', '.join(origin)
    if isinstance(max_age, timedelta):
        max_age = max_age.total_seconds()

    def get_methods():
        if methods is not None:
            return methods

        options_resp = current_app.make_default_options_response()
        return options_resp.headers['allow']

    def decorator(f):
        def wrapped_function(*args, **kwargs):
            if automatic_options and request.method == 'OPTIONS':
                resp = current_app.make_default_options_response()
            else:
                resp = make_response(f(*args, **kwargs))
            if not attach_to_all and request.method != 'OPTIONS':
                return resp

            h = resp.headers

            h['Access-Control-Allow-Origin'] = origin
            h['Access-Control-Allow-Methods'] = get_methods()
            h['Access-Control-Max-Age'] = str(max_age)
            if headers is not None:
                h['Access-Control-Allow-Headers'] = request.headers.get('Access-Control-Request-Headers', 'Authorization')
            return resp

        f.provide_automatic_options = False
        return update_wrapper(wrapped_function, f)
    return decorator

def common_exception_handler(f):
    def exception_decorator(*args, **kwargs):
        try:
            return f(*args, **kwargs)
        except ex.RequestMissingException as e:
            print e
            return commons.body(message=e.msg, status=e.status)
        except ex.RequestMismatchException as e:
            print e
            return commons.body(message=e.msg, status=e.status)
        except ex.DateValidateException as e:
            print e
            return commons.body(message=e.msg, status=e.status)
        except ex.DatetimeValidateException as e:
            print e
            return commons.body(message=e.msg, status=e.status)
        except ex.NotExistException as e:
            print e
            return commons.body(message=e.msg, status=e.status)
        except ex.AlreadyExistException as e:
            print e
            return commons.body(message=e.msg, status=e.status)
        except ex.NotEnoughMoneyException as e:
            print e
            return commons.body(message=e.msg, status=e.status)
        except ex.AmountMinusException as e:
            print e
            return commons.body(message=e.msg, status=e.status)
        except ex.CertificationFailException as e:
            print e
            return commons.body(message=e.msg, status=e.status)
        except KeyError as e:
            print "KeyError : %s" %e
            return commons.body(message="field validation fail(wrong key, %s), check request key"%e, \
                    status=commons.FIELD_VALIDATION_FAIL)
        except mysql.connector.IntegrityError as e:
            print "mysql.connector.IntegrityError : %s" %e
            return commons.body(message="DB error, reqeust to admin", \
                    status=commons.DATABASE_ERROR)
        except Exception as e:
            print "Exception : %s" %e
            return commons.body(message="server internal error, request to admin", \
                    status=commons.SERVER_INTERNAL_ERROR)
    return exception_decorator
