# -*- coding: utf-8 -*-
import logging
from logging.handlers import RotatingFileHandler
from flask import Flask, request, current_app
from decorators import crossdomain, set_grant, common_exception_handler
from database import Database as db
from certifier import Certifier as cert
from config import Grant as g
from gevent import monkey
import config as CONFIG
import commons
import common_exceptions as ex
import mysql.connector
from google_calendar import GoogleCalendar as gc
monkey.patch_all()

app = Flask(__name__)

@app.before_request
def grant_check_before_request():
    '''
    logging & authentification token
    '''
    if CONFIG.DEBUG_MODE:
        try:
            current_app.logger.debug('request headers : \n%s' % request.headers)
            current_app.logger.debug('request data : \n%s' % request.data)
            current_app.logger.debug('request files : \n%s' % str(request.files))
            print "reqeust : %s" %request
        except Exception as e:
            print e

    if request.endpoint in app.view_functions:
        view_func = app.view_functions[request.endpoint]
        try:
            print "You have KEYs in HEADER.AUTHRIZATION : %s" %str(request.headers['Authorization'])
            if request.endpoint == 'login' or request.endpoint == 'create_the_account_info' or request.endpoint == 'logout':
                print "you are access in login!!"
                pass
            elif 'Authorization' in request.headers and cert.checkTokenValidate(str(request.headers['Authorization'])):
                '''
            elif 'Authorization' in request.headers:
                print "You have KEYs in HEADER.AUTHRIZATION : %s" %str(request.headers['Authorization'])
                if cert.checkTokenValidate(str(request.headers['Authorization'])):
                    print "certification good"
                    if hasattr(view_func, '_grant'):
                        if cert.getGrantFromToken(str(request.headers['Authorization'])) in getattr(view_func, '_grant'):

                            pass
                    else:
                        return commons.body(message="Authorization failed", status=commons.API_UNAUTHORIZED)
                '''

                print "certification good"
                if hasattr(view_func, '_grant'):
                    if cert.getGrantFromToken(str(request.headers['Authorization'])) in getattr(view_func, '_grant'):
                        pass

                current_app.logger.debug('request authorization : \n %s' %request.headers['Authorization'])
            else:
                app.logger.warning("Unauthorized Access for %s" %request.endpoint)
                print "TODO : please set sertification. you didn't cert"
                return commons.body(message="Authorization failed", status=commons.API_UNAUTHORIZED)
        except Exception as e:
            print "auth : %s " %e
    else:
        app.logger.warning("Wrong endpoint, Please deny this below IP. request.endpoint is %s" %request.endpoint)
        return commons.body(message="Page Not Found", status=commons.PAGE_NOT_FOUND)

#################################################
# sample app for test                           #
#################################################
@app.route("/simple", methods=["GET", "POST"])
@crossdomain(origin='*', headers='Content-Type')
def hello():
    if request.method == "GET":
        return commons.body(message="hello", data="hello data")
    elif request.method == "POST":
        data = request.get_json(force=True)
        return commons.body(message="hello Post get", data=data)


@app.route("/simples")
@crossdomain(origin='*', headers='Content-Type')
def hellos():
    return commons.body(message="hello arrays", data=[{'hello':'hello1'}, {'hello':'hello2'}])

#################################################
# Login / Logout                                #
#################################################
@app.route("/login", methods=["POST", "OPTIONS"], \
        endpoint="login")
@crossdomain(origin='*', headers='Content-Type')
@common_exception_handler
def login():
    data = request.get_json(force=True)
    auth_key = cert.getToken(data['id'], data['password'])
    return commons.body(message="user key created done", data=auth_key)

@app.route("/logout/<account_id>", methods=["GET", "OPTIONS"], \
        endpoint="logout")
@crossdomain(origin='*', headers='Content-Type')
@common_exception_handler
def logout(account_id):
    print "destroy token : %s" %str(request.headers['Authorization'])
    cert.destroyToken(str(request.headers['Authorization']))
    return commons.body(message="user logout")

#################################################
# Accounts                                      #
#################################################

@app.route("/accounts", methods=["GET", "OPTIONS"], \
        endpoint="get_all_accounts_info")
@crossdomain(origin='*', headers='Content-Type')
@set_grant(g.master_chief + g.current_members)
@common_exception_handler
def get_all_accounts_info():
    '''
    get all account's collection information
    '''
    data = db.selectAllUsers()
    return commons.body(message="get users info list done", data=data)

@app.route("/accounts/<account_id>", methods=["GET", "OPTIONS"], \
        endpoint="get_the_account_info")
@crossdomain(origin='*', headers='Content-Type')
@set_grant(g.master_chief + g.current_members)
@common_exception_handler
def get_the_account_info(account_id):
    '''
    get the account element information by account_id
    '''
    if account_id is None:
        raise ex.RequestMissingException

    data = db.selectUser(account_id)
    return commons.body(message="get users info done", data=data)

@app.route("/accounts/googleid/{googleid}", methods=["GET", "OPTIONS"], \
        endpoint="get_the_account_info_by_googleid")
@crossdomain(origin='*', headers='Content-Type')
@set_grant(g.master_chief + g.current_members)
@common_exception_handler
def get_the_account_info_by_googleid(googleid):
    '''
    get the account element information by account_id
    '''
    if googleid is None:
        raise ex.RequestMissingException

    data = db.selectUserByGoogleID(googleid)
    return commons.body(message="get users info by googleid done", data=data)

@app.route("/accounts", methods=["POST", "OPTIONS"], \
        endpoint="create_the_account_info")
@crossdomain(origin='*', headers='Content-Type')
@set_grant(g.master_chief + g.network)
@common_exception_handler
def create_the_account_info():
    '''
    create the account element information
    '''
    data = request.get_json(force=True)

    db.createUser(data['id'], data['password'], data['name'], data['entered'], \
            data['googleID'], data['birthday'], data['phone'], data['university'], \
            data['grant'], data['social_security_number'])

    cal = gc()
    cal.register_credentials(data['id'], data['code'])
    del cal

    return commons.body(message="user create done")

@app.route("/accounts/<account_id>", methods=["PUT", "OPTIONS"], \
        endpoint="modify_the_account_info")
@crossdomain(origin='*', headers='Content-Type')
@set_grant(g.master_chief + g.network)
@common_exception_handler
def modify_the_account_info(account_id):
    '''
    modify the account element information
    '''
    data = request.get_json(force=True)
    if data['id'] != account_id:
        raise ex.RequestMismatchException

    db.modifyUser(data['id'], data['password'], data['name'], data['entered'], \
            data['googleID'], data['birthday'], data['phone'], data['university'], \
            data['grant'], data['social_security_number'])

    cal = gc()
    cal.update_common_calendar(data['id'])
    del cal

    return commons.body(message="user modify done")

#################################################
# Living - Attendance                           #
#################################################

@app.route("/attend/<month>/<account_id>", methods=["GET", "OPTIONS"], \
        endpoint="get_the_account_attend_info")
@crossdomain(origin='*', headers='Content-Type')
@set_grant(g.master_chief + g.current_members)
@common_exception_handler
def get_the_account_attend_info(month, account_id):
    '''
    get the account attendance's collection information of month
    '''
    if account_id is None or month is None:
        raise ex.RequestMissingException
    if commons.validate_date(month):
        data = db.getAttendance(account_id, month)
        return commons.body(message="get users attendance info", data=data)
    else:
        raise ex.DateValidateException

@app.route("/attend/<account_id>", methods=["POST", "OPTIONS"], \
        endpoint="create_the_attend_info")
@crossdomain(origin='*', headers='Content-Type')
@set_grant(g.master_chief + g.living)
@common_exception_handler
def create_the_attend_info(account_id):
    '''
    create the account attendance element information
    '''
    data = request.get_json(force=True)
    db.setAttendance(data['account_id'], data['attenddate'], data['attendance'])
    return commons.body(message="set users attendance done at %s" %data['attenddate'])

@app.route("/attend/rate/<month>", methods=["GET", "OPTIONS"], \
        endpoint="get_attend_rate")
@crossdomain(origin='*', headers='Content-Type')
@set_grant(g.master_chief + g.current_members)
@common_exception_handler
def get_attend_rate(month):
    '''
    get the attendance's collection rates information by month
    '''
    if commons.validate_date(month):
        attend_rate = db.getAttendanceRate(month)
        return commons.body(message="get attendance rate of month, %s" %month, data=attend_rate)
    else:
        raise ex.DateValidateException

@app.route("/attend/number/<month>", methods=["GET", "OPTIONS"], \
        endpoint="get_attend_number")
@crossdomain(origin='*', headers='Content-Type')
@set_grant(g.master_chief + g.current_members)
@common_exception_handler
def get_attend_number(month):
    '''
    get the attendance's collection days information by month
    '''
    if commons.validate_date(month):
        data = db.getAttendanceNumber(month)
        return commons.body(message="get attendance count number of month, %s" %month, data=data)
    else:
        raise ex.DateValidateException

@app.route("/attend/nocount/<month>", methods=["GET", "OPTIONS"], \
        endpoint="get_nocount_days")
@crossdomain(origin='*', headers='Content-Type')
@set_grant(g.master_chief + g.current_members)
@common_exception_handler
def get_nocount_days(month):
    '''
    get no count day's (~= holidays) collection information by month
    '''
    if commons.validate_date(month):
        data = db.getNoCountDays(month)
        return commons.body(message="get no count days of month, %s" %month, data=data)
    else:
        raise ex.DateValidateException

@app.route("/attend/nocount/<month>", methods=["POST", "OPTIONS"], \
        endpoint="create_nocount_days")
@crossdomain(origin='*', headers='Content-Type')
@set_grant(g.master_chief + g.living)
@common_exception_handler
def create_nocount_days(month):
    '''
    create no count day's (~= holidays) collection information by month
    '''
    if commons.validate_date(month):
        data = request.get_json(force=True)
        db.setNoCountDay(data['attenddate'], data['reason'])
        return commons.body(message="set no count day is done on %s"%month)
    else:
        raise ex.DateValidateException

#################################################
# Living Duties APIs                            #
#################################################
@app.route("/duties/<month>", methods=["GET", "OPTIONS"], \
        endpoint="get_duties")
@crossdomain(origin='*', headers='Content-Type')
@set_grant(g.master_chief + g.current_members)
@common_exception_handler
def get_duties(month):
    if commons.validate_date(month):
        data = db.getDuty(month)
        return commons.body(message="get duties collection info", data=data)
    else:
        raise ex.DateValidateException

@app.route("/duties/<month>/<account_id>", methods=["GET", "OPTIONS"], \
        endpoint="get_duties_of_account")
@crossdomain(origin='*', headers='Content-Type')
@set_grant(g.master_chief + g.current_members)
@common_exception_handler
def get_duties(month, account_id):
    if commons.validate_date(month):
        data = db.getDuty(month, account_id)
        return commons.body(message="get duties collection info", data=data)
    else:
        raise ex.DateValidateException

@app.route("/duties", methods=["POST", "OPTIONS"], \
        endpoint="create_duty")
@crossdomain(origin='*', headers='Content-Type')
@set_grant(g.master_chief + g.living_chief)
@common_exception_handler
def create_duty():
    datas = request.get_json(force=True)
    for data in datas:
        if commons.validate_date(data['dutydate']):
            db.setDuty(data['dutydate'], data['m1_account_id'], \
                    data['m2_account_id'], data['m3_account_id'], \
                    data['m4_account_id'], data['f1_account_id'], \
                    data['f2_account_id'], data['f3_account_id'], \
                    data['f4_account_id'])
        else:
            raise ex.DateValidateException
    return commons.body(message="duty info list create done")

#################################################
# Living PX APIs                                #
#################################################
@app.route("/px/items", methods=["GET", "OPTIONS"], \
        endpoint="get_px_items_info")
@crossdomain(origin='*', headers='Content-Type')
@set_grant(g.master_chief + g.current_members)
@common_exception_handler
def get_px_items_info():
    '''
    get PX Item's collection list
    '''
    data = db.getItems()
    return commons.body(message="get px item list done", data=data)

@app.route("/px/items", methods=["POST", "OPTIONS"], \
        endpoint="create_px_item_info")
@crossdomain(origin='*', headers='Content-Type')
@set_grant(g.master_chief + g.px)
@common_exception_handler
def create_px_item_info():
    '''
    register the new px item element
    '''
    data = request.get_json(force=True)
    db.registItem(data['barcode'], data['itemname'], \
            data['price'], data['remained'])
    return commons.body(message="px item register done")

@app.route("/px/items", methods=["PUT", "OPTIONS"], \
        endpoint="modify_px_item_info")
@crossdomain(origin='*', headers='Content-Type')
@set_grant(g.master_chief + g.px)
@common_exception_handler
def modify_px_item_info():
    '''
    modify the new px item element
    '''
    data = request.get_json(force=True)
    db.modifyItem(data['barcode'], data['itemname'], data['price'], data['remained'])
    return commons.body(message="px item modified done")

@app.route("/px/items/<barcode>", methods=["GET", "OPTIONS"], \
        endpoint="get_one_px_item_info")
@crossdomain(origin='*', headers='Content-Type')
@set_grant(g.master_chief + g.current_members)
@common_exception_handler
def get_one_px_item_info(barcode):
    '''
    get one PX Item's collection list
    '''
    data = db.getItemByBarcode(barcode)
    return commons.body(message="get px item by barcode done", data=data)

@app.route("/px/items/<barcode>", methods=["DELETE", "OPTIONS"], \
        endpoint="delete_px_item_info")
@crossdomain(origin='*', headers='Content-Type')
@set_grant(g.master_chief + g.px)
@common_exception_handler
def delete_px_item_info(barcode):
    '''
    delete the px item element
    '''
    db.deleteItem(barcode)
    return commons.body(message="delete px item done")

@app.route("/px/charge", methods=["POST", "OPTIONS"], \
        endpoint="charge_px_amount_to_account")
@crossdomain(origin='*', headers='Content-Type')
@set_grant(g.master_chief + g.current_members)
@common_exception_handler
def charge_px_amount_to_account():
    '''
    charge px amount to account
    '''
    data = request.get_json(force=True)
    db.chargePX(data['account_id'], data['amount'])
    return commons.body(message="Charge amount is done")

@app.route("/px/purchase", methods=["POST", "OPTIONS"], \
        endpoint="purchase_px_items")
@crossdomain(origin='*', headers='Content-Type')
@set_grant(g.master_chief + g.current_members)
@common_exception_handler
def purchase_px_items():
    '''
    purchase px items by several accounts
    '''
    data = request.get_json(force=True)
    db.buyItem(data['account_id_list'], data['item_list'])
    return commons.body(message="Purchase item is done")

@app.route("/px/refund", methods=["POST", "OPTIONS"], \
        endpoint="refund_px_items")
@crossdomain(origin='*', headers='Content-Type')
@set_grant(g.master_chief + g.current_members)
@common_exception_handler
def refund_px_items():
    '''
    refund px items to serveral accounts
    '''
    data = request.get_json(force=True)
    db.refundItem(data['account_id_list'], data['item_list'])
    return commons.body(message="Refund item is done")

@app.route("/px/log/<start_datetime>/<end_datetime>", methods=["GET", "OPTIONS"], \
        endpoint="get_px_log_info")
@crossdomain(origin='*', headers='Content-Type')
@set_grant(g.master_chief + g.current_members)
@common_exception_handler
def get_px_log_info(start_datetime, end_datetime):
    '''
    get PX Item's collection log information
    '''
    if commons.validate_datetime(start_datetime) and commons.validate_datetime(end_datetime):
        data = db.getPXLog(start_datetime, end_datetime)
        return commons.body(message="get px item list done", data=data)
    else:
        raise ex.ValidateDatetimeException

@app.route("/px/log/<start_datetime>/<end_datetime>/<account_id>", methods=["GET", "OPTIONS"], \
        endpoint="get_px_log_by_id_info")
@crossdomain(origin='*', headers='Content-Type')
@set_grant(g.master_chief + g.current_members)
@common_exception_handler
def get_px_log_by_id_info(start_datetime, end_datetime, account_id):
    '''
    get PX Item's collection log information
    '''
    if commons.validate_datetime(start_datetime) and commons.validate_datetime(end_datetime):
        data = db.getPXLogbyID(account_id, start_datetime, end_datetime)
        return commons.body(message="get px log list by id done", data=data)
    else:
        raise ex.ValidateDatetimeException

#################################################
# PMS Projects APIs                             #
#################################################

@app.route("/projects", methods=["GET", "OPTIONS"], \
        endpoint="get_pms_project_info_list")
@crossdomain(origin='*', headers='Content-Type')
@set_grant(g.master_chief + g.current_members)
@common_exception_handler
def get_pms_project_info_list():
    '''
    get PMS project's collection information
    '''
    data = db.selectAllProjects()
    return commons.body(message="get projects list info", data=data)

@app.route("/projects/<project_id>", methods=["GET", "OPTIONS"], \
        endpoint="get_pms_project_info")
@crossdomain(origin='*', headers='Content-Type')
@set_grant(g.master_chief + g.current_members)
@common_exception_handler
def get_pms_project_info(project_id):
    '''
    get the PMS project element information by id
    '''
    data = db.selectProject(project_id)
    return commons.body(message="get project %s info" %project_id, data=data)

@app.route("/projects/search/<keyword>", methods=["GET", "OPTIONS"], \
        endpoint="search_pms_project_info")
@crossdomain(origin='*', headers='Content-Type')
@set_grant(g.master_chief + g.current_members)
@common_exception_handler
def search_pms_project_info(keyword):
    '''
    search the PMS project's collection information by Keyword
    '''
    data = db.searchProject(keyword)
    return commons.body(message="searched project info", data=data)

@app.route("/projects", methods=["POST", "OPTIONS"], \
        endpoint="create_pms_project_info")
@crossdomain(origin='*', headers='Content-Type')
@set_grant(g.master_chief + g.current_members)
@common_exception_handler
def create_pms_project_info():
    '''
    create the PMS project element information
    '''
    data = request.get_json(force=True)
    if 'team2' not in data:
        db.createProject(data['projectname'], data['summary'], data['startmonth'], \
                data['endmonth'], data['creative_rate'], data['technical_rate'], \
                data['useful_rate'], data['successful_rate'], data['pl'], data['team1'])
    elif 'team3' not in data:
        db.createProject(data['projectname'], data['summary'], data['startmonth'], \
                data['endmonth'], data['creative_rate'], data['technical_rate'], \
                data['useful_rate'], data['successful_rate'], data['pl'], data['team1'], \
                data['team2'])
    elif 'team4' not in data:
        db.createProject(data['projectname'], data['summary'], data['startmonth'], \
                data['endmonth'], data['creative_rate'], data['technical_rate'], \
                data['useful_rate'], data['successful_rate'], data['pl'], data['team1'], \
                data['team2'], data['team3'])
    else:
        db.createProject(data['projectname'], data['summary'], data['startmonth'], \
                data['endmonth'], data['creative_rate'], data['technical_rate'], \
                data['useful_rate'], data['successful_rate'], data['pl'], data['team1'], \
                data['team2'], data['team3'], data['team4'])
    return commons.body(message="project create done")

@app.route("/projects", methods=["PUT", "OPTIONS"], \
        endpoint="modify_pms_project_info")
@crossdomain(origin='*', headers='Content-Type')
@set_grant(g.master_chief + g.current_members)
@common_exception_handler
def modify_pms_project_info():
    '''
    modify the PMS project element information
    '''
    data = request.get_json(force=True)

    if 'team2' not in data:
        db.modifyProject(data['id'], data['projectname'], data['summary'], data['startmonth'], \
                data['endmonth'],  data['creative_rate'], data['technical_rate'], \
                data['useful_rate'], data['successful_rate'], data['pl'], data['team1'])
    elif 'team3' not in data:
        db.modifyProject(data['id'], data['projectname'], data['summary'], data['startmonth'], \
                data['endmonth'], data['creative_rate'], data['technical_rate'], \
                data['useful_rate'], data['successful_rate'], data['pl'], data['team1'], data['team2'])
    elif 'team4' not in data:
        db.modifyProject(data['id'], data['projectname'], data['summary'], data['startmonth'], \
                data['endmonth'], data['creative_rate'], data['technical_rate'], \
                data['useful_rate'], data['successful_rate'], data['pl'], data['team1'], data['team2'], \
                data['team3'])
    else:
        db.modifyProject(data['id'], data['projectname'], data['summary'], data['startmonth'], \
                data['endmonth'], data['creative_rate'], data['technical_rate'], \
                data['useful_rate'], data['successful_rate'], data['pl'], data['team1'], data['team2'], \
                data['team3'], data['team4'])
    return commons.body(message="project modify done")


@app.route("/projects/<project_id>", methods=["DELETE", "OPTIONS"], \
        endpoint="delete_pms_project_info")
@crossdomain(origin='*', headers='Content-Type')
@set_grant(g.master_chief + g.current_members)
@common_exception_handler
def delete_pms_project_info(project_id):
    '''
    delete the PMS project element information
    '''
    db.deleteProject(project_id)
    return commons.body(message="project delete done")

#################################################
# PMS jurors APIs                               #
#################################################
@app.route("/presentations/jurors", methods=["GET", "OPTIONS"], \
        endpoint="get_all_jurors_info_list")
@crossdomain(origin='*', headers='Content-Type')
@set_grant(g.master_chief + g.current_members)
@common_exception_handler
def get_all_jurors_info_list():
    '''
    get all juror's info collection
    '''
    data = db.getAllJuror()
    return commons.body(message="get Juror's info collection is done", data=data)

@app.route("/presentations/jurors/<account_id>", methods=["GET", "OPTIONS"], \
        endpoint="get_the_jurors_info")
@crossdomain(origin='*', headers='Content-Type')
@set_grant(g.master_chief + g.current_members)
@common_exception_handler
def get_the_jurors_info(account_id):
    '''
    get the juror's info by id
    '''
    data = db.getJurorByAccount(account_id)
    return commons.body(message="get Juror's info element is done", data=data)


#################################################
# PMS jurors about presentations                #
#################################################

@app.route("/presentations/<project_id>/jurors", methods=["GET", "OPTIONS"], \
        endpoint="get_all_jurors_info_by_project_id")
@crossdomain(origin='*', headers='Content-Type')
@set_grant(g.master_chief + g.current_members)
@common_exception_handler
def get_all_jurors_info_by_project_id(project_id):
    '''
    get all juror's collection information by project
    '''
    data=db.getJuror(project_id)
    return commons.body(message="get Jurors info is done", data=data)

@app.route("/presentations/<project_id>/jurors", methods=["POST", "OPTIONS"], \
        endpoint="create_the_juror_info")
@crossdomain(origin='*', headers='Content-Type')
@set_grant(g.master_chief + g.current_members)
@common_exception_handler
def create_the_juror_info(project_id):
    '''
    create the juror element information of project
    '''
    data = request.get_json(force=True)
    if project_id is None:
        raise ex.RequestMissingException
    if project_id != data['project_id']:
        raise ex.RequestMismatchException

    db.registJuror(data['account_id'], data['project_id'], data['presentation_type'])
    return commons.body(message="regist Juror is done")

@app.route("/presentations/<project_id>/type/<presentation_type>/jurors/<account_id>", methods=["DELETE", "OPTIONS"], \
        endpoint="delete_the_juror_info")
@crossdomain(origin='*', headers='Content-Type')
@set_grant(g.master_chief + g.education)
@common_exception_handler
def delete_the_juror_info(project_id, presentation_type, account_id):
    '''
    delete the juror element information of project
    '''
    db.deleteJuror(project_id, presentation_type, account_id)
    return commons.body(message="delete Juror is done")

#################################################
# PMS Presentations Attendance APIs             #
#################################################
@app.route("/presentations/<project_id>/type/<int:presentation_type>/attend", methods=["GET", "OPTIONS"], \
        endpoint="get_all_attendance_info_of_presentation")
@crossdomain(origin='*', headers='Content-Type')
@set_grant(g.master_chief)
@common_exception_handler
def get_all_attendance_info_of_presentation(project_id, presentation_type):
    '''
    get all attendance's collection information by presentation
    '''
    raise NotImplementedError()

@app.route("/presentations/<project_id>/type/<int:presentation_type>/attend/<account_id>", methods=["GET", "OPTIONS"], \
        endpoint="get_the_user_attendance_info_of_presentation")
@crossdomain(origin='*', headers='Content-Type')
@set_grant(g.master_chief)
@common_exception_handler
def get_the_user_attendance_info_of_presentation(project_id, presentation_type, account_id):
    '''
    get the user attendance element information of presentation
    '''
    raise NotImplementedError()

@app.route("/presentations/<project_id>/type/<int:presentation_type>/attend/<account_id>", methods=["POST", "OPTIONS"], \
        endpoint="create_the_user_attendance_info_of_presentation")
@crossdomain(origin='*', headers='Content-Type')
@set_grant(g.master_chief)
@common_exception_handler
def create_the_user_attendance_info_of_presentation(project_id, presentation_type, account_id):
    '''
    create the user attendance element information of presentation
    '''
    raise NotImplementedError()

@app.route("/presentations/<project_id>/type/<int:presentation_type>/attend/<account_id>", methods=["PUT", "OPTIONS"], \
        endpoint="modify_the_user_attendance_info_of_presentation")
@crossdomain(origin='*', headers='Content-Type')
@set_grant(g.master_chief)
@common_exception_handler
def modify_the_user_attendance_info_of_presentation(project_id, presentation_type, account_id):
    '''
    modify the user attendance element information of presentation
    '''
    raise NotImplementedError()

@app.route("/presentations/<project_id>/type/<int:presentation_type>/attend/<account_id>", methods=["DELETE", "OPTIONS"], \
        endpoint="delete_the_user_attendance_info_of_presentation")
@crossdomain(origin='*', headers='Content-Type')
@set_grant(g.master_chief)
@common_exception_handler
def delete_the_user_attendance_info_of_presentation(project_id, presentation_type, account_id):
    '''
    delete the user attendance element information of presentation
    '''
    raise NotImplementedError()


#################################################
# PMS Presentation Appraise Contents APIs       #
#################################################
@app.route("/presentations/<project_id>/type/<int:presentation_type>/appraise/contents", methods=["GET", "OPTIONS"], \
        endpoint="get_all_appraise_contents_info_of_presentation")
@crossdomain(origin='*', headers='Content-Type')
@set_grant(g.master_chief + g.current_members)
@common_exception_handler
def get_all_appraise_contents_info_of_presentation(project_id, presentation_type):
    '''
    get all appraise content's collection information by presentation
    '''
    data = db.getAppraiseContents(project_id, presentation_type)
    return commons.body(message="get appraise contents done", data=data)


@app.route("/presentations/<project_id>/type/<int:presentation_type>/appraise/contents", methods=["PUT", "OPTIONS"], \
        endpoint="modify_all_appraise_contents_info_of_presentation")
@crossdomain(origin='*', headers='Content-Type')
@set_grant(g.master_chief + g.education)
@common_exception_handler
def modify_all_appraise_contents_info_of_presentation(project_id, presentation_type):
    '''
    modify all appraise content's collection information by presentation
    '''
    data = request.get_json(force=True)
    db.modifyAppraiseContents(project_id, presentation_type, data['contents'])
    return commons.body(message="modify appraise contents is done")

#################################################
# PMS Presentations Appraise Result APIs        #
#################################################
@app.route("/presentations/<project_id>/type/<int:presentation_type>/appraise/results", methods=["GET", "OPTIONS"], \
        endpoint="get_all_results_info_of_presentation")
@crossdomain(origin='*', headers='Content-Type')
@set_grant(g.master_chief + g.current_members)
@common_exception_handler
def get_all_results_info_of_presentation(project_id, presentation_type):
    '''
    get all result's collection information by presentation
    '''
    data = db.getAppraiseResult(project_id, presentation_type)
    return commons.body(message="get appraise contents is done", data=data)

@app.route("/presentations/<project_id>/type/<int:presentation_type>/appraise/results", methods=["POST", "OPTIONS"], \
        endpoint="create_result_info_of_presentation")
@crossdomain(origin='*', headers='Content-Type')
@set_grant(g.master_chief + g.current_members)
@common_exception_handler
def create_result_info_of_presentation(project_id, presentation_type):
    '''
    create the result element information by user of presentation
    '''
    data = request.get_json(force=True)
    db.appraiseProject(data['account_id'], data['appraise_id'], data['contents'])
    return commons.body(message="appraise project is done")

#################################################
# Group Board APIs                              #
#################################################
@app.route("/boards/free/pages/<page_num>", methods=["GET", "OPTIONS"], \
        endpoint="get_board_free_info_list")
@crossdomain(origin='*', headers='Content-Type')
@set_grant(g.master_chief + g.current_members)
@common_exception_handler
def get_board_free_info_list(page_num):
    '''
    get board type free information's collection list
    '''
    data = db.getFreeBoardList(page_num)
    return commons.body(message="get free board list by id done", data=data)

@app.route("/boards/free", methods=["POST", "OPTIONS"], \
        endpoint="create_board_free_info")
@crossdomain(origin='*', headers='Content-Type')
@set_grant(g.master_chief + g.current_members)
@common_exception_handler
def create_board_free_info():
    '''
    create the board type free information
    '''
    data = request.get_json(force=True)
    db.writeFreeBoard(data['account_id'], data['title'], data['content'])
    return commons.body(message="write free board is done")


@app.route("/boards/free/<board_id>", methods=["GET", "OPTIONS"], \
        endpoint="get_board_free_info_element")
@crossdomain(origin='*', headers='Content-Type')
@set_grant(g.master_chief + g.current_members)
@common_exception_handler
def get_board_free_info_element(board_id):
    '''
    get the board type free information by board's id
    '''
    data = db.getFreeBoardDetail(board_id)
    return commons.body(message="get free board element by id done", data=data)

@app.route("/boards/comment", methods=["POST", "OPTIONS"], \
        endpoint="create_comment_info")
@crossdomain(origin='*', headers='Content-Type')
@set_grant(g.master_chief + g.current_members)
@common_exception_handler
def create_comment_info():
    '''
    create the comment information
    '''
    data = request.get_json(force=True)
    db.writeComment(data['board_id'], data['account_id'], data['content'])
    return commons.body(message="write comment is done")

@app.route("/boards/comment/{comment_id}", methods=["DELETE"], \
        endpoint="delete_comment_info")
@crossdomain(origin='*', headers='Content-Type')
@set_grant(g.master_chief + g.current_members)
@common_exception_handler
def delete_comment_info(comment_id):
    '''
    delete the comment information
    '''
    data = request.get_json(force=True)
    db.deleteComment(comment_id)
    return commons.body(message="delete comment is done")


#############################
# Run Application           #
#############################

if __name__ == "__main__":
    # create log handler
    formatter = logging.Formatter("[%(asctime)s] %(levelname)s {%(pathname)s:%(lineno)d} - %(message)s")
    handler = RotatingFileHandler(CONFIG.LOG_PATH, encoding='UTF-8', maxBytes=CONFIG.LOG_FILE_MAX_BYTES, \
            backupCount=CONFIG.BACKUP_COUNTS)
    handler.setLevel(logging.INFO)
    handler.setFormatter(formatter)

    # get log from werkzeug
    logger = logging.getLogger('werkzeug')
    logger.addHandler(handler)

    # get log from app.logger(by code)
    app.logger.addHandler(handler)
    app.run(debug=CONFIG.DEBUG_MODE, host=CONFIG.RUN_HOST, port=CONFIG.RUN_PORT)
