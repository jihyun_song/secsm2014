
/// Cross Site Scripting 을 방지하기 위한 filter function을 정의
var XSSfilter = function( content ) {

    return content.replace(/</g, "&lt;").replace(/>/g, "&gt;");
};

