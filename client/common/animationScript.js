


var SpinnerAnimation = function($timeout, duration, callback){
	
	var opts = {
		lines: 10, // The number of lines to draw
		length: 10, // The length of each line
		width: 7, // The line thickness
		radius: 12, // The radius of the inner circle
		color: '#000', // #rgb or #rrggbb
		speed: 1.4, // Rounds per second
		trail: 54, // Afterglow percentage
		shadow: false // Whether to render a shadow
	};

	var target = document.createElement('div');
	document.body.appendChild(target);

	var spinner = new Spinner(opts).spin(target);

	$timeout(function(){
		console.log('timeout');
		spinner.stop();
		
		if(callback != null)
			callback();
	}, duration);
	
};



