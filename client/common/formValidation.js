

var validateEmail = function(email){
	// var re = /^[\w-]{4,}@[\w-]+(\.\w+){1,3}$/;
	var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	if(!re.test(email)){
		alert('email 형식이 잘 못 되었습니다.');
		return false;
	}
	return true;
};

var validateId = function(id){
	if(id.length < 5 || id.length > 10){
		alert('아이디 형식이 잘 못 되었습니다.');
		return false;
	}
	return true;
};

var validatePassword = function(pw){
	if(pw.length < 5 || pw.length > 12){
		alert('비밀번호 형식이 잘 못 되었습니다.');
		return false;
	}
	return true;
};

var validateName = function(name){
	if(name.length < 2 || name.length > 8){
		alert('이름 형식이 잘 못 되었습니다.');
		return false;
	}
	return true;
};

var validateSocialNumber = function(socialNumber){

	var fmt = /^\d{6}[1234]\d{6}$/;  //포멧 설정
	if (!fmt.test(socialNumber)) {
	 return false;
	}
	
	  // 생년월일 검사
	var birthYear = (socialNumber.charAt(6) <= "2") ? "19" : "20";
	birthYear += socialNumber.substr(0, 2);
	var birthMonth = socialNumber.substr(2, 2) - 1;
	var birthDate = socialNumber.substr(4, 2);
	var birth = new Date(birthYear, birthMonth, birthDate);
	
	if ( birth.getYear() % 100 != socialNumber.substr(0, 2) ||
	     birth.getMonth() != birthMonth ||
	     birth.getDate() != birthDate) {
	   return false;
	}
	
	// Check Sum 코드의 유효성 검사
	var buf = new Array(13);
	for (var i = 0; i < 13; i++) buf[i] = parseInt(socialNumber.charAt(i));
	 
	// multipliers = [2,3,4,5,6,7,8,9,2,3,4,5];
	var multipliers = [2,3,4,5,6,7,8,9,2,3,4,5];
	 
	for (var sum = 0, i = 0; i < 12; i++) sum += (buf[i] *= multipliers[i]);
	
	if ((11 - (sum % 11)) % 10 != buf[12]) {
	   return false;
	}
	
	  
	return true;
	
};

var validateUniversity = function(univ){
	if(univ.length < 4 || univ.length > 10 || (univ.search('대학교') == -1 && univ.search('university') == -1)){
		alert('학교 형식이 잘 못 되었습니다.');
		return false;
	}
	return true;
};

/// 로그인 유효성 검사
var validateLogin = function(email, pw){
	if(validateEmail(email) == 0 || validatePassword(pw) == 0){
		return false;
	}
	return true;
};
		
/// 회원가입 유효성 검사
var validateSignup = function(email, id, pw, name, number, univ){
	
	if(validateEmail(email) == false || validateId(id) == false || validatePassword(pw) == false 
	|| validateName(name) == false || validateSocialNumber(number) == false || validateUniversity(univ) == false){
		return false;
	}
	return true;
};
