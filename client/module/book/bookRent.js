app.controller('bookRentCtrl', function BookRentCtrl($scope, bookWasService){
	console.log("Book Rental Control");

	$scope.curPage = 0;
	$scope.pageSize = 10;	
	$scope.books = [
		{
			bookid: 1,
			bookname: '테스트1',
			bookauthor: '나자신',
			bookpublish: '멤버십',
			bookstatus: '대여중'
		},
		{
			bookid: 2,
			bookname: '테스트2',
			bookauthor: '너자신',
			bookpublish: 'cgv',
			bookstatus: '대여가능'
		},
		{
			bookid: 0,
			bookname: '테스트3',
			bookauthor: '봇',
			bookpublish: '메박',
			bookstatus: '대여가능'
		}

	];
	console.log($scope.books);

	$scope.showData = function(){
		$scope.numberOfPages = function(){
			return Math.ceil($scope.books.length / $scope.pageSize);
		};
	};
});
