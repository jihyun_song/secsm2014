app.controller('bookReturnCtrl', function bookReturnCtrl($scope){
	console.log("Book Return Controller");
	
	$scope.curPage = 1;
	$scope.numPerPage = 10;
	$scope.maxSize = 5;
	$scope.books = [
		{
			bookid: 1,
			bookname: '임시로 떼우는 더미테이터',
			rentDate: '1288323623006 ',
			returnDate: '1288323623006 ',
			renter: '대여중'
		},
	];

	$scope.showData = function(){
		$scope.numberOfPages = function(){
			return Math.ceil($scope.books.length / $scope.pageSize);
		};
	};
});

app.controller('expiredBookCtrl', function expiredBookCtrl($scope) {
	$scope.curPage = 1;
	$scope.numPerPage = 10;
	$scope.maxSize = 5;
	$scope.books = [
		{
			bookid: 1,
			bookname: '임시로 떼우는 더미테이터',
			rentDate: '1288323623006 ',
			returnDate: '1288323623006 ',
			renter: '대여중'
		},
	];
});