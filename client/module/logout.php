
<?
	//현재 사용중인 세션 불러오기
	session_start();

	//SESSION 삭제
	unset($_SESSION['token']);
	unset($_SESSION['uid']);
	unset($_SESSION['realid']);
	unset($_SESSION['username']);
	unset($_SESSION['timeout']);
	session_destroy();
	
	header("location: http://remake.secsm.org/#/"); 
?>
