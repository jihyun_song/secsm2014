

	

var clientId = "257566283142-c0algk432k6rj15jpkjol3euuvjv59b9.apps.googleusercontent.com";
var apiKey = '-7sKsz1MuLDuCiSoFWUwTh62';
var scopes = 'https://www.googleapis.com/auth/calendar';

var OAUTHURL    =   'https://accounts.google.com/o/oauth2/auth?';
var VALIDURL    =   'https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=';

var REDIRECT    =   'http://remake.secsm.org/signup.html';
var TYPE        =   'token';

var _url = 'https://accounts.google.com/o/oauth2/auth?client_id=' + clientId + '&scope=' + scopes + '&redirect_uri=' + REDIRECT + '&response_type=code&access_type=offline';

var acToken = "";
var tokenType = "";
var expiredIn = "";


var WebServer = function(){
	return "http://remake.secsm.org";
};

var LoginServer = function(){
	return "http://remake.secsm.org:8080";
};


document.onkeypress = function(e){
	var e = window.event || e;
	var keyCode = e.keyCode;
	if(keyCode == 13){
		login();
	}
};


var login = function(){
	var _id = $('#id-login').val();
	var _pw = $('#pw-login').val();

	if(validateLogin(_id, _pw) == false){
		return;
	}

	var server = LoginServer() + "/login";
	var loginData = {
		id: _id,
		password: _pw
	};



	//Spin animation 추가
	$('#loginnavBar').css('opacity', '0.2');
	$('#vivid-layer').css('opacity', '0.2');

	var spinTarget = document.createElement('div');
	document.body.appendChild(spinTarget);
	var opts = {
		lines: 10, // The number of lines to draw
		length: 10, // The length of each line
		width: 7, // The line thickness
		radius: 12, // The radius of the inner circle
		color: '#000', // #rgb or #rrggbb
		speed: 1.4, // Rounds per second
		trail: 54, // Afterglow percentage
		shadow: false // Whether to render a shadow
	};
	var spinner = new Spinner(opts).spin(spinTarget);

		
	//post를 통한 로그인 프로세스를 진행.
	$.ajax({
		method: 'POST',
		url: server,
		data: JSON.stringify(loginData),
		dataType: 'json',
		contentType: 'application/json; charset=UTF-8'
	}).success(function(data, status, headers, config){

		if(data.status == 200 && data.result.message == "user key created done"){
			alert('로그인 되었습니다.');
			
			//Spin animation 취소
			spinner.stop(spinTarget);
			
			var token = data.result.data.token;
			var realid = data.result.data.id;
			var username = data.result.data.username;
			var grant = data.result.data.accountgrant;
			
			var phpServer = WebServer() + '/module/login.php';

			var form = document.createElement('form'); 
			form.action = phpServer;
			form.method = 'post';

			var tokenField = document.createElement("input");
	        tokenField.setAttribute("type", "hidden");
	        tokenField.setAttribute("name", "token");
	        tokenField.setAttribute("value", token);
	        var idField = document.createElement('input');
	        idField.setAttribute('type', 'hidden');
	        idField.setAttribute('name', 'uid');
	        idField.setAttribute('value', _id);
	        
	        var realIdField = document.createElement('input');
	        realIdField.setAttribute('type', 'hidden');
	        realIdField.setAttribute('name', 'realid');
	        realIdField.setAttribute('value', realid);
	        
	        var usernameField = document.createElement('input');
	        usernameField.setAttribute('type', 'hidden');
	        usernameField.setAttribute('name', 'username');
	        usernameField.setAttribute('value', username);
	        
	        var grantField = document.createElement('input');
	        grantField.setAttribute('type', 'hidden');
	        grantField.setAttribute('name', 'accountgrant');
	        grantField.setAttribute('value', grant);
	        
			form.appendChild(tokenField);
			form.appendChild(idField);
			form.appendChild(realIdField);
			form.appendChild(usernameField);
			form.appendChild(grantField);

			$('#loginnavBar').css('opacity', '1.0');
			$('#vivid-layer').css('opacity', '1.0');
			
			form.submit();
			
		}else{
			alert('잘못된 계정 정보입니다.');

			//Spin animation 취소
			spinner.stop(spinTarget);
			$('#loginnavBar').css('opacity', '1.0');
			$('#vivid-layer').css('opacity', '1.0');			
		}
	});
 

};
	
var loginGoogle = function(){
	location.href=_url;
};

var signup = function(){
	location.href = "../signup.html";
};



/*
//로그인 컨트롤러
app.controller('SignInController', function SignInControl($scope, $http){
	console.log("SignIn Control");


	var clientId = "257566283142-c0algk432k6rj15jpkjol3euuvjv59b9.apps.googleusercontent.com";
	var apiKey = '-7sKsz1MuLDuCiSoFWUwTh62';
	var scopes = 'https://www.googleapis.com/auth/calendar';

	var OAUTHURL    =   'https://accounts.google.com/o/oauth2/auth?';
    var VALIDURL    =   'https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=';

    var REDIRECT    =   'http://remake.secsm.org/signup.html';
    var TYPE        =   'token';

	var _url = 'https://accounts.google.com/o/oauth2/auth?client_id=' + clientId + '&scope=' + scopes + '&redirect_uri=' + REDIRECT + '&response_type=code&access_type=offline';

	var acToken = "";
	var tokenType = "";
	var expiredIn = "";

	$scope.login = function(){
		var _id = $('#id-login').val();
		var _pw = $('#pw-login').val();

		if(validateLogin(_id, _pw) == false){
			return;
		}

		var server = LoginServer() + "/login";
		var loginData = {
			id: _id,
			password: _pw
		};
		//post를 통한 로그인 프로세스를 진행.
		$http.post(server, JSON.stringify(loginData)).success(function(data){
			console.log(data);
			if(data.status == 200 && data.result.message == "user key created done"){
				alert('로그인 되었습니다.');
				
				var token = data.result.data.token;
				var realid = data.result.data.id;
				var username = data.result.data.username;
				
				var phpServer = WebServer() + '/module/login.php';

				var form = document.createElement('form'); 
				form.action = phpServer;
				form.method = 'post';

				var tokenField = document.createElement("input");
		        tokenField.setAttribute("type", "hidden");
		        tokenField.setAttribute("name", "token");
		        tokenField.setAttribute("value", token);
		        var idField = document.createElement('input');
		        idField.setAttribute('type', 'hidden');
		        idField.setAttribute('name', 'uid');
		        idField.setAttribute('value', _id);
		        
		        var realIdField = document.createElement('input');
		        realIdField.setAttribute('type', 'hidden');
		        realIdField.setAttribute('name', 'realid');
		        realIdField.setAttribute('value', realid);
		        
		        var usernameField = document.createElement('input');
		        usernameField.setAttribute('type', 'hidden');
		        usernameField.setAttribute('name', 'username');
		        usernameField.setAttribute('value', username);
		        
				form.appendChild(tokenField);
				form.appendChild(idField);
				form.appendChild(realIdField);
				form.appendChild(usernameField);
				form.submit();
				
			}else{
				alert('잘못된 계정 정보입니다.');
			}
		});
		
	};
	
	$scope.loginGoogle = function(){
		location.href=_url;
	};
	
	$scope.signup = function(){
		location.href = "../signup.html";
	};

	document.onkeypress = function(e){
		var e = window.event || e;
		var keyCode = e.keyCode;
		if(keyCode == 13){
			$scope.login();
		}
	};

});
*/

