
// app = new Secsm();



var initForms = function(){
	
	/***
	 *		DatePicker Configuration 
	 */	
	$('.form_datetime').datepicker({
		format: 'yy-mm-dd',
		todaybtn: true,
		pickerPosition: "bottom-left"
	}).on('changeDate', function(event){
		$('.datepicker').hide();
	});

	if(oAuthCode != null){
		if(oAuthCode.length > 4){
			$('#linkGoogleBtn').hide();
		}
	}
};


var signUpMember = function(){
	console.log("Sign Up Member");
	
	var userId = XSSfilter($('#userId').val());
	var userPw = XSSfilter($('#userPw').val());
	var userGmail = XSSfilter($('#userGmail').val());
	var userName = XSSfilter($('#userName').val());
	var userEntered = XSSfilter($('#userEntered').val());
	var userBirth = XSSfilter($('#birthDate').val());
	var userSocialSecurityNumber = XSSfilter($('#personIdBirth').val() + $('#personIdKey').val());
	var userPhoneNumber = XSSfilter($('#phoneLocal').val() + $('#phoneNumA').val() + $('#phoneNumB').val());
	var userUniv = XSSfilter($('#userUniv').val());
	
	if(validateSignup(userGmail, userId, userPw, userName, userSocialSecurityNumber, userUniv) == false){
		return ;
	}
	
	var server = "http://210.118.74.115:8080/accounts";
	
	// userPw = Sha1.hash(userPw);
	// userSocialSecurityNumber = $('#ersonIdBirth').val() + Sha1.hash($('#personIdKey').val());
	
	var signUpData = {
		id: userId,
		password: userPw,
		name: userName,
		entered: userEntered,
		birthday: userBirth,
		googleID: userGmail,
		phone: userPhoneNumber,
		university: userUniv,
		code: oAuthCode,
		grant: 1000,
		social_security_number: userSocialSecurityNumber
	};

	// console.log(signUpData);
	
	$.ajax({
		method: 'POST',
		url: server,
		data: JSON.stringify(signUpData),
		dataType: 'json',
		contentType: 'application/json; charset=UTF-8'
	}).success(function(data, status, headers, config){
		if(data.status == 400){
			if(oAuthCode == null){
				alert('구글 계정과 연동해주시기 바랍니다.');
			}else{
				alert('입력 항목이 잘 못 되었습니다.');
			}
			return ;
		}
		else if(data.status == 401){
			alert('API 권한 인증 오류입니다.');
			return ;
		}
		else if(data.status == 402){
			alert('이미 가입된 계정입니다.');
			return ;
		}
		else if(data.status >= 500 && data.status < 600){
			alert('서버에 문제가 발생했습니다. 담당자에 문의해주시기 바랍니다.');
			return ;
		}
		// console.log(data);
		location.href = "http://remake.secsm.org/index.html";
	});
	
};

var backToIndex = function(){
	location.href = "http://remake.secsm.org/index.html";
};

/// 구글 Calendar API와 연동하게 하는 함수
var linkGoogle = function(){

	var clientId = "257566283142-c0algk432k6rj15jpkjol3euuvjv59b9.apps.googleusercontent.com";
	var apiKey = '-7sKsz1MuLDuCiSoFWUwTh62';
	var scopes = 'https://www.googleapis.com/auth/calendar';

	var OAUTHURL    =   'https://accounts.google.com/o/oauth2/auth?';
    var VALIDURL    =   'https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=';

    var REDIRECT    =   'http://remake.secsm.org/signup.html';
    var TYPE        =   'token';

	var _url = 'https://accounts.google.com/o/oauth2/auth?client_id=' + clientId + '&scope=' + scopes + '&redirect_uri=' + REDIRECT + '&response_type=code&access_type=offline&approval_prompt=force';

	// console.log(_url);

	//리다이렉트되서 다시 오면 토큰값이 생기게 된다.
	location.href = _url;	
};

/// Gmail 과 연동이 되었는지를 체크하는 함수
var checkLinkingGmail = function(){
	
	if(oAuthCode == null){
		$('#linkAlertMsg').css('color', 'red');
	}else if(oAuthCode != null && oAuthCode.length > 5){
		$('#linkAlertMsg').css('color', 'rgb(12, 167, 213)');
	}
	
};

