



/***
 * Make secsm singleton object. 
 */
var Secsm = (function(){
	var singleObject = null;
	
	var Secsm =  function(){
		if(singleObject)
		{
			return singleObject;
		}
		singleObject = angular.module('secsm', ['ngResource', 'angularFileUpload', 'ngRoute', 'ngSanitize', 'textAngular', 'ngAnimate']);
		return singleObject;
	};
	
	return Secsm;
})();


//Secsm 어플리케이션을 싱글턴으로 정의
var app = new Secsm();

var WebServer = function(){
	return "http://remake.secsm.org";
};

var LoginServer = function(){
	return "http://remake.secsm.org:8080";
};


app.config(['$httpProvider', function($httpProvider){
	$httpProvider.defaults.useXDomain = true;
	delete $httpProvider.defaults.headers.common['X-Requested-With'];

	$httpProvider.defaults.headers.common = {};
	$httpProvider.defaults.headers.post = {};
	$httpProvider.defaults.headers.put = {};
	$httpProvider.defaults.headers.patch = {};
	// $httpProvider.defaults.headers.common['Authorization'] = 'authdafa';

}]);


app.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider){

	//기본 Angular Viewer로 라우팅.
	$routeProvider.when('/', {
		templateUrl: '/view/entrance.html'
		// controller: 'SignInController'
	});

	//pms Angular Viewer로 라우팅
	$routeProvider.when('/pms', {
		templateUrl:'/view/education/pms/pms.html',
		controller: 'pmsCtrl'
	});
	
	//프로젝트 상세보기 화면으로 라우팅
	$routeProvider.when('/pms/:projectid', {
		templateUrl: '/view/education/pms/select-project.html',
		controller: 'selectProjectCtrl'
	});
	
	$routeProvider.when('/pms/:projectid/Eval', {
		templateUrl: '/view/education/pms/select-project-evals.html',
		controller: 'selectProjectEvalCtrl'
	});
	
	$routeProvider.when('/pms/:projectid/Result', {
		templateUrl: '/view/education/pms/select-project-result.html',
		controller: 'selectProjectResultCtrl'
	});
	
	//pms에 프로젝트를 등록하기 위한 화면으로 라우팅
	$routeProvider.when('/pmsRegister', {
		templateUrl: 'view/education/pms/register-project.html',
		controller: 'registerProjectCtrl'
	});
	
	
	//pms의 프로젝트를 수정하기 위한 화면으로 라우팅
	$routeProvider.when('/pms/:projectid/edit', {
		templateUrl: 'view/education/pms/edit-project.html',
		controller: 'editProjectCtrl'
	});
	
	//pms의 프로젝트를 삭제하기 위한 페이지로 라우팅. => 컨트롤러를 거친 이후에 다시 제자리로 돌아오는 과정을 수행한다.
	$routeProvider.when('/pms/removeProject', {
		templateUrl: '/view/education/pms/pms.html',
		controller: 'removeProjectCtrl'
	});
	
	//프로젝트 미리보기 화면으로 라우팅
	$routeProvider.when('/pms/previewProject', {
		templateUrl: '/view/education/pms/preview-project.html',
		controller: 'registerProjectCtrl'
	});
	
	//배심원 관리 페이지로 라우팅
	$routeProvider.when('/juror', {
		templateUrl: '/view/education/jurors/juror-management.html',
		controller: 'jurorManagementCtrl'
	});
	
	//SIG 페이지로 라우팅
	$routeProvider.when('/sig/', {
		templateUrl: '/view/education/sig/sig.html',
		controller: 'sigCtrl'
	});
	
	//SIG 게시판 페이지로 라우팅
	$routeProvider.when('/sig/:signame', {
		templateUrl: '/view/education/sig/sigboard.html',
		controller: 'sigBoardCtrl'
	});
	
	//SIG 게시글 등록 페이지로 라우팅
	$routeProvider.when('/sig/sigboard/registermessage', {
		templateUrl: '/view/education/sig/sigboard-registermessage.html',
		controller: 'sigboardRegisterMessageCtrl'
	});
	
	//SIG 게시글 수정 페이지로 라우팅
	$routeProvider.when('/sig/sigboard/editmessage', {
		templateUrl: '/view/education/sig/sigboard-editmessage.html',
		controller: 'sigboardEditMessageCtrl'
	});
	
	
	//Calendar 페이지로 라우팅
	$routeProvider.when('/calendar', {
		templateUrl: '/view/calendarview.html'
	});
	
	
	
	//Book 페이지로 라우팅
	$routeProvider.when('/book', {
		templateUrl: '/view/book/book-rent.html'
	});
	
	//Book 반납 페이지로 라우팅
	$routeProvider.when('/book/return', {
		templateUrl: '/view/book/book-return.html'
	});
	
	
	//
	//  안혜수 부분
	// 
	
	//회원정보 Angular Viewer로 라우팅
	$routeProvider.when('/members', {
		templateUrl: '/view/living/members.html'
		
	});
	
	//출석 페이지로 라우팅
	$routeProvider.when('/attendance', {
		templateUrl: '/view/living/attendance.html'
	});
	
	//당직 페이지로 라우팅
	$routeProvider.when('/duty', {
		templateUrl: '/view/living/duty.html'
	});
	
	//px 페이지로 라우팅
	$routeProvider.when('/px', {
		templateUrl: '/view/px/px-buy.html'
	});
	
	//px 관리자 페이지로 라우팅
	$routeProvider.when('/px/admin', {
		templateUrl: '/view/px/px-admin.html'
	});
	
	$routeProvider.when('/freeboard', {
		templateUrl: '/view/community/freeboard.html',
		controller: 'freeBoardCtrl'
	});
	
	$routeProvider.when('/freeboard/:messageId', {
		templateUrl: '/view/community/freeboard-selectmessage.html',
		controller: 'freeBoardSelectCtrl'
	});	
	
	$routeProvider.when('/freeboardRegister', {
		templateUrl: '/view/community/freeboard-registermessage.html',
		controller: 'freeBoardRegisterMessageCtrl'
	});
	
	$routeProvider.when('/freeboard/editmessage', {
		templateUrl: '/view/community/freeboard-editmessage.html',
		controller: 'freeBoardEditMessageCtrl'
	});
	
	//잘못된 주소 접근은 입장 페이지로 리다이렉트
	$routeProvider.otherwise({
		redirectTo: '/'
	});
	
	$locationProvider.html5Mode(true);
	
}]);




