
app.controller('dutyCalendarviewCtrl', function CalendarviewCtrl($scope, dutyWasService, wasService, calendarAPIService){
	console.log("Duty CalenderView Ctrl");


	var today = new Date();
	
	$scope.Year = today.getFullYear();
	$scope.Month = today.getMonth() + 1;
	
	$scope.viewY = today.getFullYear();
	$scope.viewM = today.getMonth()+1;

	//한달의 스케쥴을 하루 단위로 들고있는 리스트
	$scope.daySchedules = [];
	for(var i=0; i<=31; i++){
		$scope.daySchedules.push([]);
	}
	
	//한달의 스케쥴 미리보기를 하루 단위로 들고 있는 리스트
	$scope.dayPreview = [];
	for(var i=0; i<=31; i++){
		$scope.dayPreview.push(null);
	}
	
	$scope.todaySchedules = {};

	$scope.dayIndex = [];
	for(var i=0; i<42; i++)
		$scope.dayIndex.push('');

	$scope.NumberOfDaysOfMonth = function(y, m){
		return (m*9-m*9%8)/8%2+(m==2?y%4||y%100==0&&y%400?28:29:30);
	};
	
	$scope.BeginDayOfMonth = function(y, m){
		return (y+(y-1-(y-1)%4)/4-(y-1-(y-1)%100)/100+(y-1-(y-1)%400)/400 +m*2+(m*5-m*5%9)/9-(m>2?y%4||y%100==0&&y%400?4:3:2))%7;
	};

	
	$scope.selectDay = function(idxNum){
		console.log("Duty Select Day" + $scope.dayIndex[idxNum]);
		
		var year = $scope.Year;
		var month = $scope.Month;
		
		var beginDay = $scope.BeginDayOfMonth($scope.Year, $scope.Month);
		var endDay = beginDay + $scope.NumberOfDaysOfMonth($scope.Year, $scope.Month);
		
		if(idxNum < beginDay){
			if(month == 1){
				year -= 1;
				month = 12;
			}else{
				month -= 1;
			}
		}
		else if(idxNum > endDay){
			if(month == 12){
				year += 1;
				month = 1;
			}else{
				month += 1;
			}
		}
		
		var date = {
			year: year,
			month: month,
			day: $scope.dayIndex[idxNum]
		};

		calendarAPIService.dateReceiver(date);
		
		//선택하면 그 날짜의 당직 이벤트들을 전달해옴
		$scope.$parent.loadDailyDuties($scope.dayIndex[idxNum]);
		
		// calendarAPIService.dayEventReceiver($scope.daySchedules[date.day]);

		$('#dutyModal').modal('toggle');
	};
	
	$scope.moverDay = function(){
		$('body').css('cursor', 'pointer');
	};
	$scope.mleaveDay = function(){
		$('body').css('cursor', 'default');
	};

	$scope.moveNextMonth = function(){
		if($scope.Month == 12){
			$scope.Year += 1;
			$scope.Month = 1;
		}else{
			$scope.Month += 1;
		}

		//달이 바뀔 때마다 이벤트를 다시 로드
		$scope.$parent.loadDuty($scope.Year, $scope.Month);
	};
	$scope.movePrevMonth = function(){
		if($scope.Month == 1){
			$scope.Year -= 1;
			$scope.Month = 12;
		}else{
			$scope.Month -= 1;
		}
		//달이 바뀔 때마다 이벤트를 다시 로드
		$scope.$parent.loadDuty($scope.Year, $scope.Month);
	};

	
	$scope.$watch('Year', function(){
		$scope.refreshCalendar($scope.Year, $scope.Month);
	});
	$scope.$watch('Month', function(){
		$scope.refreshCalendar($scope.Year, $scope.Month);
	});

	$scope.refreshCalendar = function(y, m){

		var prevNumberOfDaysOfMonth = 0;
		if(m-1 > 0)	
			prevNumberOfDaysOfMonth = $scope.NumberOfDaysOfMonth(y, m-1);
		else
			prevNumberOfDaysOfMonth = $scope.NumberOfDaysOfMonth(y-1, 12);	

		var numberOfDaysOfMonth = $scope.NumberOfDaysOfMonth(y, m);
		var beginDayOfMonth = $scope.BeginDayOfMonth(y, m);
	

		for(var i=beginDayOfMonth; i<beginDayOfMonth + numberOfDaysOfMonth; i++){
			$scope.dayIndex[i] = i-beginDayOfMonth+1;
			if(i % 7 != 0 && i % 7 != 6)
				$('#day' + i + 'div').css('color', 'black');
			else{
				if(i % 7 == 0)
					$('#day' + i + 'div').css('color', 'red');
				else if(i % 7 == 6)
					$('#day' + i + 'div').css('color', 'blue');
			}
		}
		
		//이전 달의 일자들
		for(var i=0; i<beginDayOfMonth; i++){
			$scope.dayIndex[i] = prevNumberOfDaysOfMonth - beginDayOfMonth + i + 1;
			$('#day' + i + 'div').css('color', 'rgb(218, 218, 218)');
		}		
		//다음 달의 일자들
		for(var i=beginDayOfMonth + numberOfDaysOfMonth; i<42; i++){
			$scope.dayIndex[i] = i-(beginDayOfMonth + numberOfDaysOfMonth)+1;
			$('#day' + i + 'div').css('color', 'rgb(218, 218, 218)');
		}
	};

	$scope.showCalendar = function(y, m) { 
		console.log("Show Calendar");
		
		$scope.refreshCalendar(y, m);
		
		//parent의 scope로부터 데이터를 받아와서 뿌려줄 수 있도록 하자.
		var previewCallback = function(data){
			console.log("Preview Callback");

			for(var i=0; i<data.length; i++){
//				var day = i;//parseInt(monthlyDuties[i].dutydate.substring(8, 10));
				var muser1 = data[i].m1_username;
				var muser2 = data[i].m2_username;
				var fuser1 = data[i].f1_username;
				var fuser2 = data[i].f2_username;
				
				$scope.dayPreview[beginDayOfMonth + i] = i;//monthlyDuties[i].m1_username + "\r\n" + monthlyDuties[i].f1_username;
			}
		};
		$scope.$parent.loadDuty($scope.Year, $scope.Month, previewCallback);
	};
	
	//달력 초기화
	// $scope.showCalendar($scope.viewY, $scope.viewM);
	$scope.refreshCalendar($scope.viewY, $scope.viewM);
});