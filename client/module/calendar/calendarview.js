

app.controller('calendarviewCtrl', function CalendarviewCtrl($scope, calendarAPIService, wasService){
	console.log("CalenderView Ctrl");


	var today = new Date();
	
	$scope.Year = today.getFullYear();
	$scope.Month = today.getMonth() + 1;

	$scope.viewY = today.getFullYear();
	$scope.viewM = today.getMonth()+1;

	//한달의 스케쥴을 하루 단위로 들고있는 리스트
	$scope.daySchedules = [];
	for(var i=0; i<=31; i++){
		$scope.daySchedules.push([]);
	}
	
	//한달의 스케쥴 미리보기를 하루 단위로 들고 있는 리스트
	$scope.dayPreview = [];
	for(var i=0; i<=31; i++){
		$scope.dayPreview.push(null);
	}
	

	$scope.todaySchedules = {};

	$scope.dayIndex = [];
	for(var i=0; i<42; i++)
		$scope.dayIndex.push('');

	$scope.NumberOfDaysOfMonth = function(y, m){
		return (m*9-m*9%8)/8%2+(m==2?y%4||y%100==0&&y%400?28:29:30);
	};
	
	$scope.BeginDayOfMonth = function(y, m){
		return (y+(y-1-(y-1)%4)/4-(y-1-(y-1)%100)/100+(y-1-(y-1)%400)/400 +m*2+(m*5-m*5%9)/9-(m>2?y%4||y%100==0&&y%400?4:3:2))%7;
	};

	
	$scope.selectDay = function(idxNum){
		console.log("Select Day" + $scope.dayIndex[idxNum]);
		
		var year = $scope.Year;
		var month = $scope.Month;
		
		var beginDay = $scope.BeginDayOfMonth($scope.Year, $scope.Month);
		var endDay = beginDay + $scope.NumberOfDaysOfMonth($scope.Year, $scope.Month);
		
		if(idxNum < beginDay){
			if(month == 1){
				year -= 1;
				month = 12;
			}else{
				month -= 1;
			}
		}
		else if(idxNum > endDay){
			if(month == 12){
				year += 1;
				month = 1;
			}else{
				month += 1;
			}
		}
		
		var date = {
			year: year,
			month: month,
			day: $scope.dayIndex[idxNum]
		};

		calendarAPIService.dateReceiver(date);
		calendarAPIService.dayEventReceiver($scope.daySchedules[date.day]);

	};
	
	$scope.moverDay = function(){
		$('body').css('cursor', 'pointer');
	};
	$scope.mleaveDay = function(){
		$('body').css('cursor', 'default');
	};

	var readEventCallback = function(data){
		console.log(data);
		
		if(data == null)
			return ;
		
		$scope.schedules = data;
		$scope.daySchedules = [];
		for(var i=0; i<=31; i++){
			$scope.daySchedules.push([]);
		}
		
		for(var i=0; i<$scope.schedules.length; i++){
			var day = parseInt($scope.schedules[i].start.dateTime.substring(8, 10));

			$scope.daySchedules[day].push($scope.schedules[i]);

////////////////////////Preview 등록 부분////////////////////////////////////

			var firstSchedulePreview = "";
			var secondSchedulePreview = "";
			var thirdSchedulePreview = "";
			
			for(var j=0; j<$scope.daySchedules[day].length; j++){
				
				//3개 이상의 미리보기는 지원하지 않음.
				if(thirdSchedulePreview != "")
					break;
				if(firstSchedulePreview == ""){
					firstSchedulePreview = $scope.daySchedules[day][j].summary.substring(0, 10);
				}
				else if(secondSchedulePreview == ""){
					secondSchedulePreview = $scope.daySchedules[day][j].summary.substring(0, 10);
				}
				else if(thirdSchedulePreview == ""){
					thirdSchedulePreview = $scope.daySchedules[day][j].summary.substring(0, 10);
				}
			}
			
			var beginDay = $scope.BeginDayOfMonth($scope.Year, $scope.Month);
			
			$scope.dayPreview[beginDay + day - 1] = firstSchedulePreview + "\n" + secondSchedulePreview + "\n" + thirdSchedulePreview;
/////////////////////////////////////////////////////////////////////////
		}
		
		$scope.todaySchedules = $scope.daySchedules[$scope.Day];
		
		if(!$scope.$$phase){
			$scope.$apply();
		}
	};
	
	$scope.loadEvents = function(){
		var modifiedMonth = $scope.Month;
		var modifiedDay = $scope.Day;
		if(modifiedMonth < 10){
			modifiedMonth = "0" + $scope.Month;
		}
		if(modifiedDay < 10){
			modifiedDay = "0" + $scope.Day;
		}
		
		for(var i=0; i<42; i++){
			$scope.dayPreview[i] = '';
		}
		
		calendarAPIService.readEvent(token, realid, $scope.Year + "-" + modifiedMonth + "-01", readEventCallback);
	};
	

	$scope.moveNextMonth = function(){
		if($scope.Month == 12){
			$scope.Year += 1;
			$scope.Month = 1;
		}else{
			$scope.Month += 1;
		}
		//달이 바뀔 때마다 이벤트를 다시 로드
		$scope.loadEvents();
	};
	$scope.movePrevMonth = function(){
		if($scope.Month == 1){
			$scope.Year -= 1;
			$scope.Month = 12;
		}else{
			$scope.Month -= 1;
		}
		//달이 바뀔 때마다 이벤트를 다시 로드
		$scope.loadEvents();
	};

	
	$scope.$watch('Year', function(){
		// $scope.showCalendar($scope.Year, $scope.Month);
		$scope.refreshCalendar($scope.Year, $scope.Month);
	});
	$scope.$watch('Month', function(){
		// $scope.showCalendar($scope.Year, $scope.Month);
		$scope.refreshCalendar($scope.Year, $scope.Month);
	});

	$scope.refreshCalendar = function(y, m){
		var prevNumberOfDaysOfMonth = 0;
		if(m-1 > 0)	
			prevNumberOfDaysOfMonth = $scope.NumberOfDaysOfMonth(y, m-1);
		else
			prevNumberOfDaysOfMonth = $scope.NumberOfDaysOfMonth(y-1, 12);	

		var numberOfDaysOfMonth = $scope.NumberOfDaysOfMonth(y, m);
		var beginDayOfMonth = $scope.BeginDayOfMonth(y, m);


		for(var i=beginDayOfMonth; i<beginDayOfMonth + numberOfDaysOfMonth; i++){
			$scope.dayIndex[i] = i-beginDayOfMonth+1;
			if(i % 7 != 0 && i % 7 != 6)
				$('#day' + i + 'div').css('color', 'black');
			else{
				if(i % 7 == 0)
					$('#day' + i + 'div').css('color', 'red');
				else if(i % 7 == 6)
					$('#day' + i + 'div').css('color', 'blue');
			}
		}
		
		//이전 달의 일자들
		for(var i=0; i<beginDayOfMonth; i++){
			$scope.dayIndex[i] = prevNumberOfDaysOfMonth - beginDayOfMonth + i + 1;
			$('#day' + i + 'div').css('color', 'rgb(218, 218, 218)');
		}		
		//다음 달의 일자들
		for(var i=beginDayOfMonth + numberOfDaysOfMonth; i<42; i++){
			$scope.dayIndex[i] = i-(beginDayOfMonth + numberOfDaysOfMonth)+1;
			$('#day' + i + 'div').css('color', 'rgb(218, 218, 218)');
		}
	};

	$scope.showCalendar = function(y, m) { 
		console.log("Show Calendar");
		
		$scope.refreshCalendar(y, m);
		
		// $scope.loadEvents();
	}; 

	//초기에 이벤트롤 로드
	// $scope.loadEvents();
	
	
	//달력 초기화
	$scope.showCalendar($scope.viewY, $scope.viewM);
});

app.controller('calendarScheduleCtrl', function CalendarScheduleCtrl($scope, calendarAPIService){
	console.log("Calendar Schedule Ctrl");

	$scope.Year = null;
	$scope.Month = null;
	$scope.Day = null;

	$scope.addExpand = true;

	$scope.detailList = [];
	
	$scope.schedules = [

	];

	
	$scope.times = [
		"00:00", "01:00","02:00","03:00","04:00","05:00","06:00","07:00","08:00","09:00","10:00","11:00",
		"12:00","13:00","14:00","15:00","16:00","17:00","18:00","19:00","20:00","21:00","22:00","23:00"
	];
	
	
	$scope.overDetail = function($index){
		$('body').css('cursor', 'pointer');
		$('#schedule'+$index).css('color', 'rgb(58, 161, 247)');
	};
	$scope.leaveDetail = function($index){
		$('body').css('cursor', 'default');
		$('#schedule'+$index).css('color', 'black');
	};
	

	var dayEvent = function(data){
		console.log(data);
		$scope.schedules = data;
	};

	calendarAPIService.dayEventProvider(dayEvent);

	
	$scope.openDetails= function($index){
		console.log("Open " + $index);
		
		$scope.addExpand = !$scope.addExpand;
		$scope.detailList[$index] = ! $scope.detailList[$index];
		
		//제니가 api 업데이트 해줄 때까지 대기
		
		$('#titleEdit' + $index).val($scope.schedules[$index].summary);
		// $('#beginTimeEdit').val($scope.schedules[$index].start.dateTime);
		// $('#endTimeEdit').val($scope.schedules[$index].end.dateTime);
		$('#locationEdit' + $index).val($scope.schedules[$index].location);
		$('#descriptionEdit' + $index).val($scope.schedules[$index].description);
	};	
	

	var dateCallback = function(date){
		console.log(date);
		$scope.Year = date.year;
		$scope.Month = date.month;
		$scope.Day = date.day;

	};
	calendarAPIService.dateProvider(dateCallback);


			
	if(!$scope.$$phase){
		$scope.$apply();
	}

	/***
	 *		$index 번째 폼 들에 있는 데이터를 REST로 update 시킨다. 
 	 * @param {Object} $index
	 */
	$scope.updateEvent = function($index){
		console.log('Update Event');
		
		var titleEdit = $('#titleEdit'+$index).val();
		var beginTimeEdit = $scope.Year;
		var endTimeEdit = $scope.Year;
		var locationEdit = $('#locationEdit'+$index).val();
		var descriptionEdit = $('#descriptionEdit'+$index).val();

		var modifiedMonth = $scope.Month;
		var modifiedDay = $scope.Day;
		if(modifiedMonth < 10){
			modifiedMonth = "0" + $scope.Month;
		}
		if(modifiedDay < 10){
			modifiedDay = "0" + $scope.Day;
		}

		beginTimeEdit += "-" + modifiedMonth + "-" + $scope.Day + " " + $('#beginTimeEdit').val() + ":00";
		endTimeEdit += "-" + modifiedMonth + "-" + $scope.Day + " " + $('#endTimeEdit').val() + ":00";
		
		var accountId = realid;
		var eventId = $scope.schedules[$index].id;
		
		var reqData = {
			event_id: eventId,
			account_id: accountId,
			summary: titleEdit,
			description: descriptionEdit,
			location: locationEdit,
			start_datetime: beginTimeEdit,
			end_datetime: endTimeEdit
		};
		console.log(reqData);
		
		var updateEventCallback = function(data){
			console.log(data);

		
			if(!$scope.$$phase){
				$scope.$apply();
			}
		};
		calendarAPIService.updateEvent(token, $scope.schedules[$index].id, reqData, updateEventCallback);
	};

	$scope.deleteEvent = function($index){
		console.log('Delete Event');
		
		var deleteEventCallback = function(data){
			console.log(data);
			
			if(!$scope.$$phase){
				$scope.$apply();
			}			
		};
		
		var accountId = realid;
		
		calendarAPIService.deleteEvent(token, accountId, $scope.schedules[$index].id,deleteEventCallback);
	};


	$scope.createEvent = function(){
		console.log("Create Event");
		
		var titleInfo = $('#titleContext').val();
		var startTime = $scope.Year + "-";// + $scope.Month + "-" + $scope.Day + " " + $('#beginTime').val();
		var endTime = $scope.Year + "-";// + $scope.Month + "-" + $scope.Day + " " + $('#endTime').val();

		var modifiedMonth = $scope.Month;
		var modifiedDay = $scope.Day;
		if(modifiedMonth < 10){
			modifiedMonth = "0" + $scope.Month;
		}
		if(modifiedDay < 10){
			modifiedDay = "0" + $scope.Day;
		}

		startTime += modifiedMonth + "-" + $scope.Day + " " + $('#beginTime').val() + ":00";
		endTime += modifiedMonth + "-" + $scope.Day + " " + $('#endTime').val() + ":00";
		
		
		var locationInfo = $('#locationContext').val();
		var descriptionInfo = $('#descriptionContext').val();
		
		var reqData = {
			account_id: id,
			summary: titleInfo,
			description: descriptionInfo,
			location: locationInfo,
			start_datetime: startTime,
			end_datetime: endTime
		};
		
		var createEventCallback = function(data){
			console.log(data);
			if(!$scope.$$phase){
				$scope.$apply();
			}
		};
		
		$('#titleContext').val("");
		$('#locationContext').val('');
		$('#descriptionContext').val('');
		
		calendarAPIService.createEvent(token, reqData, createEventCallback);
	};


});


