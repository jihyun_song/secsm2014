

app.controller('jurorManagementCtrl', function($scope, educationWasService){
	console.log('Juror Management Control');

	InitializeComponent();

	$scope.projects = [];
	
	$scope.members = [];
	
	$scope.jurors = [[]];
	
	$scope.ProjectName = "";
	$scope.ProjectJurors = "";
	$scope.ProjectTerm = "";
	
	
	$scope.curPage = 0;
	$scope.pageSize = 3;

	$scope.showData = function(){

		var showJurorCallback = function(jurorList){
			$scope.jurors.push(jurorList);
			console.log(jurorList);
		};

		var showProjectCallback = function(projectList){
			$scope.projects = projectList;
			showProjectJurors();
		};
		
		var showMemberCallback = function(memberList){
			$scope.members = memberList;
			console.log(memberList);
		};
		
		$scope.judges = [];
		
		var JurorInfoCallback = function(jurorList){
			
			$scope.members = jurorList;

			for(var i=0; i<$scope.members.length; i++){
				var judgeName = $scope.members[i].username;
				var judgeEntered = $scope.members[i].entered;
				var judgePresentations = [];
				
				for(var j=0; j<$scope.members[i].list.length; j++){
					var judgePresentation = $scope.members[i].list[j].projectname;
					if($scope.members[i].list[j].presentation_type == 1){
						judgePresentation += "(시작)";
					}
					else if($scope.members[i].list[j].presentation_type == 2){
						judgePresentation += "(중간)";
					}
					else if($scope.members[i].list[j].presentation_type == 3){
						judgePresentation += "(완료)";
					}
					judgePresentations.push(judgePresentation);
				}
				var judge = {
					username: judgeName,
					entered: judgeEntered,
					judgeLists: judgePresentations
				};
				
				$scope.judges.push(judge);
			}
			
		};
		
		educationWasService.readJurorInformation(token, JurorInfoCallback);

		$scope.numberOfPages = function(){
			return Math.ceil($scope.projects.length / $scope.pageSize);
		};
	};
	
	if(!$scope.$$phase){
		console.log("PMS Scope Applied");
		$scope.$apply();
	}		
	
});


app.controller('editJurorsCtrl', function($scope, wasService){
	console.log("Edit Jurors Control");
	
	$scope.members = [];
	
	var callback = function(memberList){
		$scope.members = memberList;
	};

	wasService.readMembers(token, callback);
	
});

