


app.controller('sigBoardCtrl', function SigBoardCtrl($scope, $routeParams, sigService){
	console.log("Sig Board Control");
	
	var sigInfo = sigService.getSelectSig();

	InitializeComponent();


	$scope.SigName = $routeParams.signame;
});

app.controller('sigMessageListingCtrl', function SigMessageListingCtrl($scope, sigService){
	console.log("Sig Message Listing Control");

	$scope.boardmessages = [
		{
			number: '1',
			context: '안녕하세요. 시그입니다.',
			writer: 'Test guy',
			due: '01/09 12:30'
		},
		{
			number: '2',
			context: '안녕하세요. 시그장임다.',
			writer: 'Master guy',
			due: '01/09 12:31'
		},
		{
			number: '3',
			context: '안녕하세요. 시그원입니다.',
			writer: 'Sig guy',
			due: '01/09 12:33'
		}
	];
	
});

app.controller('sigBoardManageCtrl', function SigBoardManageCtrl($scope, sigService){
	console.log("Sig Board Manage Control");
	
	
	$scope.registerMessage = function(){
		console.log("Register Message");
		
		location.href = "#sig/sigboard/registermessage";
	};
	
	$scope.editMessage = function(){
		console.log("Edit Message");
		
	};
	
	$scope.removeMessage = function(){
		console.log("Remove Message");
		
	};
	
});

