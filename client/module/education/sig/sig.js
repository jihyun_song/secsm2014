

app.controller('sigCtrl', function($scope, wasService){
	console.log("Sig Control");

	InitializeComponent();

});


app.controller('sigListingCtrl', function($scope, sigService){
	
	console.log('Sig Listing Control');
	
	$scope.mysigs = [
		{
			signame: "Calgorithm",
			siginfo: "C를 이용한 알고리즘 문제풀이 시그",
			sigmaster: "전재검"
		},
		{
			signame: "머신러닝 시그",
			siginfo: "머신러닝 및 통계와 수학을 배우는 시그",
			sigmaster: "박진상"
		}	
	];
	
	$scope.allsigs = [
		{
			signame: "Calgorithm",
			siginfo: "C를 이용한 알고리즘 문제풀이 시그",
			sigmaster: "전재검"			
		},
		{
			signame: "머신러닝 시그",
			siginfo: "머신러닝 및 통계와 수학을 배우는 시그",
			sigmaster: "박진상"		
		},
		{
			signame: "임베디드 시그",
			siginfo: "하드웨어와 소프트웨어를 이용한 임베디드 프로그래밍을 하는 시그",
			sigmaster: "정애경"
		},
		{
			signame: "웹 시그",
			siginfo: "웹을 공부하는 시그",
			sigmaster: "누군가"			
		},
		{
			signame: "롤 시그",
			siginfo: "롤을 배우는 시그",
			sigmaster: "보이"
		},
		{
			signame: "전처리 시그",
			siginfo: "전을 처리하는 시그",
			sigmaster: "김춘초"
		}
	];
	
	
});

