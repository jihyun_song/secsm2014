
/***
 *		프로젝트 보기 컨트롤 
 */


app.controller('selectProjectCtrl', function SelectProjectCtrl($scope, $routeParams, educationWasService, projectService){

	InitializeComponent();

	$scope.projectid = $routeParams.projectid;
	
	$scope.ProjectName = "";
	$scope.ProjectMembers = "";
	
	$scope.creativeEvalRatio = 100;
	$scope.usefulEvalRatio = 80;
	$scope.technicalEvalRatio = 60;
	$scope.successfulEvalRatio = 100;
	

	$scope.beginevals = [
	];

	$scope.middleevals = [
	];

	$scope.endevals = [
	];	

	$scope.members = [];
	
	$scope.beginJurors = [];
	$scope.notBeginJurors = [];
	$scope.middleJurors = [];
	$scope.notMiddleJurors = [];
	$scope.endJurors = [];
	$scope.notEndJurors = [];
	$scope.settingJuror = -1;



	$scope.setJurorFlag = function(number){
		console.log("Set Juror Flag");
		$scope.settingJuror = number;
		console.log($scope.settingJuror);
	};

	
	$scope.askJuror = function(type){
		if(type == 0){
			if(confirm($scope.ProjectName + " 시작 발표 배심원을 신청하시겠습니까?") != 0){
				//신청을 클릭할 경우
				
				var askJurorCallback = function(data){
					console.log(data);
				};
				
				//id, projectid, type, callback
				educationWasService.createJuror(token, realid, $scope.projectid, type+1, askJurorCallback);
				
			}else{
				//취소할 경우
			}
		}
		else if(type == 1){
			if(confirm($scope.ProjectName + " 중간 발표 배심원을 신청하시겠습니까?") != 0){
				//신청을 클릭할 경우
				
				var askJurorCallback = function(data){
					console.log(data);
				};
				
				//id, projectid, type, callback
				educationWasService.createJuror(token, realid, $scope.projectid, type+1, askJurorCallback);
				
			}else{
				//취소할 경우
			}
		}
		else if(type == 2){
			if(confirm($scope.ProjectName + " 완료 발표 배심원을 신청하시겠습니까?") != 0){
				//신청을 클릭할 경우
				
				var askJurorCallback = function(data){
					console.log(data);
				};
				
				//id, projectid, type, callback
				educationWasService.createJuror(token, realid, $scope.projectid, type+1, askJurorCallback);
				
			}else{
				//취소할 경우
			}
		}
	};
	
	
	$scope.$watch('beginJurors', function(){
		// console.log("Begin Jurors Changed");
	});
	
	$scope.$watch('notBeginJurors', function(){
		// console.log("Not Begin Jurors Changed");
	});
	


	$scope.mhoverpanel = function(){
		$('body').css('cursor', 'pointer');
	};
	
	$scope.mleavepanel = function(){
		$('body').css('cursor', 'default');
	};
	

	var showProjectInfo = function(projectInfo){
		console.log(projectInfo);
		$scope.ProjectName = projectInfo.projectname;
		
		var membersText = "";
		if(projectInfo.team1name != null)
			membersText += projectInfo.team1name;
		if(projectInfo.pl == projectInfo.team1)
			membersText += "(PL)";
		if(projectInfo.team2name != null)
			membersText += ", " + projectInfo.team2name;
		if(projectInfo.pl == projectInfo.team2)
			membersText += "(PL)";
		if(projectInfo.team3name != null)
			membersText += ", " + projectInfo.team3name;
		if(projectInfo.pl == projectInfo.team3)
			membersText += "(PL)";
		if(projectInfo.team4name != null)
			membersText += ", " + projectInfo.team4name;
		if(projectInfo.pl == projectInfo.team4)
			membersText += "(PL)";
		
		$scope.ProjectMembers = membersText;
		$scope.ProjectSummary = projectInfo.summary;
		
		$scope.ProjectTerm = projectInfo.startmonth + " ~ " + projectInfo.endmonth;
		
		$scope.ProjectCreativeRatio = projectInfo.creative_rate;
		$scope.ProjectUsefulRatio = projectInfo.useful_rate;
		$scope.ProjectTechnicalRatio = projectInfo.technical_rate;
		$scope.ProjectSuccessfulRatio = projectInfo.successful_rate;
	};

	
	
	/***
	 *		PL인지 아닌지를 판단하는 함수
	 */
	var isPl = function(memberId, plId){
		if(memberId == plId){
			return true;
		}
		return false;
	};
	
	$scope.loadProjectInfo = function(){
		educationWasService.readProject(token, $routeParams.projectid, showProjectInfo);
	};

	//edit and remove
	$scope.editProject = function(){
		//redirecting
		location.href = "#pms/" + $scope.projectid + "/edit";
	};
	
	$scope.removeProject = function(){
		
		var deleteProjectCallback = function(){
			alert("프로젝트 데이터가 성공적으로 삭제되었습니다.");
		};
		
		educationWasService.deleteProject(token, $scope.projectid, deleteProjectCallback);
	};
	
	if(!$scope.$$phase){
		console.log("PMS Result Scope Applied");
		$scope.$apply();
	}
});


