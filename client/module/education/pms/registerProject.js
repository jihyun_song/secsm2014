


/***
 *
 * 		프로젝트 등록 컨트롤
 *  
 */


app.controller('registerProjectCtrl', function RegisterProjectCtrl($scope, projectService, educationWasService){
	console.log('Register Project Control');
	
	InitializeComponent();
	
	$scope.members = [];							//전체 회원 리스트
	$scope.attachedFile = "";						//첨부된 파일 목록

	$scope.doMembers = [];							//프로젝트를 진행하는 회원 리스트
	$scope.undoMembers = [];						//프로젝트를 진행하지않는 회원 리스트
	$scope.doLeader = "";							//프로젝트의 리더 회원
	
	$scope.teamLeaderText = "";
	$scope.teamMemberText = "";
	
	/***
	 *		DatePicker Configuration 
	 */	
	$('.form_datetime').datepicker({
		format: 'yy-mm-dd',
		todaybtn: true,
		pickerPosition: "bottom-left"
	}).on('changeDate', function(event){
		$('.datepicker').hide();
	});
	
	
	$scope.$watch('doMembers', function(){
		console.log("doMember Change");
		console.log($scope.doMembers);
	});

	$scope.$watch('doLeader', function(){
		console.log('doLeader Change');
		console.log($scope.doLeader);
	});

	
	/***
	 *		pms 프로젝트 등록
	 */
	$scope.clickRegister = function($files){
		console.log("Click Register");
		

		var projectName = $('#projectNameTextBox').val();
		var projectMembers = $('#teamMemberInputForm').val();
		var projectInfo = $('#projectInfoTextArea').val();
		var projectBegin = $('#beginDateForm').val();
		var projectEnd = $('#endDateForm').val();

		var projectCreativeRate = $('#creative_rate_select').val();
		var projectUsefulRate = $('#useful_rate_select').val();
		var projectTechnicalRate = $('#technical_rate_select').val();
		var projectSuccessfulRate = $('#successful_rate_select').val();
		
		
		var projectTeamMembers = $scope.$parent.doMembers;
		var projectTeamLeader = $scope.$parent.doLeader;
		
		console.log(projectTeamLeader);
		
		var temp_team = [];
		var temp_team_name = [];
		for(var i=0; i<4; i++){
			temp_team.push(null);
			temp_team_name.push(null);
		}
		
		for(var i=0; i<projectTeamMembers.length; i++){
			if(projectTeamMembers[i].username.length < 2){
				continue;
			}
			temp_team[i] = projectTeamMembers[i].id;
			temp_team_name[i] = projectTeamMembers[i].username;
		}
        var data = {
            projectname: projectName,
            summary: projectInfo,
            startmonth: projectBegin,
            endmonth: projectEnd,
            pl: projectTeamLeader.id,
            team1: temp_team[0],
            team1name: temp_team_name[0],
            team2: temp_team[1],
            team2name: temp_team_name[1],
            team3: temp_team[2],
            team3name: temp_team_name[2],
            team4: temp_team[3],
            team4name: temp_team_name[3],
            creative_rate: parseInt(projectCreativeRate),
            technical_rate: parseInt(projectUsefulRate),
            useful_rate: parseInt(projectTechnicalRate),
            successful_rate: parseInt(projectSuccessfulRate)
        };
        
        var callback = function(){
        	location.href = "#pms";
        };

		educationWasService.createProject(token, data, callback);

		projectService.uploadProjectFile($scope.attachedFile);
	};


	$scope.clickPreview = function($files){
		console.log("Click Preview");
		var projectName = $('#projectNameTextBox').val();
		var projectMembers = $('#teamMemberInputForm').val();
		var projectInfo = $('#projectInfoTextArea').val();
		var projectBegin = $('#beginDateForm').val();
		var projectEnd = $('#endDateForm').val();

		var projectTeamMembers = projectMembers.split(';');
		console.log(projectTeamMembers);

		var temp_team = [];
		for(var i=0; i<4; i++)
			temp_team.push("");
		
		
		for(var i=0; i<projectTeamMembers.length; i++){
			if(projectTeamMembers[i].length < 2){
				continue;
			}
			projectTeamMembers[i] = projectTeamMembers[i].trim();
			temp_team[i] = (projectTeamMembers[i].substring(0, projectTeamMembers[i].indexOf('(')));
		}
		
		var projectDetail = {
			projectname: projectName,
			projectmembers: temp_team[0] + ", " + temp_team[1] + ", " + temp_team[2] + ", " + temp_team[3],
			projectinfo: projectInfo,
			projectbegin: projectBegin,
			projectend: projectEnd
		};

		projectService.setSelectProjectDetail(projectDetail);
		
		location.href = "#pms/previewProject";
	};
	
	$scope.clickCancel = function(){
		console.log("Click Cancel");
		location.href="#pms";
	};
	

	$scope.uploadFile = function($files){
		console.log("Upload File");
		console.log($files);

		$scope.attachedFile = $files[0];

	};


	if(!$scope.$$phase){
		console.log("Register PMS Scope Applied");
		$scope.$apply();
	}

});

