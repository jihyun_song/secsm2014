


///PMS 페이지 컨트롤러
app.controller('pmsCtrl', function PMSCtrl($scope, $timeout, educationWasService){
	console.log("PMS Control");

	InitializeComponent();
	
	SpinnerAnimation($timeout, 500, null);

});


/***
 *
 * 		Project Listing Control
 *  
 */

app.controller('projectListingCtrl', function ProjectListingCtrl($scope, projectService, educationWasService){
	console.log("Project Listing Control");

/***
 *		Pagination 모듈 
 */
		
	$scope.projects = [];
	$scope.projectViews = [];
	$scope.checkedIdx = 0;

	$scope.curPage = 0;
	$scope.pageSize = 10;
	$scope.showData = function(){

		var callback = function(projectList){
			console.log(projectList);
			var projectData = projectList;
			var projectViewLists = [];
							
			for(var i=0; i<projectData.length; i++){
				var scopeProjectName = projectData[i].projectname;
				var scopeProjectMembers = "";

				//null 인경우는 굳이 출력하지 않는다.
				if(projectData[i].team1name != null)
					scopeProjectMembers += projectData[i].team1name + ";";
				if(projectData[i].team2name != null)
					scopeProjectMembers += projectData[i].team2name + ";";
				if(projectData[i].team3name != null)
					scopeProjectMembers += projectData[i].team3name + ";";
				if(projectData[i].team4name != null)
					scopeProjectMembers += projectData[i].team4name + ";";

				var scopeProjectDate = "";
				scopeProjectDate = projectData[i].startmonth + " ~ " + projectData[i].endmonth;

				var scopeProjectData = {
					name: scopeProjectName,
					members: scopeProjectMembers,
					term: scopeProjectDate
				};
				projectViewLists.push(scopeProjectData);
			}			
			
			$scope.projectViews = projectViewLists;
			$scope.projects = projectList;
		};
		
		educationWasService.readProjects(token, callback);
		
		$scope.numberOfPages = function(){
			return Math.ceil($scope.projectViews.length / $scope.pageSize);
		};
	};


	$scope.projectMouseOver = function(){
		$('body').css('cursor', 'pointer');
	};
	
	$scope.projectMouseLeave = function(){
		$('body').css('cursor', 'default');
	};
	
	
	$scope.checkProject = function($index){
		$scope.checkedIdx = $index;
		
	};
	
	
	$scope.projectSelect = function($index){
		//index 를 페이지별로 계산하여, 해당 인덱스 번째 프로젝트 항목의 id로 포워딩한다.
		console.log($scope.projects[$index].id);
		$('body').css('cursor', 'default');
		var new_index = ($scope.curPage * $scope.pageSize) + $index;
		location.href = "/pms/" + $scope.projects[new_index].id;
	};

	
	$scope.registerProject = function(){
		console.log("Register Project");
		
		location.href = "/pmsRegister";
	};


	if(!$scope.$$phase){
		console.log("PMS Scope Applied");
		$scope.$apply();
	}
});


