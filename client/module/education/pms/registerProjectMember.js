
/***
 *		registerProject/registerProjectMember => 프로젝트 등록시 멤버를 추가하는 뷰에 대한 컨트롤러 
 */

app.controller('registerMemberCtrl', function SearchTeamMemberCtrl($scope, wasService, projectService){

	InitializeComponent();

	$scope.members = [];
	$scope.doMembers = [];
	$scope.undoMembers = [];
	
	$scope.SearchMembers = function(){
		var searchMemberCallback = function(memberList){
			console.log(memberList);
			$scope.members = memberList;
			
			for(var i=0; i<memberList.length; i++){
				var doFlag = 0;
				for(var j=0; j<$scope.doMembers.length; j++){
					if(doMembers[j].id == $scope.members[i].id){
						doFlag = 1;
						break;
					}
				}
				if(doFlag == 0){
					$scope.undoMembers.push($scope.members[i]);
				}
			}
		};

		wasService.readMembers(token, searchMemberCallback);
	};
	
	$scope.appendTeamMembers = function($event, $index){
		console.log("Append Members");

		//회원 이름과 기수를 바탕으로 하는 회원정보 조합을 Readonly 텍스트로 추가.
		var memberName = ($event.target).innerHTML;
		var memberGrade = ($($event.target).closest('tr')[0].children[1]).innerHTML;
		var memberText = memberName + "(" + memberGrade + ")";

		$scope.doMembers.push($scope.undoMembers[$index]);
		$scope.undoMembers.splice($index, 1);		
		
		
		var originText = $('#addQueue').val();
		originText += memberText + "; ";
		$('#addQueue').val(originText);
	};
	
	$scope.applyAppendings = function(){
		console.log("Apply Appendings");
		
		$('#addQueue').val("");
		
		var memberText = "";
		for(var i=0; i<$scope.doMembers.length; i++){
			memberText += $scope.doMembers[i].username + "(" + $scope.doMembers[i].entered + "); ";
		}

		$scope.$parent.doMembers = $scope.doMembers;
		$('#teamMemberInputForm').val(memberText);
	};
});

