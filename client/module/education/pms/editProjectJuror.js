
app.controller('editProjectJurorCtrl', function EditProjectJurorCtrl($scope, $routeParams, wasService, educationWasService){
	console.log("Edit Project Juror Ctrl");

	InitializeComponent();

	var projectId = $routeParams.projectid;

	$scope.evalJurors = [];
	$scope.settingJurors = -1;

	$scope.$parent.$watch('settingJuror', function(data){
		console.log("Value");
		console.log(data);
		
		if(data == 0){
			$scope.members = $scope.$parent.notBeginJurors;
			$scope.evalJurors = $scope.$parent.beginJurors;
			$scope.settingJurors = 0;
		}
		else if(data == 1){
			$scope.members = $scope.$parent.notMiddleJurors;
			$scope.evalJurors = $scope.$parent.middleJurors;
			$scope.settingJurors = 1;
		}
		else if(data == 2){
			$scope.members = $scope.$parent.notEndJurors;
			$scope.evalJurors = $scope.$parent.endJurors;
			$scope.settingJurors = 2;
		}
		console.log($scope.members);
		console.log($scope.evalJurors);
	});

	var showMembersCallback = function(memberList){

		$scope.members = memberList;
		
		var existMembers = "";
		
		if($scope.$parent.settingJuror == 0){
			for(var i=0; i<$scope.$parent.beginJurors.length; i++){
				existMembers += $scope.$parent.beginJurors[i].username + "; ";
			}
		}
		else if($scope.$parent.settingJuror == 1){
			for(var i=0; i<$scope.$parent.middleJurors.length; i++){
				existMembers += $scope.$parent.middleJurors[i].username + "; ";
			}
		}
		else if($scope.$parent.settingJuror == 2){
			for(var i=0; i<$scope.$parent.endJurors.length; i++){
				existMembers += $scope.$parent.endJurors[i].username + "; ";
			}
		}
		

		for(var i=0; i<memberList.length; i++){
			var beginflag = 0;
			for(var j=0; j<$scope.$parent.beginJurors.length; j++){
				
				if($scope.$parent.beginJurors[j].account_id == memberList[i].id){
					beginflag = 1;
					break;
				}
			}
			if(beginflag == 0)
				$scope.$parent.notBeginJurors.push(memberList[i]);
				
			var middleflag = 0;
			for(var j=0; j<$scope.$parent.middleJurors.length; j++){
				if($scope.$parent.middleJurors[j].account_id == memberList[i].id){
					middleflag = 1;
					break;
				}
			}
			if(middleflag == 0)
				$scope.$parent.notMiddleJurors.push(memberList[i]);
				
			var endflag = 0;
			for(var j=0; j<$scope.$parent.endJurors.length; j++){
				if($scope.$parent.endJurors[j].account_id == memberList[i].id){
					endflag = 1;
					break;
				}
			}
			if(endflag == 0)
				$scope.$parent.notEndJurors.push(memberList[i]);
		}
	};
	
	$scope.showMembers = function(){
		wasService.readMembers(token, showMembersCallback);
	};


	$scope.deleteModalJuror = function($index){
		
		var jurorToDelete = $scope.evalJurors[$index];
		
		console.log("Delete Modal");
		console.log(jurorToDelete);
		
		if($scope.settingJurors == 0){
			for(var i=0; i<$scope.$parent.beginJurors.length; i++){
				if(jurorToDelete.account_id == $scope.$parent.beginJurors[i].account_id){
					//splice. -> 오류 있을 우려가 있다.
					$scope.$parent.notBeginJurors.push($scope.$parent.beginJurors[i]);
					$scope.$parent.beginJurors.splice(i, 1);
				}
			}
		}
		else if($scope.settingJurors == 1){
			for(var i=0; i<$scope.$parent.middleJurors.length; i++){
				if(jurorToDelete.account_id == $scope.$parent.middleJurors[i].account_id){
					//splice. -> 오류 있을 우려가 있다.
					$scope.$parent.notMiddleJurors.push($scope.$parent.middleJurors[i]);
					$scope.$parent.middleJurors.splice(i, 1);
				}
			}
		}
		else if($scope.settingJurors == 2){
			for(var i=0; i<$scope.$parent.endJurors.length; i++){
				if(jurorToDelete.account_id == $scope.$parent.endJurors[i].account_id){
					//splice. -> 오류 있을 우려가 있다.
					$scope.$parent.notEndJurors.push($scope.$parent.endJurors[i]);
					$scope.$parent.endJurors.splice(i, 1);
				}
			}
		}
		
		
	};


	$scope.newlyJurors = [];


	/***
	 * 		Modal 이기 때문에, $parent의 scope에 접근해서 작업을 해줘야 한다.
	 * @param {Object} $index
	 */
	$scope.memberCheck = function($index){
		console.log("Member Checked");
		if($scope.$parent.settingJuror == 0){
			console.log("Push to begin Juror");
			$scope.$parent.beginJurors.push($scope.$parent.notBeginJurors[$index]);
			$scope.newlyJurors.push($scope.$parent.notBeginJurors[$index]);
			$scope.$parent.notBeginJurors.splice($index, 1);
		}
		else if($scope.$parent.settingJuror == 1){
			console.log("Push to middle Juror");
			$scope.$parent.middleJurors.push($scope.$parent.notMiddleJurors[$index]);
			$scope.newlyJurors.push($scope.$parent.notMiddleJurors[$index]);
			$scope.$parent.notMiddleJurors.splice($index, 1);
		}
		else if($scope.$parent.settingJuror == 2){
			console.log("Push to end Juror");
			$scope.$parent.endJurors.push($scope.$parent.notEndJurors[$index]);
			$scope.newlyJurors.push($scope.$parent.notEndJurors[$index]);
			$scope.$parent.notEndJurors.splice($index, 1);
		}
		
		if(!$scope.$$phase){
			$scope.$apply();
		}
		
	};
	
	/***
	 * 		Modal의 Apply 버튼을 눌러서 변화를 적용시키는 함수
	 */
	$scope.applyAppendings = function(){
		console.log("Apply Appendings");
		
		//모달의 상위 레이어에 변경된 값을 전달
		if($scope.$parent.settingJuror == 0){
			$scope.$parent.beginJurors = $scope.$parent.beginJurors;
		}
		else if($scope.$parent.settingJuror == 1){
			$scope.$parent.middleJurors = $scope.$parent.middleJurors;
		}
		else if($scope.$parent.settingJuror == 2){
			$scope.$parent.endJurors = $scope.$parent.endJurors;
		}
		
		var createJurorCallback = function(data){
			console.log(data);
		};

		for(var i=0; i<$scope.newlyJurors.length; i++){
			console.log($scope.newlyJurors[i].id);

			educationWasService.createJuror(token, $scope.newlyJurors[i].id, projectId, $scope.$parent.settingJuror+1, createJurorCallback);
		}
		$scope.newlyJurors = [];
		//초기화 작업.
		if(!$scope.$$phase){
			$scope.$apply();
		}
	};
	
});



