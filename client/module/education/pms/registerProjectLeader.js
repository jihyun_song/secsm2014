

/***
 *		registerProject/registerProjectLeader => 프로젝트 등록시 리더를 추가하는 뷰에 대한 컨트롤러 
 */


app.controller('registerLeaderCtrl', function RegisterLeaderCtrl($scope, projectService){
	console.log("Register Leader Control");
	
	InitializeComponent();
	
	$scope.members = $scope.$parent.doMembers;
	
	$scope.SearchLeader = function(){
		console.log("Init");
	};
	
	$scope.$parent.$watch('doMembers', function(){
		$scope.members = $scope.$parent.doMembers;
	});
	
	$scope.applySelecting = function($index){
		console.log($scope.members[$index]);
		$scope.$parent.doLeader = $scope.members[$index];
		$('#teamLeaderInputForm').val($scope.members[$index].username + "(" + $scope.members[$index].entered + ")");
	};	
});

