


app.controller('editProjectCtrl', function EditProjectCtrl($scope, $routeParams, educationWasService, projectService){
	console.log("Edit Project Controller");

	InitializeComponent();

	$scope.projectInfo = projectService.getSelectProject();

	$scope.projectid = $routeParams.projectid;

	
	/***
	 *		DatePicker Configuration 
	 */	
	$('.form_datetime').datepicker({
		format: 'yy-mm-dd',
		todaybtn: true,
		pickerPosition: "bottom-left"
	}).on('changeDate', function(event){
		$('.datepicker').hide();
	});
	
	var showProjectDetailCallback = function(projectDetail){
		console.log(projectDetail);
		$scope.projectInfo	= projectDetail;
		
		$scope.ProjectName = $scope.projectInfo.projectname;
		$scope.ProjectSummary = $scope.projectInfo.summary;
		
		var memberNames = $scope.projectInfo.team1name;
		if($scope.projectInfo.team2name != null)
			memberNames += ", " + $scope.projectInfo.team2name;
		if($scope.projectInfo.team3name != null)
			memberNames += ", " + $scope.projectInfo.team3name;
		if($scope.projectInfo.team4name != null)
			memberNames += ", " + $scope.projectInfo.team4name;	
	
		$scope.ProjectMembers = memberNames;
	};

	educationWasService.readProject(token, $scope.projectid, showProjectDetailCallback);

	$scope.clickEditUpdate = function(){
		console.log("Click Edit Update");
		console.log($scope.projectInfo);
		var projectId = $scope.projectInfo.id;
		var projectName = $('#projectNameTextBox').val();
		var projectMembers = $('#teamMemberInputForm').val();
		var projectInfo = $('#projectInfoTextArea').val();
		var projectBegin = $('#beginDateForm').val();
		var projectEnd = $('#endDateForm').val();

		var projectTeamMembers = projectService.getSelectProjectMembers();
		
		var projectCreativeRate = $('#creative_rate_select').val();
		var projectUsefulRate = $('#useful_rate_select').val();
		var projectTechnicalRate = $('#technical_rate_select').val();
		var projectSuccessfulRate = $('#successful_rate_select').val();


		var temp_team = [];
		for(var i=0; i<4; i++)
			temp_team.push(null);
		
		for(var i=0; i<projectTeamMembers.length; i++){
			if(projectTeamMembers[i].length < 2){
				continue;
			}
			temp_team[i] = projectTeamMembers[i];
		}
        var updateData = {
			id: projectId,
            projectname: projectName,
            summary: projectInfo,
            startmonth: projectBegin,
            endmonth: projectEnd,
            pl: temp_team[0],
            team1: temp_team[0],
            team2: temp_team[1],
            team3: temp_team[2],
            team4: temp_team[3],
            creative_rate: parseInt(projectCreativeRate),
            technical_rate: parseInt(projectUsefulRate),
            useful_rate: parseInt(projectTechnicalRate),
            successful_rate: parseInt(projectSuccessfulRate)
        };
        
        var updateCallback = function(){
        	location.href = "#pms";
        };	
        		
		educationWasService.updateProject(token, projectId, updateData, updateCallback);
	};
	
	$scope.clickEditPreview = function(){
		console.log("Click Edit Preview");
	};
	
	$scope.clickEditCancel = function(){
		console.log("Click Edit Cancel");
		location.href = "#pms/" + $scope.projectid;
	};
	
});

