

app.service('projectService', ['$upload', function(){
	console.log('Project Service');
	
	
	
	
	var selectedProject = "";
	
	this.setSelectProject = function(project){
		selectedProject = project;
	};
	this.getSelectProject = function(){
		return selectedProject;
	};

	
	/***
	 *		Project 입력 포멧을 담는 객체 
	 */
	var selectedProjectDetail = {
		
	};

	this.setSelectProjectDetail = function(projectDetail){
		selectedProjectDetail = projectDetail;
	};
	this.getSelectProjectDetail = function(){
		return selectedProjectDetail;
	};


	/***
	 * 		Project 멤버들을 담는 객체
	 */
	var selectedProjectMembers = {
		
	};
	
	this.setSelectProjectMembers = function(projectMembers){
		selectedProjectMembers = projectMembers;
	};
	this.getSelectProjectMembers = function(){
		return selectedProjectMembers;
	};


	
	//Angular-file-upload library를 이용한 비동기 파일전송 모듈
	this.uploadProjectFile = function($files){
		console.log("Upload File");
		console.log($files);
		for(var i=0; i<$files.length; i++){
			var file = $files[i];
			//$upload 서비스를 통해서 실제 비동기 업로드를 수행. 이 때 HTTP 경로와 메서드 그리고 해당 파일 필드이름을 지정할 수 있다.
			$scope.upload = $upload.upload({
				url: loginServer,					//경로
				method: 'POST',							//메서드
				file: file,								//파일
				fileFormDataName : 'fileField1'			//필드 이름
			}).success(function(data, status, headers, config){
				//upload를 하고 성공시 롤백처리를 success 메서드를 통해 할 수 있다.
				console.log(data);
			});
		}
	};
	
	
	
	
}]);


