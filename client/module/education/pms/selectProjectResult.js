

app.controller('selectProjectResultCtrl', function SelectProjectResultCtrl($scope, $routeParams, educationWasService){
	
	InitializeComponent();
	
	var projectid = $routeParams.projectid;
	
	$scope.members = {};
	

	$scope.beginAppraisals = {};
	$scope.middleAppraisals = {};
	$scope.endAppraisals = {};
	
	var readJurorCallback = function(jurors){
		console.log(jurors);
		
		var origBeginJurors = [];
		var origMiddleJurors = [];
		var origEndJurors = [];
		
		for(var i=0; i<jurors.length; i++){
			if(jurors[i].presentation_type == 1){
				origBeginJurors.push(jurors[i]);
			}else if(jurors[i].presentation_type == 2){
				origMiddleJurors.push(jurors[i]);
			}else if(jurors[i].presentation_type == 3){
				origEndJurors.push(jurors[i]);
			}
		}

		$scope.$parent.beginJurors = origBeginJurors;
		$scope.$parent.middleJurors = origMiddleJurors;
		$scope.$parent.endJurors = origEndJurors;
		
	};
	
	educationWasService.readJurors(token, projectid, readJurorCallback);
	
	var readAppraisalCallback = function(appraise, presentationType){
		if(appraise == null){
			return ;
		}
		if(presentationType == 1){
			$scope.beginAppraisals = appraise;
			console.log(appraise);
			var beginAppraises = [];
			for(var i=0; i<appraise.list.length; i++){
				var evalData = {
					name: appraise.list[i].username,
					grade: appraise.list[i].entered,
					contents: appraise.list[i].feedback,
					creativeRatio: appraise.list[i].creative_score,
					usefulRatio: appraise.list[i].useful_score,
					technicalRatio: appraise.list[i].technical_score,
					successfulRatio: appraise.list[i].successful_score
				};
				beginAppraises.push(evalData);
			}
			$scope.beginevals = beginAppraises;
		}
		else if(presentationType == 2){
			$scope.middleAppraisals = appraise;


			var middleAppraises = [];
			for(var i=0; i<appraise.list.length; i++){
				var evalData = {
					name: appraise.list[i].username,
					grade: appraise.list[i].entered,
					contents: appraise.list[i].feedback,
					creativeRatio: appraise.list[i].creative_score,
					usefulRatio: appraise.list[i].useful_score,
					technicalRatio: appraise.list[i].technical_score,
					successfulRatio: appraise.list[i].successful_score
				};
				middleAppraises.push(evalData);
			}
			$scope.middleevals = middleAppraises;
			// $scope.$parent.middleevals = middleAppraises;
		}
		else if(presentationType == 3){
			$scope.endAppraisals = appraise;
			
			var endAppraises = [];
			for(var i=0; i<appraise.list.length; i++){
				var evalData = {
					name: appraise.list[i].username,
					grade: appraise.list[i].entered,
					contents: appraise.list[i].feedback,
					creativeRatio: appraise.list[i].creative_score,
					usefulRatio: appraise.list[i].useful_score,
					technicalRatio: appraise.list[i].technical_score,
					successfulRatio: appraise.list[i].successful_score
				};
				endAppraises.push(evalData);
			}
			$scope.endevals = endAppraises;
			// $scope.$parent.endevals = endAppraises;
		}
		
		
	};
	
	educationWasService.readAppraisals(token, projectid, 1, readAppraisalCallback);
	educationWasService.readAppraisals(token, projectid, 2, readAppraisalCallback);
	educationWasService.readAppraisals(token, projectid, 3, readAppraisalCallback);


	$scope.deleteBeginJuror = function($index){
				
		var deleteBeginCallback = function(data){
			console.log(data);
			
			if(data.status == 401 && data.result.data == null){
				alert('권한이 없습니다.');
				return;
			}else if(data.status == 200){
				$scope.$parent.notBeginJurors.push($scope.$parent.beginJurors[$index]);
				$scope.$parent.beginJurors.splice($index, 1);
			}
		};
		
		educationWasService.deleteJuror(token, projectid, 1, $scope.$parent.beginJurors[$index].account_id, deleteBeginCallback);
		
	};
	$scope.deleteMiddleJuror = function($index){
		
		var deleteMiddleCallback = function(data){
			if(data.status == 401 && data.result.data == null){
				alert('권한이 없습니다.');
				return;
			}else if(data.status == 200){
				$scope.$parent.notMiddleJurors.push($scope.$parent.middleJurors[$index]);
				$scope.$parent.middleJurors.splice($index, 1);
			}
		};
		
		educationWasService.deleteJuror(token, projectid, 2, $scope.$parent.middleJurors[$index].account_id, deleteMiddleCallback);
		
	};
	$scope.deleteEndJuror = function($index){
		
		var deleteEndCallback = function(data){
			if(data.status == 401 && data.result.data == null){
				alert('권한이 없습니다.');
				return;
			}
			else if(data.status == 200){
				$scope.$parent.notEndJurors.push($scope.$parent.endJurors[$index]);	
				$scope.$parent.endJurors.splice($index, 1);
			}
		};
		
		educationWasService.deleteJuror(token, projectid, 3, $scope.$parent.endJurors[$index].account_id, deleteEndCallback);
	};


	/***
	 *		시작 평가 토글 함수 
	 */
	$scope.clickBeginEvals = function(){
		console.log("Click Begin Evaluation");
		if($scope.expand == true){
			$scope.expand = false;
		}
		$scope.showBeginEvals = ! $scope.showBeginEvals;
		$scope.beginEvalTable = ! $scope.beginEvalTable;
	};
	$scope.mhoverBeginEvals = function(){
		$('body').css('cursor', 'pointer');
		$('#beginEvalSpan').css('color', 'rgb(101, 189, 244)');
	};
	$scope.mleaveBeginEvals = function(){
		$('body').css('cursor', 'default');
		$('#beginEvalSpan').css('color', 'black');
		
	};


	/***
	 *		중간 평가 토글 함수 
	 */
	$scope.clickMiddleEvals = function(){
		if($scope.expand == true){
			$scope.expand = false;
		}
		$scope.showMiddleEvals = ! $scope.showMiddleEvals;
		$scope.middleEvalTable = !$scope.middleEvalTable;
	};
	$scope.mhoverMiddleEvals = function(){
		$('body').css('cursor', 'pointer');
		$('#middleEvalSpan').css('color', 'rgb(101, 189, 244)');
	};
	$scope.mleaveMiddleEvals = function(){
		$('body').css('cursor', 'default');
		$('#middleEvalSpan').css('color', 'black');
		
	};
		
	
	/***
	 *		완료 평가 토글 함수 
	 */
	$scope.clickEndEvals = function(){
		if($scope.expand == true){
			$scope.expand = false;
		}
		$scope.showEndEvals = ! $scope.showEndEvals;
		$scope.endEvalTable = ! $scope.endEvalTable;
	};
	$scope.mhoverEndEvals = function(){
		$('body').css('cursor', 'pointer');
		$('#endEvalSpan').css('color', 'rgb(101, 189, 244)');
	};
	$scope.mleaveEndEvals = function(){
		$('body').css('cursor', 'default');
		$('#endEvalSpan').css('color', 'black');
	};	

	
});
