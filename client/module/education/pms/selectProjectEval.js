

app.controller('selectProjectEvalCtrl', function SelectProjectEvalCtrl($scope, $routeParams, educationWasService){

	InitializeComponent();
	
	var projectid = $routeParams.projectid;
	
	$scope.beginAppraisalContents = null;
	$scope.middleAppraisalContents = null;
	$scope.endAppraisalContents = null;
	
	
	$scope.registerFeedback = function(oper){
		var creative_score;
		var useful_score;
		var technical_score;
		var successful_score;
		var feedback_text;
		if(oper == 0){
			//add begin appraisal
			creative_score = $('#creativeBeginEvalSelect').val();
			useful_score = $('#usefulBeginEvalSelect').val();
			technical_score = $('#technicalBeginEvalSelect').val();
			successful_score = $('#successfulBeginEvalSelect').val();
			feedback_text = $('#feedbackBeginText').val();
		}else if(oper == 1){
			//add middle appraisal
			creative_score = $('#creativeMiddleEvalSelect').val();
			useful_score = $('#usefulMiddleEvalSelect').val();
			technical_score = $('#technicalMiddleEvalSelect').val();
			successful_score = $('#successfulMiddleEvalSelect').val();			
			feedback_text = $('#feedbackMiddleText').val();
		}
		else if(oper == 2){
			//add end appraisal
			creative_score = $('#creativeEndEvalSelect').val();
			useful_score = $('#usefulEndEvalSelect').val();
			technical_score = $('#technicalEndEvalSelect').val();
			successful_score = $('#successfulEndEvalSelect').val();			
			feedback_text = $('#feedbackEndText').val();
		}
		
		var appraisalid = -1;
		
		if(oper == 0){
			appraisalid = $scope.beginAppraisalContents.result.data.id;
		}else if(oper == 1){
			appraisalid = $scope.middleAppraisalContents.result.data.id;
		}else if(oper == 2){
			appraisalid = $scope.endAppraisalContents.result.data.id;
		}
		
		var appraisalData = {
			account_id: realid,
			appraise_id: appraisalid,
			contents:[{
				num: 1,
				result: creative_score
			},
			{
				num:2,
				result: useful_score
			},
			{
				num:3,
				result: technical_score
			},
			{
				num:4,
				result: successful_score
			},
			{
				num:5,
				result: feedback_text
			}
		]};
		
		var createAppraisalCallback = function(data){
			
			if(data.status == 402 && data.result.data == null){
				alert('이미 전에 평가하신 항목입니다. 평가 이후 수정은 금지됩니다.');
			}
			
			console.log(data);
		};
		
		educationWasService.createAppraisal(token, projectid, oper+1, appraisalData, createAppraisalCallback);		
	};
	
	
	var beginAppraisalContentsCallback = function(data){
		$scope.beginAppraisalContents = data;
	};
	var middleAppraisalContentsCallback = function(data){
		$scope.middleAppraisalContents = data;
	};
	var endAppraisalContentsCallback = function(data){
		$scope.endAppraisalContents = data;
	};
	
	
	educationWasService.readAppraisalContents(token, projectid, 1, realid, beginAppraisalContentsCallback);
	educationWasService.readAppraisalContents(token, projectid, 2, realid, middleAppraisalContentsCallback);
	educationWasService.readAppraisalContents(token, projectid, 3, realid, endAppraisalContentsCallback);
	
});
