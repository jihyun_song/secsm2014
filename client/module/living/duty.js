

app.controller('dutyCtrl', function DutyCtrl($scope, dutyWasService, wasService, calendarAPIService){
	// console.log("Duty Controller");

	if(accountgrant != '200' || accountgrant != '201'){
		$('#duty-change-accept-container').css('visibility', 'hidden');
	}else{
		$('#duty-change-accept-container').css('visibility', 'visible');
	}
	
	var today = new Date();
	$scope.todayYear = today.getFullYear();
	$scope.todayMonth = today.getMonth() + 1;

	
	$scope.changedDate = "";
	
	/***
	 *		DatePicker Configuration 
	 */	
	$('.form_datetime').datepicker({
		format: 'yy-mm-dd',
		todaybtn: true,
		pickerPosition: "bottom-left"
	}).on('changeDate', function(event){
		
		
		var reqDate = $('#dutyDateForm').val();
		$scope.updateReqDate(reqDate);

		
		$('.datepicker').hide();
	});


	$scope.initDuty = function(year, month){
		
		var Year = year;
		var Month = month;
		
		Year = "" + Year;
		if(Month < 10){
			Month = '0' + Month;
		}		
		
		$scope.loadDuty(Year, Month, null);
		$scope.loadMyDuty(Year, Month);
		
		$scope.loadDutyChangeRequest(Year, Month);
	};

	//1일부터 달의 마지막날까지의 당직 목록들의 리스트.
	$scope.monthlyDuties = [];
	
	$scope.loadDuty = function(year, month, callback){

		var spinTarget = document.createElement('div');
		document.body.appendChild(spinTarget);
		var opts = {
			lines: 10, // The number of lines to draw
			length: 10, // The length of each line
			width: 7, // The line thickness
			radius: 12, // The radius of the inner circle
			color: '#000', // #rgb or #rrggbb
			speed: 1.4, // Rounds per second
			trail: 54, // Afterglow percentage
			shadow: false // Whether to render a shadow
		};
		var spinner = new Spinner(opts).spin(spinTarget);
		$('#dutyMainView').css('visibility', 'hidden');
		// $('#dutyMainView').css('opacity', '0.4');

		var readDutyCallback = function(data){
			
			spinner.stop(spinTarget);
			$('#dutyMainView').css('visibility', 'visible');
			// $('#dutyMainView').css('opacity', '1.0');
			
			if(data.status == 403){
				return ;
			}
			else if(data.status == 200){
				monthlyDuties = data.result.data;

				if(callback != null)
					callback(monthlyDuties);
			}
		};
		dutyWasService.readDuties(token, year, month, readDutyCallback);
	};
	
	
	$scope.mydutys = [];
	$scope.myselectduty = $scope.mydutys[0];
	
	$scope.loadMyDuty = function(year, month){

		var readMyDutyCallback = function(data){
			if(data.status == 200)
				$scope.mydutys = data.result.data;
		};
		dutyWasService.readDutyById(token, year, month, id, readMyDutyCallback);
	};
	
	//////////////당직 작성 메뉴
	
	//선택된 날짜 정보	
	$scope.Year = "";
	$scope.Month = "";
	$scope.Day = "";
	
	
	$scope.generateRandomDuty = function(){
		console.log("Generate random duty");
		
		
	};
	
	$scope.confirmDuty = function(){
		console.log("Confirm random duty");
		
		
	};
	
	$scope.dutyboys = [];
	$scope.dutygirls = [];
	
	$scope.dutymembers = [];
	$scope.newdutymember = "";
	
	$scope.loadDutyMembers = function(){

		var spinTarget = document.createElement('div');
		document.body.appendChild(spinTarget);
		var opts = {
			lines: 10, // The number of lines to draw
			length: 10, // The length of each line
			width: 7, // The line thickness
			radius: 12, // The radius of the inner circle
			color: '#000', // #rgb or #rrggbb
			speed: 1.4, // Rounds per second
			trail: 54, // Afterglow percentage
			shadow: false // Whether to render a shadow
		};
		var spinner = new Spinner(opts).spin(spinTarget);
		$('#dutyMainView').css('visibility', 'hidden');
		// $('#dutyMainView').css('opacity', '0.4');

		var readMembersCallback = function(data){
			$scope.dutymembers = data;
			spinner.stop(spinTarget);
			$('#dutyMainView').css('visibility', 'visible');
			// $('#dutyMainView').css('opacity', '1.0');
		};


		wasService.readMembers(token, readMembersCallback);
	};
	
	$scope.loadDailyDuties = function(day){
		//날짜가 선택될 때 실행되는 로직으로 날짜의 당직을 가져오고 초기화하는 루틴을 포함한다. 연관 전역으로 dutyboys와 dutygirls만 초기화해주면 되는군.
		
		//해당 날짜의 당직 정보들을 가져옴.
		var dayDutyInfo = $scope.monthlyDuties[day];
		
		//모달 내 초기화.
		$scope.dutyboys = [];
		$scope.dutygirls = [];
		
		if(dayDutyInfo.m1_account_id != ""){
			
		}
		if(dayDutyInfo.m2_account_id != ""){
			
		}
		
		
		for(var i=0; i<$scope.dutymembers.length; i++){
			
			if(dayDutyInfo.m1_account_id != "" && dayDutyInfo.m1_account_id == $scope.dutymembers[i].googleid){
				
				//여기서 성별을 분류해서 같이 포함시켜서 dutyboys나 dutygirls에 넣어줘야 한다.
				
			}
			
		}
	};
	

	$scope.changeNewDuty = function(gender, $index){
		
		if(gender == 0){
			//남성의 당직을 $index번째 회원과 바꿈. -> dutyboys의 항목을 스왑하면 된다.
			
		}else if(gender == 1){
			//여성의 당직을 $index번째 회원과 바꿈. -> dutygirls의 항목을 스왑하면 된다.
			
		}
		
	};
	
	$scope.addNewDuty = function(){
		var name = $scope.newdutymember;
		var selectmemberindex = $('#newDutyName option').index($('#newDutyName option:selected'));
		var gender = $('#newDutyGender option:selected').val();
		console.log("Add Duty");
		console.log($scope.dutyboys);
		
		if(gender == '남성'){
			//남성쪽으로 UI 분류
			
			var dutyInput = {
				memberinfo: $scope.dutymembers[selectmemberindex],
				dutytype: "남성"
			};
			
			$scope.dutyboys.push(dutyInput);
		}else if(gender == '여성'){
			//여성쪽으로 UI 분류

			var dutyInput = {
				memberinfo: $scope.dutymembers[selectmemberindex],
				dutytype: "여성"
			};

			$scope.dutygirls.push(dutyInput);
		}
		
	};
	
	$scope.confirmNewDuty = function(){
		
		//여기서는 dutyboys와 dutygirls를 잘 분류해서 create 요청을 날려주면 된다.
		
		var year = $scope.Year;
		var month = $scope.Month;
		var day = $scope.Day;
		
		if(month < 10)
			month = '0' + month;
		if(day < 10)
			day = '0' + day;
		
		var reqDate = {
			dutydate: year + '-' + month + '-' + day,
			m1_account_id: '',
			m2_account_id: '',
			m3_account_id: '',
			m4_account_id: '',
			f1_account_id: '',
			f2_account_id: '',
			f3_account_id: '',
			f4_account_id: '',
			
		};
		
		for(var i=0; i<$scope.dutyboys.length; i++){
			
		}
		for(var i=0; i<$scope.dutygirls.length; i++){
			
		}
		
	};
	
	
	$scope.initDutySchedules = function(){

		var dayInfo = function(data){
			console.log(data);
			$scope.Year = data.year;
			$scope.Month = data.month;
			$scope.Day = data.day;
		};
		console.log("init schedules");
		calendarAPIService.dateProvider(dayInfo);

		$scope.loadDutyMembers();
	};
	
	
	////////////당직 변경 신청 관련
	
	$scope.changeReqMembers = [];
	
	///부트스트랩 캘린더의 날짜를 변경하면 변경 당직자들을 업데이트시킴.
	$scope.updateReqDate = function(reqDate){
		$scope.changedDate = reqDate;
		console.log($scope.changedDate);

		//선택한 날짜의 당직 정보들을 가져옴.
		var day = parseInt(reqDate.substring(7, 9));
		var destDayDuty = monthlyDuties[day];
		
		$scope.changeReqMembers = [];
		$scope.changeReqMembers.append(destDayDuty.m1_account_id);
		$scope.changeReqMembers.append(destDayDuty.m2_account_id);
		$scope.changeReqMembers.append(destDayDuty.m3_account_id);
		$scope.changeReqMembers.append(destDayDuty.m4_account_id);

		//동적으로 select에 options 를 추가함.
		$('#changeDutyNames').append("<option>" + destDayDuty.m1_username + "</option>");
		$('#changeDutyNames').append("<option>" + destDayDuty.m2_username + "</option>");

		$('#changeDutyNames').append("<option>" + destDayDuty.m3_username + "</option>");
		$('#changeDutyNames').append("<option>" + destDayDuty.m4_username + "</option>");
		
	};
	
	
	$scope.requestDutyChange = function(){
		
		var dutySrc = $scope.myselectduty;
		var idSrc = id;
		var dutyDest = $('#dutyDateForm').val();
		
		//선택된 당직자의 id를 갖고옴.
		var idDest = $scope.changeReqMembers[$('#changeDutyNames option').index($('#changeDutyNames option:selected'))];
		
		var reqSummary = $('#dutyChangeReason').val();
		
		var reqChangeDuty = {
			dutydate_src: dutySrc,
			account_id_src: idSrc,
			dutydate_dest: dutyDest,
			account_id_dest: idDest,
			request_summary: reqSummary
		};

		var requestChangeCallback = function(data){
			alert('당직 변경 신청이 완료되었습니다.');
		};

		console.log(reqChangeDuty);
		dutyWasService.createDutyChangeRequest(token, reqChangeDuty, requestChangeCallback);
	};
	
	
	//////////////당직 변경 승인 관련
	
	$scope.dutyChangeList = [
	{
		username: '정영호',
		prevdate: '15-03-18',
		changeduser: '김준호',
		nextdate: '15-03-20',
		reason: '귀찮아서'
	}
	];
	
	$scope.loadDutyChangeRequest = function(year, month){
		
		var loadDutyChangesCallback = function(data){
			console.log(data);
			
			
			$scope.dutyChangeList = data.result.data;
		};
		
		dutyWasService.readDutyChangeRequest(token, year, month, loadDutyChangesCallback);
	};
	
	$scope.acceptChangeReq = function($index){

		var acceptReq = {
			change_duty_id: $scope.dutyChangeList[$index].change_duty_id,
			accept: 1,
			accept_summary: '승인합니다.'
		};

		var changeAcceptCallback = function(data){
			alert(data.result.message);
		};

		dutyWasService.createDutyChangeAccept(token, acceptReq, changeAcceptCallback);
	};
	$scope.denyChangeReq = function($index){
		//duty change request delete method is needed.
	};
	
});


