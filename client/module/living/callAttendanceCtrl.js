/***
 *		출석률 보기 컨트롤 
 */
app.controller('AttendanceController', function($scope, livingWasService, wasService) {

	InitializeComponent();

	$scope.attendance = "";
	
	var attendCallback = function(attendDetail){
		$scope.attendance = attendDetail;
		
		console.log($scope.attendance);
	};
	
	/*
	 * 오늘의 날짜를 출력해서 이달의 출석율을 불러오도록 돕는 함수
	 */
	var getToday = function() {

	    var newDate = new Date();
	    var yy = newDate.getFullYear();
	    var mm = newDate.getMonth()+1;
	    
	    var mmstr = "00";
	    if(mm < 10)
	        mmstr = "0" + mm;
	    else
	        mmstr = "" + mm;
	    
	    var dd = newDate.getDate();
	    
	    var ddstr = "00";
	    if(dd < 10)
	        ddstr = "0" + dd;
	    else
	        ddstr = "" + dd;
	    
	    return yy + "-" + mmstr + "-" + ddstr;
	};

	livingWasService.readAttendanceRate(token, getToday(), attendCallback); //2015년 1월의 출석률을 받고 싶을때 이렇게 쓴다.

});
