

app.service('dutyWasService', function($http){
	
	
	this.readDuties = function(token, year, month, callback){
		
		var server = APIServer() + '/duties/' + year +'-' + month + '-01';
		
		$http({
			method: 'GET',
			url: server,
			headers: {'Authorization': token}
		}).success(function(data, status, headers, config){
			console.log("Success to get duties");
			console.log(data);
			callback(data);
		}).error(function(data, status, headers, config){
			console.log("Error occurs in getting duties");
		});
		
		
	};
	
	this.readDutyById = function(token, year, month, userid, callback){
		
		var server = APIServer() + '/duties/' + year + '-' + month + '-01/' + userid;
		
		$http({
			method: 'GET',
			url: server,
			headers: {'Authorization': token}
		}).success(function(data, status, headers, config){
			console.log(data);
			
			callback(data);
			
		}).error(function(data, status, headers, config){
			
		});
		
	};
	
	this.readDutyChangeRequest = function(token, year, month, callback){
		
		var server = APIServer() + '/change/' + year + '-' + month + '-01';
		
		$http({
			method: 'GET',
			url: server,
			headers: {'Authorization': token}
		}).success(function(data, status, headers, config){
			console.log(data);
			
			callback(data);
			
		}).error(function(data, status, headers, config){
			console.log(data);
		});
		
	};
	
	this.createDuty = function(token, dutyData, callback){
		var server = APIServer() + '/duties';
		
		$http({
			method: 'POST',
			url: server,
			data: JSON.stringify(dutyData),
			headers: {'Authorization': token}
		}).success(function(data, status, headers, config){
			console.log(data);
			
			
		}).error(function(data, status, headers, config){
			console.log(data);
		});
	};
	
	this.createDutyChangeRequest = function(token, reqData, callback){
		var server = APIServer() + '/change';

		$http({
			method: 'POST',
			url: server,
			data: JSON.stringify(reqData),
			headers: {'Authorization': token}
		}).success(function(data, status, headers, config){
			console.log(data);
			callback(data);
		}).error(function(data, status, headers, config){
			console.log(data);
		});
		
	};
	
	this.createDutyChangeAccept = function(token, acceptData, callback){
		
		var server = APIServer() + '/change/accept';
		
		$http({
			method: 'POST',
			url: server,
			data: JSON.stringify(acceptData),
			headers: {'Authorization': token}
		}).success(function(data, status, headers, config){
			console.log(data);
			
			callback(data);
			
		}).error(function(data, status, headers, config){
			console.log(data);
		});
		
	};
	
});
