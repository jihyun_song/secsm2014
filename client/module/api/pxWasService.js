

app.service('pxWasService', function($http){
	
	 /***
    *      data를 입력받아서 px item 목록를 생성, callback을 통해 결과를 전달한다. 
    */   
   this.createItem = function(token, itemData, callback){

      var server = APIServer() + '/px/items';


	  $http({
	  	method: 'POST',
	  	url: server,
	  	headers: {'Authorization': token},
	  	data: JSON.stringify(itemData)
	  }).success(function(data, status, headers, confgi){
		callback(data);
	  }).error(function(data, status, headers, config){
	  	consoel.log(data);
	  });

   };
   
   this.editItem = function(token, itemData, callback){
      var server = APIServer() + '/px/items';

	  $http({
	  	method: 'PUT',
	  	url: server,
	  	data: JSON.stringify(itemData)
	  }).success(function(data, status){
	  	callback(data);
	  });

   };
   
   this.deleteItem = function(token, itemData, callback){
      var server = APIServer() + '/px/items/' + itemData;

	  $http({
	  	method: 'DELETE',
	  	url: server,
	  }).success(function(data, status){
	  	callback(data);
	  });
   };
   
   this.getPXLogs = function(token, callback){
      var logserver = APIServer()+'/px/log/1999-01-01%2000:00:00/2020-01-01%2000:00:00';
      
      $http({
         method: 'GET',
         url: logserver,
		 headers: {'Authorization': token}
      }).success(function(data, status, headers, config){
         console.log("Success to get All PX Logs");
         
         if(data.status == 200){
         
         	var PXLogs = data.result.data;
         
         	callback(PXLogs);
         
         }
         
      }).error(function(data, status, headers, config){
         console.log("Error Occur");
      });
   };
   
   /***
    *      data를 입력받아서 px 구매, callback을 통해 결과를 전달한다. 
    */   
   this.showPXItem = function(token, itemBarcode, callback) {
      var server = APIServer()+'/px/items';
      
      $http({
         method: 'GET',
         url: server + "/" + itemBarcode,
		 headers: {'Authorization': token}
      }).success(function(data, status, headers, config){
         console.log("Success to get ItemByBarcode");
         
         if(data.status == 200){
         
         	var ItemByBarcode = data.result.data;
         	var StatByBarcode = data.status;
         
         	callback(ItemByBarcode, StatByBarcode);
         
         }
         
      }).error(function(data, status, headers, config){
         console.log("Error Occur");
      });
      
   };
   
   /*
    * 하는 중 살때 삭제하는 부분
    */
   this.deleteItem = function(token, itemData, callback){
      var server = APIServer()+'/px/items/' + itemData;
	  alert("아이템이 삭제되었습니다");
	  $http({
	  	method: 'DELETE',
	  	url: server,
	  	headers: {'Authorization': token}
	  }).success(function(data, status){
	  	callback(data);
	  });
	};

	/*
	 * data를 입력받아서 px 구매, callback을 통해 결과를 전달한다. 
	 */	 
   this.purchasePXItem = function(token, itemData, callback) {
   	      var server = APIServer()+'/px/purchase';

		
	  $http({
	  	method: 'POST',
	  	url: server,
	  	data: JSON.stringify(itemData),
	  	dataType: 'json',
		contentType: 'application/json; charset=UTF-8',
		headers: {'Authorization': token}
	  }).success(function(data, status){
	  	if(data.status==200){
	 	 	callback(data, status);
	 	 	alert("구입하였습니다.");
	 	}else if(data.status==700){
	 		alert("잔액이 부족합니다.");
	 	}
	  }).error(function(data, status, headers, config){
         console.log("Error Occur");
      });
   };
   
   	/*
	 * data를 입력받아서 px 환불, callback을 통해 결과를 전달한다. 
	 */	 
   this.refundPXItem = function(token, itemData, callback) {
   	      var server = APIServer()+'/px/refund';
	  $http({
	  	method: 'POST',
	  	url: server,
	  	data: JSON.stringify(itemData),
		headers: {'Authorization': token}
	  }).success(function(data, status){
	  	if(data.status==200){
	 	 	callback(data, status);
	 	 	alert("환불하였습니다.");
	 	}else if(data.status==700){
	 		alert("잔액이 부족합니다.");
	 	}
	  }).error(function(data, status, headers, config){
         console.log("Error Occur");
      });
      
   };   
   
   this.getMyPXLogs = function(token, pxId, callback){
      var logserver = APIServer()+"/px/log/1999-01-01%2000:00:00/2020-01-01%2000:00:00";
      
      $http({
         method: 'GET',
         url: logserver + "/"+ pxId,
		 headers: {'Authorization': token}
      }).success(function(data, status, headers, config){
         
        
        
		if(data.status == 200){       
			console.log("Success to get My PX Logs");
         var MyPXLogs = data.result.data;
         
         callback(MyPXLogs);
        }
         
      }).error(function(data, status, headers, config){
         console.log("Error Occur");
      });
   };
   
   /*
    * PX Charge Amount
    */
   this.chargePXAmount = function(token, itemData, callback) {
      var server = APIServer()+'/px/charge';
      
      $http({
      	method: 'POST',
      	url: server,
      	data: JSON.stringify(itemData),
 	    headers: {'Authorization': token}
      }).success(function(data, status){
      	console.log(data);
      });
      
    };
      
	/*
    *       px 목록 받아오는 api를 호출한다.
    */
   this.getItems = function(token, callback){
      
      var server = APIServer()+"/px/items";
      
      $http({
         method: 'GET',
         url: server,
         headers: {'Authorization': token}
      }).success(function(data, status, headers, config){
         console.log("Success to get items");
         
         var itemList = data.result.data;
         
         
         callback(itemList);
         
      }).error(function(data, status, headers, config){
         console.log("Error Occur");
      });
      
   };
	
	
});
