

app.service('educationWasService', function($http){
	
	/***
	 *		data를 입력받아서 Project를 생성하고, callback을 통해 결과를 전달한다. 
	 */	
	this.createProject = function(token, projectData, callback){

		var server = APIServer() + '/projects';

		$http({
			method: 'POST',
			url: server,
			data: JSON.stringify(projectData),
			headers: {'Authorization': token}			
		}).success(function(data, status, headers, config){
			// console.log(data);
			callback();
		}).error(function(data, status, headers, config){
			console.log(data);
		});
	};



	/***
	 *		callback 방식을 통해서 서비스에서 갱신한 결과 값을 가공하여, callback 함수로 전달. 해당 함수에서 뷰로 업데이트하게끔 구현한다. 
	 */
	
	this.readProjects = function(token, callback){
		
		var server = APIServer() + '/projects';

		$http({
			method: 'GET',
			url: server,
			headers: {'Authorization': token}
		}).success(function(data, status, headers, config){
			console.log("Success to get projects");

			if(data.status == 403){
				alert('프로젝트 목록이 존재하지 않습니다.');
			}

			//정상적인 경우에만 콜백
			if(data.status == 200)
				callback(data.result.data);
		}).error(function(data, status, headers, config){
			console.log("Error in getting projects");
		});
	};
	
	
	this.readProject = function(token, projectId, callback){
		
		var server = APIServer() + '/projects/' + projectId;
		
		$http({
			method: 'GET',
			url: server,
			headers: {'Authorization': token}
		}).success(function(data, status, headers, config){
			console.log("Success to get project");
			console.log(data);
			callback(data.result.data);
		}).error(function(data, status, headers, config){
			console.log("Error in getting project");
		});
		
	};
	
	
	this.updateProject = function(token, projectId, projectData, callback){

		
		var server = APIServer() + '/projects/' + projectId;
		var updateData = JSON.stringify(projectData);
		console.log(updateData);
		
		var header = {
			'Authorization': token
		};
		
		$http.put(server, updateData).success(function(data, status){
			console.log(data);
			callback();
		});
	};


	this.deleteProject = function(token, projectId, callback){
		var server = APIServer() + '/projects/' + projectId;
		
		$http.delete(server).success(function(data, status){
			callback();
		});
	};

	
	
	/***
	 *		키워드를 통해서 키워드에 맞는 프로젝트를 검색하는 api를 호출한다. 
	 */
	this.searchProject = function(projectSearchKeyword, callback){
		
		var server = APIServer() + '/projects/search/' + projectSearchKeyword;
		
		$http({
			method: 'GET',
			url: server
		}).success(function(data, status, headers, config){
			console.log(data.result.data[0]);
			
			callback(data.result.data[0]);
			
		});
		
	};
	
	
	/***
	 *		프로젝트의 배심원 목록을 받아오는 api를 호출 
	 */
	this.readJurors = function(token, projectId, callback){
		
		var server = APIServer() + '/presentations/' + projectId + '/jurors';

		$http({
			method: 'GET',
			url: server,
			headers: {'Authorization': token}
		}).success(function(data, status, headers, config){
			console.log(data.result);
			
			//항목이 null인 경우들 체크
			if(data.result.data != null)
				callback(data.result.data);
		});
		
	};
	
	
	/***
	 *		모든 배심원 받아오기 
	 */
	this.readJurorInformation = function(token, callback){
		
		var server = APIServer() + '/presentations/jurors';
		
		$http({
			method: 'GET',
			url: server,
			headers: {'Authorization': token}
		}).success(function(data, status, headers, config){
			console.log(data);
			
			callback(data.result.data);
		});
	};
	
	
	
	
	/***
	 *		프로젝트의 배심원을 등록하는 api를 호출 
	 */
	this.createJuror = function(token, accountId, projectId, presentationType, callback){
		var server = APIServer() + '/presentations/' + projectId + '/jurors';
		
		var createData = {
			account_id: accountId,
			project_id: projectId,
			presentation_type: presentationType
		};
		
		$http({
			method: 'POST',
			url: server,
			data: JSON.stringify(createData),
			content: 'application/json',
			headers: {'Authorization': token}
		}).success(function(data, status, headers, config){
			console.log(data);
			
			callback(data);
		});
	};
	
	
	/***
	 *		프로젝트의 배심원을 삭제하는 api를 호출 
	 */
	this.deleteJuror = function(token, projectId, presentationType, accountId, callback){
		
		var server = APIServer() + '/presentations/' + projectId + '/type/' + presentationType + '/jurors/' + accountId;
		
		$http({
			method: 'DELETE',
			url: server,
			headers: {'Authorization': token}
		}).success(function(data, status, headers, config){
			console.log(data);
			
			callback(data);
		});
	};
	
	
	/***
	 *		project에 대한 평가를 등록하는 함수 
	 */
	this.createAppraisal = function(token, projectId, presentationType, appraisalData, callback){
		
		var server = APIServer() + '/presentations/' + projectId + '/type/' + presentationType + '/appraise/results';
		
		$http({
			method: 'POST',
			url: server,
			data: JSON.stringify(appraisalData)
		}).success(function(data, status, headers, config){
			console.log(data);
			
			callback(data);
		});
	};
	
	
	
	/***
	 *		Project에 대한 평가 항목들을 받아오는 함수 
	 */
	this.readAppraisals = function(token, projectId, presentationType, callback){
		var server = APIServer() + '/presentations/' + projectId + '/type/' + presentationType + '/appraise/results';

		$http({
			method: 'GET',
			url: server,
			headers: {'Authorization': token}
		}).success(function(data, status, headers, config){
			if(data.status == 403){
				console.log("Item 부족");
			}else{
				console.log(data);
				callback(data.result.data, presentationType);
			}
		});
	};
	
	
	/***
	 *		Appraisal Contents를 받아오는 함수 
	 */
	this.readAppraisalContents = function(token, projectId, presentationType, accountId, callback){
		
		var server = APIServer() + '/presentations/' + projectId + '/type/' + presentationType + '/appraise/contents';
		
		var reqData = {
				project_id: projectId,
				presentation_type: presentationType,
				account_id: accountId
		};
		
		$http({
			method: 'GET',
			url: server,
			headers: {'Authorization': token},
			data: JSON.stringify(reqData)
		}).success(function(data, status, headers, config){
			console.log(data);
			callback(data);
		});
		
	};
	
	
	
	
});
