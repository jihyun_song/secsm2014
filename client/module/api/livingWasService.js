

app.service('livingWasService', function($http){
	
		
	/***
    *       출석률을 받아오는 api를 호출한다.
    */
   this.readAttendanceRate = function(token, date, callback){
   	var server = APIServer() + '/attend/rate';
      
      $http({
         method: 'GET',
         url: server + "/" + date,
		 headers: {'Authorization': token}
      }).success(function(data, status, headers, config){
         console.log("Success to read attendance rate");
         
         var attendance = data.result.data.list;
         var average = data.result.data.avg;
         callback(attendance, average);
         
      }).error(function(data, status, headers, config){
         console.log("Error Occur");
      });
      
   };
   
   /***
    *       개인 출석률을 받아오는 api를 호출한다.
    */
   this.getMyAttendanceRate = function(token, date, id, callback){
   	
      var server = APIServer() + '/attend';
      
      $http({
         method: 'GET',
         url: server + "/" + date + "/" + id,
		 headers: {'Authorization': token}
      }).success(function(data, status, headers, config){
         console.log("Success to get my attendance rate");
         
         var attendRate = data.result.data.rate;
         callback(attendRate);
         
      }).error(function(data, status, headers, config){
         console.log("Error Occur");
      });
      
   };
});


