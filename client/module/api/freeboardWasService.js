

app.service('freeboardWasService', function($http){
	
   
	this.getFreeBoardList = function(token, page_num, callback){
		var server = APIServer() + '/boards/free/pages';
      
      $http({
         method: 'GET',
         url: server + "/" + page_num,
         headers: {'Authorization': token}
      }).success(function(data, status, headers, config){
         console.log("Success to get Freeboard list");
         console.log(data);
         if(data.status != 200)
			return ;
         var itemList = data.result.data.list;
         callback(itemList);
         
      }).error(function(data, status, headers, config){
         console.log("Error Occur");
      });
   };
   
   /*
    * 글을 작성한거 올리는 api
    */
   this.writeFreeBoard = function(token, writeData, callback){
   	var server = APIServer() + '/boards/free';
      
      $http({
      	method: 'POST',
      	url: server,
      	data: JSON.stringify(writeData),
        headers: {'Authorization': token}
      }).success(function(data, status){
      	console.log("Success to write freeboard");
      	callback(data);
      });
   };
   
   /*
    * 등록된 작성글, 코멘트를 가져오는 api
    */
   
   this.getFreeBoardDetail = function(token, board_id, callback){
   	var server = APIServer() + '/boards/free';
      
      $http({
         method: 'GET',
         url: server + "/" + board_id,
         headers: {'Authorization': token}
      }).success(function(data, status, headers, config){
         console.log("Success to get freeboard detail");
         console.log(data);
         if(data.status == 200){
         
   		     var board = data.result.data.board;
   	    	 var commentList = data.result.data.comment;
         
	         callback(board, commentList);
         }
         
      }).error(function(data, status, headers, config){
         console.log("Error Occur");
      });
   };
   
   /*
    * 게시글에 코멘트를 적는다.
    */
   this.writeComment = function(token, writeComment, callback){
   	var server = APIServer() + '/boards/comment';
      
      $http({
      	method: 'POST',
      	url: server, 
      	data: JSON.stringify(writeComment),
        headers: {'Authorization': token}
      }).success(function(data, status){
      	callback(data);
      });

   };
   
   /*
    * 게시글에 달린 코멘트를 지운다.
    */
   this.deleteComment = function(token, comment_id, callback){
   	var server = APIServer() + '/boards/comment/' + comment_id;
		
		$http({
			method: 'DELETE',
			url: server,
	        headers: {'Authorization': token}
		}).success(function(data, status){
			callback(data);
		});
	};   
   	
	
});
