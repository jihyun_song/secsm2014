﻿

var APIServer = function(){
	return "http://210.118.74.115:8080";
};


app.service('wasService', function($http){
	console.log("WAS Service Add");
	
	this.loginServer = function(userId, userPw, callback){
		var server= APIServer() + "/login";
		
		var requestData = {
			id: userId,
			password: userPw
		};
		
		$http({
			method: 'POST',
			url: server,
			data: requestData
		}).success(function(data, status, headers, config){
			console.log(data);
			callback(data);
		});
	};
	

	/***
	 * 		전체 회원 명단을 받아오는 api를 호출한다.
	 */
	this.readMembers = function(token, callback){
		
		var server = APIServer() + "/accounts"; //"http://210.118.74.115:8080/accounts";
		
		$http({
			method: 'GET',
			url: server,
			headers: {'Authorization': token}
		}).success(function(data, status, headers, config){
			console.log("Success to read members");
			
			var memberList = data.result.data;
			
			callback(memberList);
			
		}).error(function(data, status, headers, config){
			console.log("Error Occur");
		});
		
	};

	
	/***
    *       개인 회원 계정보를 받아오는 api를 호출한다. (주의!! readMembers와 파라미터가 다르다.)
    */
   this.readMember = function(token, memberId, callback){
      
      var server = APIServer() + "/accounts"; //"http://210.118.74.115:8080/accounts";
      
      $http({
         method: 'GET',
         url: server+"/"+memberId,
		 headers: {'Authorization': token}
      }).success(function(data, status, headers, config){
         console.log("Success to read member");
         
         var memberList = data.result.data;
         
         callback(memberList);
         
      }).error(function(data, status, headers, config){
         console.log("Error Occur");
      });
      
   };
   

});




