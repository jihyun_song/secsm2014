

app.service('calendarAPIService', function($http){
	
	var selectedYear;
	var selectedMonth;
	var selectedDay;
	
	var dateObserver = { };
	
	//callback 방식을 통한 Service 레벨에서의 watch
	this.dateReceiver = function(date){
		dateObserver(date);
	};
	this.dateProvider = function(callback){
		dateObserver = callback;
	};
	
	//하루 하루의 이벤트를 전달하는 옵저버 콜백을 정의
	var dayEventObserver = {};
	this.dayEventReceiver = function(events){
		dayEventObserver(events);
	};
	this.dayEventProvider = function(callback){
		dayEventObserver = callback;
	};
	
	
	/***
	 *		eventData를 받아서 해당 날짜에 이벤트를 기록하는 함수 
	 */
	this.createEvent = function(token, eventData, callback){
		
		var server = "http://210.118.74.115:8081/events";
		
		$http({
			method: 'POST',
			url: server,
			data: JSON.stringify(eventData),
			headers: {'Authorization': token}
		}).success(function(data, status, headers, config){
			console.log(data);
			callback(data);

		}).error(function(data, status, headers, config){
			console.log(data);
		});
		
	};
	
	/***
	 *		해당 아이디가 해당 년월에 가진 이벤트를 받아오는 함수 
	 */
	this.readEvent = function(token, accountId, month, callback){
		
		var server = "http://210.118.74.115:8081/events/" + accountId + "/" + month;

		$http({
			method: 'GET',
			url: server,
			headers: {'Authorization': token}
		}).success(function(data, status, headers, config){
			console.log(data);
			callback(data.result.data);
		}).error(function(data, status, headers, config){
			console.log(data);
		});
		
	};
	
	/***
	 *		해당 eventId를 가진 이벤트를 수정하는 함수 
	 */
	this.updateEvent = function(token, eventId, eventData, callback){
		
		var server = "http://210.118.74.115:8081/events/" + eventId;
		console.log(eventData);
		$http({
			method: 'PUT',
			url: server,
			data: JSON.stringify(eventData),
			headers: {'Authorization': token}
		}).success(function(data, status, headers, config){
			console.log(data);
			callback(data);
		}).error(function(data, status, headers, config){
			console.log(data);
		});
		
	};
	
	/***
	 *		해당 eventId를 가진 이벤트를 삭제하는 함수 
	 */
	this.deleteEvent = function(token, accountId, eventId, callback){
		
		var server = "http://210.118.74.115:8081/events/" + accountId + "/" + eventId;
		
		$http({
			method: 'DELETE',
			url: server,
			headers: {'Authorization': token}
		}).success(function(data, status, headers, config){
			console.log(data);
			callback(data);
		}).error(function(data, status, headers, config){
			console.log(data);
		});
		
	};
});



