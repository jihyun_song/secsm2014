

/***
 * 		프로젝트 파일 첨부시, 첨부 박스 리스트 아이템 디렉티브
 */
app.directive('attachedFileRecords', function(){
	
	return{
		restrict: 'E',
		templateUrl: './view/education/attached-file-records.html',
		link: function($scope, element, attrs){
			
		}
	};
	
});
