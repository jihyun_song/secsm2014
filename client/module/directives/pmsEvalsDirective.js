
/***
 * 	PMS 평가 레코드 디렉티브
 */

app.directive('pmsEvaluations', function($compile){
	return{
		restrict: 'E',
		templateUrl: './view/education/pms/pms-evaluation.html',
		link: function(scope, element, attrs){
			//element에 클릭 이벤트를 달아서, 디렉티브가 클릭될 경우 상세 보기를 활성화 || 비활성화.
			
			element.on('mouseover', function(){
				element.css('cursor', 'pointer');
			});
			
			$compile(element.contents())(scope.$new());
		}
	};
});

