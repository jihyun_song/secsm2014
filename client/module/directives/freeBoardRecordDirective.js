
/***
 *	자유게시판 리스트 레코드 디렉티브. 
 */

app.directive('freeboardRecords', function(freeboardService){
	return{
		restrict: 'E',
		templateUrl: './view/community/freeboard-record.html',
		scope: true,
		link: function($scope, element, attrs){

			var boardCheckObject = element.context.children[0].children[0].children[0];
			var boardNameObject = element.context.children[0].children[1];
		
			// console.log($scope.curPage * $scope.pageSize + $scope.$index);	
		
			$scope.checked = 0;
			//ProjectName에 대한 클릭 이벤트 핸들러
			$(boardNameObject).bind('click', function(){
				console.log($scope.boardMessages);

				// projectService.setSelectProject($scope.project);
				location.href = "#freeboard/" + $scope.boardMessages.board_id;
			});
			
			//ProjectName에 대한 마우스 오버 이벤트 핸들러
			$(boardNameObject).bind('mouseover', function(){
				$(boardNameObject).css('cursor', 'pointer');
				$(boardNameObject).css('text-decoration', 'underline');
				$(boardNameObject).css('font-weight', 'bold');
			});
			
			//ProjectName에 대한 마우스 아웃 이벤트 핸들러
			$(boardNameObject).bind('mouseout', function(){
				$(boardNameObject).css('text-decoration', 'none');
				$(boardNameObject).css('font-weight', 'initial');
			});
			
			$(boardCheckObject).bind('click', function(){
				//체크되어있지 않은 경우 체크하고 푸쉬
				if($scope.checked == 0)
				{
					$scope.checked = 1;
					console.log("Checked");

					var checkedIndex = ($scope.curPage * $scope.pageSize) + $scope.$index;					
					console.log($scope.boardMessages[checkedIndex]);
					freeboardService.setSelectBoard($scope.boardMessages[checkedIndex]);
				}
				//체크되어있는 경우 체크를 해제하고 풀
				else
				{
					$scope.checked = 0;
					console.log("Unchecked");
					
					freeboardService.setSelectBoard(null);
				}
			});
		}

	};
});

