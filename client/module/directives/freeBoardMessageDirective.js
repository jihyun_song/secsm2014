

/***
 *      자유 게시판별 게시글 디렉티브
 */
app.directive('freeBoardMessages', function() {

   return {
      restrict : 'E',
      templateUrl : './view/community/freeboard-message.html',
      link : function($scope, element, attrs) {

      }
   };

});
