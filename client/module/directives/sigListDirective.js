
/***
 *		시그 항목 리스팅 디렉티브 
 */
app.directive('sigLists', function(sigService){
	
	return{
		restrict: 'E',
		templateUrl: './view/education/sig/sig-list.html',
		link: function($scope, element, attrs){
			
			var sigNameObject = element.context.children[0].children[0];
			
			$(sigNameObject).bind('click', function(){
				console.log($scope.sig);
				sigService.setSelectSig($scope.sig);
				// location.href = "#sig/sigboard";
				location.href="#sig/" + $scope.sig.signame;
			});
			$(sigNameObject).bind('mouseover', function(){
				$(sigNameObject).css('font-weight', 'bold');
				$(sigNameObject).css('cursor', 'pointer');
			});
			$(sigNameObject).bind('mouseout', function(){
				$(sigNameObject).css('font-weight', 'initial');
			});
		}
	};
	
});

