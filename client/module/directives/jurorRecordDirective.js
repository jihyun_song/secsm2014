

/***
 *		프로젝트별 배심원 레코드 디렉티브 
 */
app.directive('jurorRecords', function(){
	
	return{
		restrict: 'E',
		templateUrl: './view/education/jurors/juror-record.html',
		link: function($scope, element, attrs){
			var jurorList = element.context.children[0].children[2].children[0].children[0];
			console.log(jurorList);
		}
	};
	
});

