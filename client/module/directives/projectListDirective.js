
/***
 *	프로젝트 리스트 레코드 디렉티브. 
 */

app.directive('projectRecords', function(projectService){
	return{
		restrict: 'E',
		templateUrl: './view/education/pms/project-record.html',
		scope: true,
		link: function($scope, element, attrs){

			var projectCheckObject = element.context.children[0].children[0].children[0];
			var projectNameObject = element.context.children[0].children[1];
		
			// console.log($scope.curPage * $scope.pageSize + $scope.$index);	
		
			$scope.checked = 0;
			//ProjectName에 대한 클릭 이벤트 핸들러
			$(projectNameObject).bind('click', function(){
				console.log($scope.project);

				// projectService.setSelectProject($scope.project);
				location.href = "#pms/" + $scope.project.name;
			});
			
			//ProjectName에 대한 마우스 오버 이벤트 핸들러
			$(projectNameObject).bind('mouseover', function(){
				$(projectNameObject).css('cursor', 'pointer');
				$(projectNameObject).css('text-decoration', 'underline');
				$(projectNameObject).css('font-weight', 'bold');
			});
			
			//ProjectName에 대한 마우스 아웃 이벤트 핸들러
			$(projectNameObject).bind('mouseout', function(){
				$(projectNameObject).css('text-decoration', 'none');
				$(projectNameObject).css('font-weight', 'initial');
			});
			
			$(projectCheckObject).bind('click', function(){
				//체크되어있지 않은 경우 체크하고 푸쉬
				if($scope.checked == 0)
				{
					$scope.checked = 1;
					console.log("Checked");

					var checkedIndex = ($scope.curPage * $scope.pageSize) + $scope.$index;					
					console.log($scope.projects[checkedIndex]);
					projectService.setSelectProject($scope.projects[checkedIndex]);
				}
				//체크되어있는 경우 체크를 해제하고 풀
				else
				{
					$scope.checked = 0;
					console.log("Unchecked");
					
					projectService.setSelectProject(null);
				}
			});
		}

	};
});


