

app.service('freeboardService', ['$upload', function(){
	console.log('Freeboard Service');

	var selectedBoard = "";
	
	this.setSelectBoard = function(board){
		selectedBoard = board;
	};
	this.getSelectBoard = function(){
		return selectedBoard;
	};

	
	/***
	 *		Board 입력 포멧을 담는 객체 
	 */
	var selectedBoardDetail = {
		
	};

	this.setSelectBoardDetail = function(boardDetail){
		selectedBoardDetail = boardDetail;
	};
	this.getSelectBoardDetail = function(){
		return selectedBoardDetail;
	};


	/***
	 * 		Board 멤버들을 담는 객체
	 */
	var selectedBoardMembers = {
		
	};
	
	this.setSelectBoardMembers = function(boardMembers){
		selectedBoardMembers = boardMembers;
	};
	this.getSelectBoardMembers = function(){
		return selectedBoardMembers;
	};
	
	
	
}]);


