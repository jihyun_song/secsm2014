

app.controller('freeBoardSelectCtrl', function FreeBoardSelectCtrl($scope, $routeParams, freeboardWasService){
	console.log("Free Board Select");
	InitializeComponent();
	
	var messageId = $routeParams.messageId;
	$scope.MessageName = "";
	$scope.board = [];
	$scope.comments = [];
	
	
	$scope.editMessage = function(){
		
	};
	
	$scope.removeMessage = function(){
		
	};
	
	
	var freeBoardDetailCallback = function(board, commentList){
		console.log(board);
		console.log(commentList);
		$scope.MessageName = board.title;
		$scope.MessageWriter = board.username;
		$scope.MessageTime = board.insert_time;
		$scope.MessageContents = board.content;
		
		$scope.comments = commentList;
		// $scope.comments = [{
			// comment_id: 0,
			// username: 'A',
			// comments: 'B'
		// },
		// {
			// comment_id: 1,
			// username: 'C',
			// comments: 'D'
		// }
		// ];
	};
	
	
	freeboardWasService.getFreeBoardDetail(token, messageId, freeBoardDetailCallback);
	
	$scope.writeComment = function(){
		var text = $('#commentText').val();
		
		var commentData = {
			board_id: messageId,
			account_id: realid,
			content: text
		};
		
		var commentCallback = function(data){
			console.log(data);
			location.href="#freeboard/:"+messageId;
		};
		
		freeboardWasService.writeComment(token, commentData, commentCallback);
	
	};
	
	$scope.deleteComment = function($index){
		
		var commentCallback = function(data){
			console.log(data);
			location.href="#freeboard/:"+messageId;
		};
		console.log($index);
		console.log($scope.comments);
		var commentId = $scope.comments[$index].comment_id;
		
		freeboardWasService.deleteComment(token, commentId, commentCallback);
	
	};
	
});

