
app.controller('freeBoardCtrl', function FreeBoardCtrl($scope, freeboardWasService){
	console.log("Free Board Control");
	InitializeComponent();
	
	//$scope.boardCount = "";
	$scope.boardMessages = "";


	var page_num = "1"; //일단 첫번재 페이지 게시글들 불러오기(더미)

	$scope.registerMessage = function(){
		console.log("Register Message");
		
		location.href = "#freeboardRegister";
	};


	$scope.curPage = 0;
	$scope.pageSize = 10;
	$scope.showData = function(){

		var getFreeBoardCallback = function(data){
			console.log(data);
				$scope.boardMessages = data;
		};
		
		freeboardWasService.getFreeBoardList(token, page_num, getFreeBoardCallback);
		
		$scope.numberOfPages = function(){
			return Math.ceil($scope.boardMessages.length / $scope.pageSize);
		};
	};


	$scope.messageMouseOver = function(){
		$('body').css('cursor', 'pointer');
	};
	
	$scope.messageMouseLeave = function(){
		$('body').css('cursor', 'default');
	};
	
	$scope.checkProject = function($index){
		$scope.checkedIdx = $index;
	};
	
	$scope.selectMessage = function($index){
		//index 를 페이지별로 계산하여, 해당 인덱스 번째 프로젝트 항목의 id로 포워딩한다.
		console.log($scope.boardMessages[$index].id);
		$('body').css('cursor', 'default');
		var new_index = ($scope.curPage * $scope.pageSize) + $index;
		console.log($scope.boardMessages[new_index]);
		location.href = "#freeboard/" + $scope.boardMessages[new_index].board_id;
	};

});

