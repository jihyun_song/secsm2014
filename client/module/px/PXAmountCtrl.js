/***
 *		PX 현재금액 컨트롤
 */
app.controller('PXAmountController', function($scope, pxWasService, wasService) {

	$scope.remained = []; //wasService 통해 잔액 받아서 담기
	/*
	 *  컨트롤러를 호출하면 사용자의 px잔액을 보여준다.
	 */

	init();
	function init() {
		
		var amountCallback = function(detail){
			$scope.remained = detail.px_amount;
			console.log($scope.remained);
		};
	
		wasService.readMember(token, realid, amountCallback);	//get PX Amount
	}
	
	/*
	 *  충전을 하면 최종잔액(remained)
	 */
	$scope.chargePXAmount = function(){
		
		if($scope.inputData.charged != "" && $scope.inputData.charged != undefined) {
			var newAmount = {
				account_id: realid,
	    		amount: $scope.inputData.charged
			};
			pxWasService.chargePXAmount(token, newAmount, function(data){
				$scope.clearInputs();
			});
			
			init();
		}
	
	
	};
	
	// Clear Product Inputs / make blanks
	$scope.clearInputs = function() {
		angular.element(document.querySelector('#inCharge')).val("");
	};
});
