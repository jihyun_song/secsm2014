/***
 *		PX 바코드 스캔 컨트롤
 */
app.controller('PXScanController', function($scope, $route, purchaseFactory, pxWasService, wasService) {

	InitializeComponent();

	// Item List Arrays
	$scope.incart = {
		items : []
	};
	$scope.barcode = "";
	$scope.remained = "";

	$scope.selection = []; 
	$scope.pxpeople = [];

	//최종구매할 아이템 리스트
	$scope.purchaseList = [];
	$scope.numberList = [];
	$scope.inputData = [{barcode:""}];

	$scope.reloadRoute = function() {
   		$route.reload();
	};
	
	var membersCallback = function(membersDetail) {
		$scope.pxpeople = membersDetail;
	};

	wasService.readMembers(token, membersCallback);

	//공동구매
	$scope.sharePurchase = function(p) {
		
		var idx = $scope.selection.indexOf(p.id);
		
		// is currently selected
		if (idx > -1) {
			$scope.selection.splice(idx, 1);
		}
		// is newly selected
		else {
			$scope.selection.push(p.id);
			console.log(p.id);
		}
		
	};	
	
	$scope.changeItemNum = function($index, $value){
		$scope.incart.items[$index].qty = $value;
	};


	//perform initialization
	init();
	function init() {
		$scope.items = purchaseFactory.getItems();
		$scope.inputData.barcode = null;
	}

	// Add a Item to the list automatically
	$scope.addAutoItem = function(isValid) {

		if (isValid) {

			var barcode = $scope.inputData.barcode;
			pxWasService.showPXItem(token, barcode, function(itembybarcode, statusbybarcode) {
				if (statusbybarcode == 200){
					$scope.incart.items.push({
						barcode : itembybarcode.barcode,
						itemname : itembybarcode.itemname,
						price : Number(itembybarcode.price),
						qty : 1,
					}); //px재고랑 비교, 아이템 자동추가
					alert("아이템을 담았습니다."+itembybarcode);
				}else
					alert("일치하는 바코드가 없습니다.");

				$scope.clearInputs();
			});

			init();
			
		}

	};

	/*
	 * 해당 아이템 지우기
	 */
	$scope.deleteItem = function(item) {
		var retVal = confirm("Do you want to delete item?");

		if (retVal == true) {
				var index = $scope.incart.items.indexOf(item);
				var delItem = $scope.incart.items[index].barcode;

				$scope.incart.items.splice(index, 1);
			if (index == purchaseFactory.itemIndex) {
				purchaseFactory.itemIndex = -1;
				pxWasService.deleteItem(token, delItem, callback);
			}
		}
	};

	$scope.deleteAllItem = function() {
		var retVal = confirm("Do you want to delete item?");

		if (retVal == true) {
			$scope.incart.items= [];
			purchaseFactory.itemIndex = -1;
		}
	};

	// Clear Product Inputs / make blanks
	$scope.clearInputs = function() {
		angular.element(document.querySelector('#inBarcode')).val("");
	};
	

	/*
	 * 아이템 총 합계 가져오기 //여기가 계산이 안되고 있음
	 */
	$scope.getTotal = function(){
		var total = 0;
		
		angular.forEach($scope.incart.items, function(item) {
			total += item.qty * item.price;
		});
		return total;
	};


	/*
	 * 구입하기
	 */
	$scope.purchaseItem = function() {
		$scope.selection.push(realid);
		console.log($scope.selection);
		var idList = [];
		// for(var i=0; i<$scope.selection.length; i++){
			// idList.push($scope.selection[i]);
		// }
		idList = $scope.selection;
		
		console.log("함께구입하는회원 : " + idList); // O
		
		var itemList = [];

		for(var i=0; i<$scope.incart.items.length; i++)
		{
			itemList.push({
				barcode: $scope.incart.items[i].barcode,
				number: $scope.incart.items[i].qty,
			});
		}

		var purchase = {
			account_id_list : idList,
			item_list : itemList
		};
		
		
		var callback = function(data, status){
			console.log(data +","+data.status);

			if(data.status == 200){
				$scope.selection = [];
			
				$scope.incart.items= [];
				purchaseFactory.itemIndex = -1;
			}
		};

		pxWasService.purchasePXItem(token, purchase, callback);

	};

	/*
	 *  환불하기
	 */
	$scope.refundItem = function() {
		$scope.selection.push(realid);
		console.log($scope.selection);
		var idList = [];
		var idList = [];
		// for(var i=0; i<$scope.selection.length; i++){
			// idList.push($scope.selection[i]);
		// }
		idList = $scope.selection;
		
		var itemList = [];

		for(var i=0; i<$scope.incart.items.length; i++)
		{
			itemList.push({
				barcode: $scope.incart.items[i].barcode,
				number: $scope.incart.items[i].qty,
			});
		}
		
		var refund = {
			account_id_list : idList,
			item_list : itemList
		};
		
		var callback = function(data, status){
			console.log(data +","+data.status);
			if(data.status == 200){
				$scope.selection = [];
			
				$scope.incart.items= [];
				purchaseFactory.itemIndex = -1;
			}
		};
		pxWasService.refundPXItem(token, refund, callback);
		
	};
});

app.factory('purchaseFactory', function() {
	//create factory object
	var factory = {};
	var itemIndex = -1;
	var items = [];

	//getItems is simply going to come in and just return items
	factory.getItems = function() {

		return items;
	};
	factory.getSelectedIndex = function() {
		return itemIndex;
	};
	return factory;
});

