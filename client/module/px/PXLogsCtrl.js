/***
 *		PX 로그 보기 컨트롤 
 */
app.controller('PXLogsController', function($scope, $route, pxWasService, wasService) {

	InitializeComponent();

	$scope.pxlogs = [];
	$scope.strpxlogs = [];
	
	$scope.reloadRoute = function() {
   		$route.reload();
	};
	
	var pxlogsCallback = function(pxlogsDetail){ 
		
		$scope.pxlogs = pxlogsDetail;

		//console.log($scope.pxlogs);
	};

	pxWasService.getPXLogs(token, pxlogsCallback);

});

/***
 *		나의 PX 로그 보기 컨트롤 
 */
app.controller('MyPXLogsController', function($scope, pxWasService, wasService) {

	$scope.mypxlogs = [];
	
	var mypxlogsCallback = function(pxlogsDetail){
		$scope.mypxlogs = pxlogsDetail;
		
		//console.log($scope.mypxlogs);
	};

	pxWasService.getMyPXLogs(token, realid, mypxlogsCallback);

});
