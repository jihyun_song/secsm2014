app.controller('AdminItemController', function($scope, itemFactory, pxWasService, wasService) {

	// Item List Arrays
	$scope.items = [];

	$scope.backups = [];
	
	//perform initialization
	init();
	function init() {
		pxWasService.getItems(token, function(itemList) {
			$scope.items = itemList;
		});
	}

	// Add a Item to the list
	$scope.addItem = function() {

		if (itemFactory.itemIndex == -1 || itemFactory.itemIndex == undefined) {
			if ($scope.inputData.itemname != "" && $scope.inputData.itemname != undefined && $scope.inputData.price != "" && $scope.inputData.price != undefined && $scope.inputData.remained != "" && $scope.inputData.remained != undefined && $scope.inputData.barcode != "" && $scope.inputData.barcode != undefined) {

				var newItem = {
					barcode : $scope.inputData.barcode,
					itemname : $scope.inputData.itemname,
					price : $scope.inputData.price,
					remained : $scope.inputData.remained
				};

				pxWasService.createItem(token, newItem, function(data) {
					$scope.items.push(newItem);
					$scope.clearInputs();
				});
			}

		} 

	};

	// Get Total Items
	$scope.getTotalItems = function() {
		return $scope.items.length;
	};

	$scope.cancelItem = function() {
		$scope.clearControls();
		itemFactory.itemIndex = -1;
	};
	
	// Clear Product Inputs / make blanks
	$scope.clearInputs = function() {
		angular.element(document.querySelector('#itemnameid')).val("");
		angular.element(document.querySelector('#priceid')).val("");
		angular.element(document.querySelector('#remainedid')).val("");
		angular.element(document.querySelector('#barcodeid')).val("");
	};

	$scope.clearControls = function() {
		angular.element(document.querySelector('#inItemName')).val("");
		angular.element(document.querySelector('#inPrice')).val("");
		angular.element(document.querySelector('#inRemained')).val("");
		angular.element(document.querySelector('#inBarcode')).val("");
		$scope.triggerChangeEventInputControls();
	};

	$scope.triggerChangeEventInputControls = function() {
		angular.element(document.querySelector('#inItemName')).change();
		angular.element(document.querySelector('#inPrice')).change();
		angular.element(document.querySelector('#inRemained')).change();
		angular.element(document.querySelector('#inBarcode')).change();
	};
	
	$scope.editBackup = function(index, item) {
		$scope.backups[index] = angular.copy(item);
	};

	$scope.editItem = function(item) {
		pxWasService.editItem(token, item, function(data) {
			
			itemFactory.itemIndex = $scope.items.indexOf(item);
			angular.element(document.querySelector('#inItemName')).val($scope.items[itemFactory.itemIndex].itemname);
			angular.element(document.querySelector('#inPrice')).val($scope.items[itemFactory.itemIndex].price);
			angular.element(document.querySelector('#inRemained')).val($scope.items[itemFactory.itemIndex].remained);
			// angular.element(document.querySelector('#inBarcode')).val($scope.items[itemFactory.itemIndex].barcode);
			$scope.triggerChangeEventInputControls();
		});

		//alert(itemFactory.itemIndex + ',' + $scope.items[itemFactory.itemIndex].itemname + ',' + $scope.items[itemFactory.itemIndex].price);
		var item = $scope.items[itemFactory.itemIndex];
		item.itemname = $scope.inputData.itemname;
		item.price = $scope.inputData.price;
		item.remained = $scope.inputData.remained;
		item.barcode = $scope.inputData.barcode;
		$scope.clearControls();
		itemFactory.itemIndex = -1; 

	};

	$scope.cancel = function(index) {

		$scope.items[index] = $scope.backups[index];
		
		// itemFactory.itemIndex = $scope.newField.indexOf(item);
			// angular.element(document.querySelector('#inItemName')).val($scope.newField[itemFactory.itemIndex].itemname);
			// angular.element(document.querySelector('#inPrice')).val($scope.newField[itemFactory.itemIndex].price);
			// angular.element(document.querySelector('#inRemained')).val($scope.newField[itemFactory.itemIndex].remained);
			// angular.element(document.querySelector('#inBarcode')).val($scope.items[itemFactory.itemIndex].barcode);
			$scope.triggerChangeEventInputControls();
		
	};

	$scope.deleteItem = function(item) {
		var retVal = confirm("Do you want to delete item?");

		if (retVal == true) {
			var index = $scope.items.indexOf(item);
			var delItem = $scope.items[index].barcode;

			pxWasService.deleteItem(token, delItem, function(data) {

				$scope.items.splice(index, 1);
			});
			if (index == itemFactory.itemIndex) {
				itemFactory.itemIndex = -1;
			}
		}
	};
});

app.factory('itemFactory', function() {
	//create factory object
	var factory = {};
	var itemIndex = -1;
	var items = [];

	//getItems is simply going to come in and just return items
	factory.getItems = function() {

		return items;
	};
	factory.getSelectedIndex = function() {
		return itemIndex;
	};
	return factory;
});
