#!/bin/bash

exit_usage(){
    echo "Error : $1"
    echo "Usage $0 <simple string>"
    echo "This command for DROP ip in iptables"
    exit 1
}

TO_BAN_IP=$1

if [ $# -ne 1 ]; then
    exit_usage "Insufficient arguments"
fi

sudo iptables -I INPUT -p all -s $TO_BAN_IP -j DROP

