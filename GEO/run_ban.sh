#!/bin/bash

echo "########## IP BLOCK Script START ###########"
# iptables init
sudo iptables -P INPUT ACCEPT
sudo iptables -F
echo "IPTABLES INIT SUCCESS"

BLOCK_LIST_FILE=./GeoIPCountryWhois.csv
echo "BLOCK LIST FILE = $BLOCK_LIST_FILE"

# ADD BLOCK TARGET LIST
# CN China
# FR France
# CL Chile
# RU Rusia
# DE Germany
BLOCK_TARGET_COUNTRY="CN|FR|CL|RU|JP|IN|BO|IT|CH|GB|NL|DE|ES|CY|CZ|UA|SG|TW|CA|DE|CH"

# REGIST BLOCK IP FOR LOOP
for IP_BANDWIDTH in `egrep $BLOCK_TARGET_COUNTRY $BLOCK_LIST_FILE | awk -F, '{print $1, $2}' | awk -F\"  '{print $2"-"$4}'`
do
   sudo iptables -I INPUT -p all -m iprange --src-range $IP_BANDWIDTH -j DROP
done

   sudo iptables -L
echo "################### IP BLOCK Script END ############# "
